<?php
/*
* @fileOverview:Controlling most of the functions for application
* @author:Jithin Zacharia <jithin.zacharia2@ust-global.com>
* @date:18/12/2017
*/


class Logincontroller extends CI_Controller{
  //global items
  private $data = array();
  public function __construct(){
        parent::__construct();
        $this->data['pageTitle']='Admin Panel';
        $this->load->helper('form','url');
        $this->load->database();
        $this->load->model('login_model');
        $this->load->library('session');
        $this->load->helper('cookie');
    }
    public function index(){
      $this->data['pageTitle']='Login';
        $this->load->model('login_model');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('form','url');
        $username=$this->input->post('username');
        $password=$this->input->post('pwd');
        $form_data=$this->input->post();
        $query = $this->db->get("user_name");
        $this->data['records'] = $query->result();
        $result= $this->login_model->login($username,$password);
        if($result){
            $session_array=array();
            foreach($result as $row){
                $session_array=array(
                    'id'=>$row->id,
                    'username'=>$row->username,
                    'employee_type'=>$row->employee_type,
                );
                //setting cookie for current user
                $cookie=array(
                  'name'=>'vibhavlogincookie',
                  'id'=>$row->id,
                  'value'=>'logged in',
                  'expire'=>'3600',
                  'domain'=>'localhost',
                  'path'=>'/',
                  'secure'=>TRUE
                  );
                $this->input->set_cookie($cookie);
                $this->session->set_userdata('logged_in',$session_array);
            }
            if($result)
            $this->load->model('home_model');
            $this->data['asset_type']=$this->home_model->get_asset_type();
            $this->load->model('map_model');
            $this->data['map_data']=$this->map_model->get_locations();
            //$this->load->view('adminpanelsidebar');
            $this->load->view('adminpanel',$this->data);
            }else{
              $this->session->set_flashdata('error','Wrong Username or password .');
              redirect(base_url().'');
            }
    }

     public function login_comparison(){
          $username = $this->input->post('username');
          $password = $this->input->post('password');
          $this->load->model('login_model');
 
          $result = $this->login_model->compare_login($username,$password); 
            if(empty($result)){
              echo "<script type='text/javascript'>alert('Invalid login');</script>";
              //$this->load->view('sidebar');
              $this->load->view('login');
            }
        }

        public function change(){
          $email=$this->input->post('email');
          $new = $this->input->post('newpwd');
          $old=$this->input->post('oldpwd');
          $this->load->model('model_users');
          $this->data['pageTitle']='Change Password';
          $result = $this->model_users->changepwd($email,$new,$old);
          //$this->load->view('adminpanelsidebar');
          $this->load->view('reset',$this->data);
        }

    public function test(){
        //$this->load->view('sidebar');
        $this->load->view('login');
    }

    public function register(){
      //$this->load->view('sidebar');
      $this->data['pageTitle']='Registration';
      $this->load->view('register',$this->data);
    }

    //function for loading designation configuration page
    public function designation(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('designation_model');
        $this->data['pageTitle']='Designation';
        $this->data['designation_results']=$this->designation_model->selectalldesignation();
        if($this->data){
          $this->load->view('designation',$this->data);
        }else{
        $this->load->view('designation',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for adding designation from main page
    public function add_designation(){
      $this->load->model('designation_model');
      $data=array('designation'=>$this->input->post('designation_name'),
        'department'=>$this->input->post('department_name'));
      $result=$this->designation_model->addesignation($data);
      if($result){
        $this->load->view('adminpanelsidebar');
        $this->load->view('designation',$result);
      }else{
        $this->load->view('adminpanelsidebar');
        $this->load->view('designation');
      }
    }

    //function for updating designation 
    public function update_designation(){
      $this->load->model('designation_model');
      $id=$this->input->post('id');
      $data=array('designation'=>$this->input->post('designation_name'),
                  'department'=>$this->input->post('department_name'));
      $result=$this->designation_model->updatedesignation($id,$data);
      if($result){
        $this->load->view('adminpanelsidebar');
        $this->load->view('designation',$result);
      }else{
        $this->load->view('adminpanelsidebar');
        $this->load->view('designation');
      }
    }

    //function for deleting the designation from the UI
    public function delete_designation(){
      $this->load->model('designation_model');
      $id=$this->input->post('id');
      $result=$this->designation_model->deletedesignation($id);
      if($result){
        $this->load->view('adminpanelsidebar');
        $this->load->view('designation',$result);
      }else{
        $this->load->view('adminpanel');
        $this->load->view('designation');
      }
    }

    //function for loading the contact info page 
    public function contact(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('contact_model');
        $this->data['pageTitle']='Contact Details';
        $this->data['public_relations']=$this->contact_model->publicinfo();
        if($this->data){
          $this->load->view('publicontact',$this->data);
        }else{
          //$this->load->view('sidebar');
          $this->load->view('publicontact',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }
    //function for loading help documents view
    public function fileupload(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('fileupload_model');
        $this->data['pageTitle'] = 'Files Upload';
        $this->data['files_data']=$this->fileupload_model->viewallfiles();
        if($this->data['files_data']){
          $this->load->view('fileupload',$this->data);  
        }else{
          $this->load->view('fileupload',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('login');
      }
    }



    //function for uploading files to server
    public function do_upload(){
      $this->data['pageTitle'] = 'Files Upload';
      $this->load->helper(array('form','url'));
      $config['upload_path']='./uploads/';
      $config['allowed_types']='pdf|jpg|png|docx|pptx|ppt|xls|xlsx';
      $config['max_size']=0;
      $config['max_width']=1024;
      $config['max_height']=768;
      $this->load->library('upload',$config);
      if(!$this->upload->do_upload('userfile')){
        echo "<script type='text/javascript'>alert('Error in uploading the file');</script>";
        $this->load->view('fileupload',$this->data);
      }else{
        $data=array('upload_data'=>$this->upload->data());
        $this->load->model('api_provider_model');
        $fileName=$this->upload->data('file_name');
        $filePath=$this->upload->data('full_path');
        $fileData=array('filename'=>$fileName,
          'filepath'=>$filePath
          );
        $transferData=$this->api_provider_model->filekeeper($fileData);
        echo "<script type='text/javascript'>alert('File successfully Uploaded');</script>";
        $this->load->view('fileupload',$this->data);
      }

    }

    //function for uploading pdf tenders files into server
  public function govtenders_upload(){
    if(isset($this->session->userdata['logged_in'])){
      $this->load->helper(array('form','url'));
      $config['upload_path']='./govTenders/';
      $config['allowed_types']='pdf|docx|xlx|xlsx';
      $config['max_size']=0;
      $config['max_width']=1024;
      $config['max_height']=768;
      $this->load->library('upload',$config);
      print_r($this->upload);
      die();
      if(!$this->upload->do_upload('userfile')){
        echo "<script type='text/javascript'>alert('Error in uploading the file');</script>";
        $this->load->view('adminpanelsidebar');
        $this->load->view('govtenders');
      }else{
        $data=array('upload_data'=>$this->upload->data());
        $this->load->model('api_provider_model');
        $fileName=$this->upload->data('file_name');
        $filePath=$this->upload->data('full_path');
        $fileData=array('filename'=>$fileName,
          'filepath'=>$filePath
          );
        $transferData=$this->api_provider_model->tenderkeeper($fileData);
        echo "<script type='text/javascript'>alert('File successfully Uploaded');</script>";
        $this->load->view('adminpanelsidebar');
        $this->load->view('govtenders');
      }
      
    }
  }

    public function logout(){
        $this->load->model('login_model');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->helper('form','url');
        $this->session->sess_destroy();
        delete_cookie('vibhavlogincookie');
        $this->data['pageTitle']='Logout';
        $this->load->view('login',$this->data);
    }
    
    public function usermodule(){
      if(isset($this->session->userdata['logged_in']))
      {
       $this->load->model('user_model');
       $this->data['pageTitle'] = 'Users';
       $this->data['main_data']=$this->user_model->get_userdata();
       //$this->load->view('adminpanelsidebar');
       $this->load->view('usermodule',$this->data);;
     }else{
      //$this->load->view('sidebar');
      $this->load->view('register');
    }
  }

    public function deleteuser(){
      if(isset($this->session->userdata['logged_in'])){
        $id=$this->input->post('id');
        $this->load->model('user_model');
        $this->user_model->delete_user($id);
        $query = $this->db->get("surveydata");
        $this->data['pageTitle'] = 'Admin';
        $this->data['main_data'] = $query->result();
        echo "<script type='text/javascript'>alert('User deleted successfully');</script>";
        //$this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$this->data);
      }else{
        //$this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function reports(){
    if(isset($this->session->userdata['logged_in'])){
      //$this->load->view('adminpanelsidebar');
      $this->data['pageTitle'] = 'Reports';
      $this->load->view('reports',$this->data);
    }else{
    //$this->load->view('sidebar');
    $this->load->view('register');
  }
   }

public function report_date(){
  if(isset($this->session->userdata['logged_in']))
     {
       $this->load->helper('pdf_helper');
       $from_date=$this->input->post('from_date');
       $to_date=$this->input->post('to_date');
       $this->load->model('reportmodel');
     if(  $data['main_data']=$this->reportmodel->get_data($from_date,$to_date))
         {
           $this->load->helper('pdf_helper');
           $this->load->view('pdf_report',$data);
         }else{
            $this->session->set_flashdata('entry','No entries ');
            redirect(base_url().'index.php/logincontroller/reports');
         }                     
     }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
}

  //api for viewing emergency numbers
  public function emergency(){
    if(isset($this->session->userdata['logged_in'])){
      $this->load->model('emergency_model');
      $this->data['pageTitle'] = 'Contant Numbers';
      $this->data['emergency_numbers']=$this->emergency_model->getDetails();
      //$this->load->view('adminpanelsidebar');
      $this->load->view('emergency',$this->data);
    }else{
      //$this->load->view('sidebar');
      $this->load->view('register');
    }
  }

  public function pdf_generate(){
       if(isset($this->session->userdata['logged_in'])){
        $this->load->helper('pdf_helper');
        $this->load->view('pdf_report',$data);
      }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
    }

  //function for deleting the pdf files from the admin fileupload
  public function delete_pdfiles(){
    $this->load->model('deletepdfiles_model');
    $pdfilename=$this->input->post("fileName");
    $pdfullpath=$this->input->post('filePath');
    $data=$this->deletepdfiles_model->deletepdf($pdfilename,$pdfullpath);
    if($data){
      $this->load->view('adminpanelsidebar');
      $this->load->view('fileupload',$data);
    }else{
      $this->load->view('adminpanelsidebar');
      $this->load->view('fileupload');
    }
  }

  public function analytics(){
    if(isset($this->session->userdata['logged_in'])){
        $this->load->model('analytics_model'); 
        $this->data['pageTitle'] = 'Analytics';
        $this->data['main_data']= $this->analytics_model->getcount(); 
        //$this->load->view('adminpanelsidebar');
        $this->load->view('analytics',$this->data);
    }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
    }
  }

  //function for viewing tenders documents module
  public function govtenders(){
    if(isset($this->session->userdata['logged_in'])){
      $this->load->model('fileupload_model');
      $this->data['pageTitle'] = 'Govt Tenders';
      $this->data['tenders']=$this->fileupload_model->govtendersupload();
      $this->load->view('govtenders',$this->data);
    }else{
      //$this->load->view("sidebar");
      $this->load->view('register');
    }
  }

  //function for deleting gov tenders
  public function govtenders_pdfiles(){
    if(isset($this->session->userdata['logged_in'])){
      $this->load->model('fileupload_model');
      $filename=$this->input->post('fileName');
      $filepath=$this->input->post('filePath');
      $data=$this->fileupload_model->deletegovtenders($filename,$filepath);
      if($data){
        return true;
      }else{
        return false;
      }
    }
  }

  public function messages(){
    if(isset($this->session->userdata['logged_in'])){
        $this->load->model('messages_model');
        $this->data['pageTitle'] = 'Messages';
        $this->data['main_data']=$this->messages_model->getmessages();
        //$this->load->view('adminpanelsidebar');
        $this->load->view('messages',$this->data);
      }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function deletemessage(){
      if(isset($this->session->userdata['logged_in'])){
        $id = $this->input->post('id');
        $time=$this->input->post('time');
        $this->data['pageTitle'] = 'Messages';
        $this->load->model('messages_model');
        $this->messages_model->delete_message($id,$time);
        $query = $this->db->get("user_message");
        $this->data['records'] = $query->result();
        //$this->load->view('adminpanelsidebar'); 
        $this->load->view('messages',$this->data);
      }else{
           // $this->load->view('sidebar');
          $this->load->view('register');
        }
    }
        
    public function admin(){
       if(isset($this->session->userdata['logged_in'])){
        $this->data['pageTitle'] = 'Home';
        $this->load->model('home_model');
        $this->data['asset_type']=$this->home_model->get_asset_type();
        $this->load->model('map_model');
        $this->data['map_data']=$this->map_model->get_locations();
        //$this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$this->data);
      }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function getassettype(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('home_model');
        $data['asset_type']=$this->home_model->get_asset_type();
        echo json_encode($data);
        $this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$data);
      }else{
         // $this->load->view('sidebar');
          $this->load->view('register');
        }
    }
    public function searchdb(){
      if(isset($this->session->userdata['logged_in'])){
        $id = $this->input->post('id');   
        $this->load->model('home_model');
        $data['map_data']=$this->home_model->search_db($id); 
        $this->load->model('home_model');
        $data['asset_type']=$this->home_model->get_asset_type();
        $this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$data);
       }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }  
    }

  public function searchdropdown(){
      if(isset($this->session->userdata['logged_in'])){
        $id = $this->input->post('id');   
        $this->load->model('home_model');
        $this->data['map_data']=$this->home_model->search_dropdown($id); 
        $this->load->model('home_model');
        $this->data['asset_type']=$this->home_model->get_asset_type();
        //$this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$this->data);
         }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }    
       
    }

  public function pass_id(){
      if(isset($this->session->userdata['logged_in'])){
          $id = $this->input->post('id');
          $this->load->model('user_model');
          $result = $this->user_model->get_profile($id);
          $this->data['pageTitle'] = 'User Profile';
          $this->data['profile_data'] = $result;
          //$this->load->view('adminpanelsidebar');
          $this->load->view('profile',$this->data);
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        } 
    }

  public function approve(){
      if(isset($this->session->userdata['logged_in'])){
          $id = $this->input->post('id');
          $this->load->model('user_model');
          $this->data['pageTitle'] = 'Users';
          $this->data['approve_data']=$this->user_model->approve_userdata($id);
          $this->load->model('user_model');
          $this->data['main_data']=$this->user_model->get_userdata();
          //$this->load->view('adminpanelsidebar');
          $this->load->view('usermodule',$this->data);
         }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        } 

      }

    public function block(){
       if(isset($this->session->userdata['logged_in'])){ 
        $id = $this->input->post('id');
        $this->load->model('user_model');
        $this->data['pageTitle'] = 'Users';
        $this->data['approve_data']=$this->user_model->block_user($id);
        $this->load->model('user_model');
        $this->data['main_data']=$this->user_model->get_userdata();
        //$this->load->view('adminpanelsidebar');
        $this->load->view('usermodule',$this->data);
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function register_comparison(){
        $id = $this->input->post('id');
        $this->load->model('register_model');
        $result = $this->register_model->compare_db($id); 
    }

    public function username_exist(){
      $username=$this->input->post('username');
      $this->load->model('register_model');
      $result=$this->register_model->compareusername($username);
      if($result){
        return true;
      }else{
        return false;
      }
    }

    public function email_comparison(){
      $email=$this->input->post('email');
      $this->load->model('register_model');
      $result=$this->register_model->compare_email($email);
      if($result){
        return true;
      }else{
        return false;
      }
    }

    public function username_comparison(){
       if(isset($this->session->userdata['logged_in'])){
        $id = $this->input->post('id');
        $this->load->model('register_model');
        $result = $this->register_model->compare_username($id); 
        if($result){
            echo "<script type='text/javascript'>alert('This username is already registered');</script>";
            //$this->load->view('sidebar');
                $this->load->view('register');
          }
          }else{
            //$this->load->view('sidebar');
          $this->load->view('register');
        }      
    }  

    public function dataupdate(){
      if(isset($this->session->userdata['logged_in'])){
       $this->load->model('assetupdate_model');
       $this->data['pageTitle'] = 'Survey data';
       $this->data['main_data']=$this->assetupdate_model->get_data();
       $this->load->view('dataupdate',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    public function delete_asset(){
          if(isset($this->session->userdata['logged_in'])){
           $id=$this->input->post('id');
          $this->load->model('assetupdate_model');
          $result= $this->assetupdate_model->deletion_asset($id);
          if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('dataupdate',$result);
            }else{
              return false;
            }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
        }
      }

    public function approve_asset(){
        if(isset($this->session->userdata['logged_in'])){
           $id=$this->input->post('id');
           $this->load->model('assetupdate_model');
           $result = $this->assetupdate_model->approve_profile($id);
             if($result){
                  return true;
                  $this->load->view('adminpanelsidebar');
                  $this->load->view('dataupdate',$result);
                }else{
                    return false;
                }
           }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }


   public function modal_asset(){  
     if(isset($this->session->userdata['logged_in'])){
        $id=$this->input->post('id');
        $data=array('employee_id'=>$this->input->post('employee_id'),
        'office_name' =>$this->input->post('office_name'),
        'administrative_department'=>$this->input->post('administrative_department'),
        'office_type'=>$this->input->post('office_type'),
        'address'=>$this->input->post('address'),
         'pincode'=>$this->input->post('pincode'),
         'building_type'=>$this->input->post('building_type'),
         'landline'=>$this->input->post('landline'),
         'district'=>$this->input->post('district'),
         'taluk'=>$this->input->post('taluk'),
         'village'=>$this->input->post('village'),
         'block'=>$this->input->post('block'),
         'localbody_type'=>$this->input->post('localbody_type'),
         'localbody_name'=>$this->input->post('localbody_name'),
         'email'=>$this->input->post('email'),
         'landmark'=>$this->input->post('landmark'),
         'contact_person'=>$this->input->post('contact_person'),
         'contact'=>$this->input->post('contact'),
         'working_start' =>$this->input->post('working_start'),
         'working_end' =>$this->input->post('working_end'),
         'status' =>$this->input->post('status'),
         
        );
          log_message('data office name',$id);
          $this->load->model('assetupdate_model');
          $result = $this->assetupdate_model->get_profile($id,$data);
         if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('dataupdate',$result);
             }else{
            return false;
            }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
    }

    public function feedbackandsuggestions(){
     if(isset($this->session->userdata['logged_in'])){
        $this->data['pageTitle']='Feedback and Suggestions';
        $this->load->view('feedbackandsuggestions',$this->data);
       }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
    }

    public function department(){
      if(isset($this->session->userdata['logged_in'])){
       $this->load->model('department_model');
       $this->data['pageTitle']='Department';
       $this->data['department_data']=$this->department_model->get_department();
       $this->load->view('department',$this->data);
       }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
    }

    //function for viewing muncipality
    public function muncipality(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('muncipality_model');
        $this->data['pageTitle']='Municipality';
        $this->data['muncipality_data']=$this->muncipality_model->getall();
        $this->load->view('muncipality',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }
    //function for viewing gramapanchayath
    public function gramapanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('gramapanchayath_model');
        $this->data['pageTitle']='Grama Panchayat';
        $this->data['gramapanchayath_data']=$this->gramapanchayath_model->get_gramapanchayath();
        $this->load->view('gramapanchayath',$this->data);
      }else{
       // $this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for loading block panchayath view 
    public function blockpanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('blockpanchayath_model');
        $this->data['pageTitle']='Block Panchayat';
        $this->data['blockpanchayath_data']=$this->blockpanchayath_model->getall();
        $this->load->view('blockpanchayath',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for loading corporation view
    public function corporation(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('corporation_model');
        $this->data['pageTitle']='Corporation';
        $this->data['corporation_data']=$this->corporation_model->getall();
        $this->load->view('corporation',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for adding new corporation
    public function add_corporation(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('corporation_model');
        $data=array('localbodyname'=>$this->input->post('corporation_name'),
          'localbodytype'=>'Municipal Corporation',
          'district'=>$this->input->post('district_name'));
        $this->data['pageTitle']='Corporation';
        $this->data = $this->corporation_model->addcorporation($data);
        if($this->data){
          $this->load->view('corporation',$this->data);
        }else{
          $this->load->view('corporation');
        }
      }else{
       // $this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for updating corporation
    public function update_corporation(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('corporation_model');
        $id=$this->input->post('id');
        $data=array('localbodyname'=>$this->input->post('corporation_name'),
          'localbodytype'=>'Municipal Corporation',
          'district'=>$this->input->post('district_name'));
        $this->data['pageTitle']='Corporation';
        $this->data = $this->corporation_model->updatecorporation($id,$data);
        if($this->data){
          $this->load->view('corporation',$this->data);
        }else{
          $this->load->view('corporation',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for deleting corporation
    public function delete_corporation(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('corporation_model');
        $id=$this->input->post('id');
        $this->data['pageTitle']='Corporation';
        $this->data =$this->corporation_model->deletecorporation($id);
        if($this->data){
          $this->load->view('corporation',$this->data);
        }else{
          $this->load->view('corporation',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
      }


    public function category(){
       if(isset($this->session->userdata['logged_in'])){
       $this->load->model('category_model');
       $this->data['pageTitle']='Category';
       $this->data['category_data']=$this->category_model->get_category();
       $this->load->view('category',$this->data);
       }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
    }
//loading localbody


    public function localbody(){
       if(isset($this->session->userdata['logged_in'])){
         $this->load->model('localbody_model');
         $this->data['pageTitle']='Localbody';
         $this->data['localbody_data']=$this->localbody_model->get_localbody();
         $this->load->view('localbody',$this->data);
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
    }


    public function districts(){
      if(isset($this->session->userdata['logged_in'])){
       $this->load->model('district_model');
       $this->data['pageTitle']='Districts';
       $this->data['district_data']=$this->district_model->get_district();
       $this->load->view('districts',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    // Creating building type
    public function buildingtype(){
      if(isset($this->session->userdata['logged_in'])){
       $this->load->model('buildingtype_model');
       $data['buildingtype_data']=$this->buildingtype_model->get_buildingtype();
       $this->load->view('buildingtype',$data);
      }else{
         // $this->load->view('sidebar');
          $this->load->view('register');
         }
    }


    //function for viewing districtpanchayath
    public function districtpanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('districtpanchayath_model');
        $this->data['pageTitle']='District Panchayat';
        $this->data['districtpanchayath']=$this->districtpanchayath_model->getall();
        $this->load->view('districtpanchayath',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for addding districtpanchayath
    public function add_districtpanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $this->data['pageTitle']='Add District Panchayat';
        $this->load->model('districtpanchayath_model');
        $this->data=array('localbodyname'=>$this->input->post('districtpanchayath_name'),
                    'localbodytype'=>'District Panchayat',
                    'district'=>$this->input->post('district_name')); 
        $result=$this->districtpanchayath_model->add_newdistrictpanchayath($this->data);
        if($result){
          //$this->load->view('adminpanelsidebar');
          $this->load->view('districtpanchayath',$this->data);
        }else{
          //$this->load->view('adminpanelsidebar');
          $this->load->view('districtpanchayath',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //Updating districtPanchayath
    public function update_districtpanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('districtpanchayath_model');
        $id=$this->input->post('id');
        $this->data['pageTitle']='Update District Panchayat';
        $this->data=array('localbodyname'=>$this->input->post('districtpanchayath_name'),
          'localbodytype'=>'District Panchayat',
          'district'=>$this->input->post('district_name'));
        $this->data = $this->districtpanchayath_model->update_districtpanchayath($id,$this->data);
        if($this->data){
          $this->load->view('districtpanchayath',$this->data);
        }else{
          $this->load->view('districtpanchayath',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //function for deleting district_panchayath
    public function delete_districtpanchayath(){
      if(isset($this->session->userdata['logged_in'])){
        $id=$this->input->post('id');
        $this->data['pageTitle']='Delete District Panchayat';
        $this->load->model('districtpanchayath_model');
        $this->data = $this->districtpanchayath_model->delete_districtpanchayath($id);
        if($this->data){
          $this->load->view('districtpanchayath',$this->data);
        }else{
          $this->load->view('districtpanchayath',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

  //for add districts
    public function add_district(){
        if(isset($this->session->userdata['logged_in'])){
          $data=array('district_name'=>$this->input->post('district_name'));
          $this->load->model('district_model');
          $this->data['pageTitle']='Add District';
          $this->data = $this->district_model->add_newdistrict($data);
          return;
          $this->load->view('districts',$this->data);
         }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
     }

   public function add_buildingtype(){
      if(isset($this->session->userdata['logged_in'])){
        $data=array('buildingtype_name'=>$this->input->post('buildingtype_name'));
        $this->load->model('buildingtype_model');
        $result = $this->buildingtype_model->add_newbuildingtype($data);
        return;
        $this->load->view('buildingtype',$result);
       }else{
       // $this->load->view('sidebar');
        $this->load->view('register');
       }
   }

     //function for adding new contact to the server
    public function add_contact(){
      if(isset($this->session->userdata['logged_in'])){
          $this->load->model('contact_model');
          $this->data['pageTitle']='Contact';
          $data=array('contactname'=>$this->input->post('contactname'),
                      'contactnumber'=>$this->input->post('contactnumber'));
          $this->data=$this->contact_model->addnewcontact($data);
          return;
          $this->load->view('emergency',$this->data);
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

    //updating contact list
    public function update_contact(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('contact_model');
        $id=$this->input->post('id');
        $data=array(
          'contactname'=>$this->input->post('contactname'),
          'contactnumber'=>$this->input->post('contactnumber'),
          );
        $this->data['pageTitle']='Contact';
        $contactname=$this->input->post('contactname');
        $contactnumber=$this->input->post('contactnumber');
        $this->data = $this->contact_model->updatecontact($id,$contactperson,$contactnumber,$data);
        if($result){
          $this->load->view('emergency',$this->data);
        }else{
          $this->load->view('emergency', $this->data);
        }
      }
    }
      //function for updating the contact list
    public function delete_contact(){
      if(isset($this->session->userdata['logged_in'])){
        $this->load->model('contact_model');
        $id=$this->input->post('id');
        $this->data['pageTitle']='Contact';
        $data=array(
          'contactname'=>$this->input->post('contactname'),
          'contatnumber'=>$this->input->post('contactnumber'),
          );
        $this->data =$this->contact_model->deletecontact($id,$data);
        if($result){
          $this->load->view('emergancy',$this->data);
        }
      }else{
        //$this->load->view('sidebar');
        $this->load->view('register');
      }
    }

      //deleting emergency numbers
      public function delete_emergency(){
        if(isset($this->session->userdata['logged_in'])){
          $this->load->model('contact_model');
          $this->data['pageTitle']='Emergency';
          $contact_person=$this->input->post('contactperson');
          $contact_number=$this->input->post('contactnumber');
          $this->data = $this->contact_model->deleteemergenecy($contact_person,$contact_number);
          if($this->data){
            $this->load->view('emergancy',$this->data);
          }else{
            $this->load->view('emergancy',$this->data);
          }
        }
      }

     //delete district
     public function delete_district(){
        if(isset($this->session->userdata['logged_in'])){
          $id=$this->input->post('id');
          $this->load->model('district_model');
          $this->data['pageTitle']='District';
          $this->data = $this->district_model->deletion_district($id);
        if($this->data){
          return true;
          $this->load->view('districts',$this->data);
        }else{
          return false;
        }
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

    public function update_district(){
      if(isset($this->session->userdata['logged_in'])){
        $id=$this->input->post('id');
        $this->data['pageTitle']='District';
        $data=array('district_name'=>$this->input->post('district_name'),);
        $this->load->model('district_model');
        $this->data = $this->district_model->update_district($id,$data);
         if($this->data){
            return false;
            $this->load->view('districts',$this->data);
            // redirect($this->uri->uri_string());
            }else{
                  return true;
              }
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

      //delete buildingtype
     public function delete_buildingtype(){
         if(isset($this->session->userdata['logged_in'])){
            $id=$this->input->post('id');
            $this->load->model('buildingtype_model');
            $result= $this->buildingtype_model->deletion_buildingtype($id);
            if($result){
              return true;
              $this->load->view('buildingtype',$result);
            }else{
              return false;
            }
          }else{
            //$this->load->view('sidebar');
            $this->load->view('register');
         }
      }

    public function update_buildingtype(){
      if(isset($this->session->userdata['logged_in'])){
        $id=$this->input->post('id');
        $data=array('buildingtype_name'=>$this->input->post('buildingtype_name'),);
        $this->load->model('buildingtype_model');
        $result = $this->buildingtype_model->update_buildingtype($id,$data);
         if($result){
            return false;
            //$this->load->view('adminpanelsidebar');
            $this->load->view('buildingtype',$result);
            // redirect($this->uri->uri_string());
            }else{
                  return true;
              }
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

 // for add category
     public function add_category(){
       if(isset($this->session->userdata['logged_in'])){
          $data=array('institution_category'=>$this->input->post('institution_category'));
          $this->load->model('category_model');
          $this->data['pageTitle']='Category';
          $this->data = $this->category_model->add_newcategory($data);
            if($this->data){
                        return true;
                     $this->load->view('category',$this->data);
                  }else{
                    return false;
                   }
        }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }

     }

    //delete district
     public function delete_category(){
           if(isset($this->session->userdata['logged_in'])){
            $id=$this->input->post('id');
            $this->load->model('category_model');
            $this->data['pageTitle']='Category';
            $this->data = $this->category_model->deletion_category($id);
            if($this->data){
              return true;
              $this->load->view('category',$this->data);
            }else{
                 return false;
                }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

    public function update_category(){
       if(isset($this->session->userdata['logged_in'])){
         $id=$this->input->post('id');
         $data=array('institution_category'=>$this->input->post('institution_category'),);
         $this->load->model('category_model');
         $this->data['pageTitle']='Category';
          $this->data = $this->category_model->update_categorytype($id,$data);
          if($this->data){
            $this->load->view('category',$this->data);
            }else{
                  return false;
            }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

// for local body
      public function add_localbody(){
        if(isset($this->session->userdata['logged_in'])){
           $data=array('local_body_type'=>$this->input->post('local_body_type'));
           $this->load->model('localbody_model');
           $this->data['pageTitle']='Localbody';
           $this->data = $this->localbody_model->add_newlocalbody($data);
            if($this->data){
                return true;
                $this->load->view('localbody',$this->data);
                }else{
                return false;
              }
           }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
     }

     public function update_localbody(){
      if(isset($this->session->userdata['logged_in'])){
         $id=$this->input->post('id');
         $data=array('local_body_type'=>$this->input->post('local_body_type'));
         $this->data['pageTitle']='Localbody';
         $this->load->model('localbody_model');
         $this->data = $this->localbody_model->update_localbodytype($id,$data);
         if($this->data){
            $this->load->view('localbody',$this->data);
          }else{
           return false;
          }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }
      }

    public function delete_local(){
         if(isset($this->session->userdata['logged_in'])){
           $id=$this->input->post('id');
            $this->load->model('localbody_model');
            $this->data['pageTitle']='Localbody';
          $this->data= $this->localbody_model->deletion_localbody($id);
          if($this->data){
                       return true;
                       $this->load->view('localbody',$this->data);
                     }else{
                     return false;
                   }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }  
      }

    public function add_department(){
      if(isset($this->session->userdata['logged_in'])){
           $data=array('department_name'=>$this->input->post('department_name'));
           $this->load->model('department_model');
           $this->data['pageTitle']='Department';
           $this->data = $this->department_model->add_newdepartment($data);
           if($this->data){
                return true;
                $this->load->view('department',$this->data);
            }else{
              return false;
            }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }  
     }

    public function delete_department(){
         if(isset($this->session->userdata['logged_in'])){
           $id=$this->input->post('id');
           $this->load->model('department_model');
           $this->data['pageTitle']='Department';
           $this->data= $this->department_model->deletion_department($id);
          if($this->data){
            return true;
            $this->load->view('department',$this->data);
          }else{
            return false;
          }
         }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         }  
      }

    public function update_department(){
      if(isset($this->session->userdata['logged_in'])){
            $id=$this->input->post('id');
            $data=array('department_name'=>$this->input->post('department_name'),);
            $this->load->model('department_model');
            $this->data['pageTitle']='Department';
            $this->data = $this->department_model->update_departmenttype($id,$data);
            if($this->data){
              $this->load->view('department',$this->data);
              }else{
                return false;
          }
          }else{
          //$this->load->view('sidebar');
          $this->load->view('register');
         } 
      }


    public function doforget(){
      //$this->load->view('sidebar');
      $this->data['pageTitle'] = 'Forgot Password';
      $this->load->view('forgotpwd',$this->data); 
    }

    public function forgot_password(){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|callback_validate_credentials');
            //check if email is in the database
      $this->load->model('model_users');
      if($this->model_users->email_exists()){
      function randomString($length = 8) {
      $length = 8;        
      $str = "";
      $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
      $max = count($characters) - 1;
      for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
      }
      return $str;
    }
      $password = randomString();
      $ci=get_instance();
      $sender_email='vibhavapp@gmail.com';
      $sender_pass='Vibhav@123';
      $email=$this->input->post('email');
      $this->load->library('email');
      $config['protocol']    = 'smtp';
      $config['smtp_host']    = 'ssl://smtp.gmail.com';
      $config['smtp_port']    = '465';
      $config['smtp_timeout'] = '7';
      $config['smtp_user']    = $sender_email;//sender email set here
      $config['smtp_pass']    = $sender_pass;//sender email password. also set "Access for less secure apps" to ON in sender gmail account
          //https://www.google.com/settings/security/lesssecureapps
          //https://accounts.google.com/DisplayUnlockCaptcha
          $config['charset']    = 'utf-8';
          $config['newline']    = "\r\n";
          $config['mailtype'] = 'text'; // or html
          $config['validation'] = TRUE; // bool whether to validate email or not      
          $this->email->initialize($config);
          $this->email->from('vibhavapp@gmail@gmail.com', 'Vibhav Password Change');
          //$mailer->Body = 'Your password is: "'.$password.'';
          $this->email->to($email); 
          //$mailer->AddAddress($_POST['email']);
          $this->email->subject('Reset Password');
          $this->email->message($password);  
          $this->email->send();
          //echo $this->email->print_debugger();
          echo "<script>alert('Your generated passsword has been sent to your email.')</script>";  
          $this->load->model('model_users');
          $result = $this->model_users->resetpwd($email,$password);   
          //$this->load->view('sidebar');
          $this->load->view('login');
            // redirect($this->uri->uri_string());
    }else{
            //$this->load->view('sidebar');
            $this->load->view('forgotpwd');
            echo "<script>alert('your email is not in our database')</script>";
    }
  }

  public function user_asset(){
    $id=$this->input->post('id');
    $this->load->model('user_model');
    $this->data['pageTitle'] = 'User Profile';
    $this->data['profile_data'] = $this->user_model->get_profile($id);   
    $this->data['asset_data'] = $this->user_model->select_allasset($id); 
    //$this->load->view('adminpanelsidebar');
    $this->load->view('profile',$this->data);
  }
}
?>
