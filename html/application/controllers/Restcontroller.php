<?php
/*
* @fileOverview:Controlling all REST services functions for application
* @author:Jithin Zacharia <jithin.zacharia2@ust-global.com>
* @date:18/19/2017
*/


 header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');

class Restcontroller extends REST_Controller{
	public function user_post(){
		//API call for register the user
		$data=array('employee_id'=>$this->post('employee_id'),
		'username'=>$this->post('username'),
		'fullname'=>$this->post('fullname'),
		'email'=>$this->post('email'),
		'mobile '=>$this->post('mobile'),
		'password'=>$this->post('password'),
		'employee_department'=>$this->post('employee_department'),
		'employee_designation'=>$this->post('employee_designation'),
		'centre_id '=>$this->post('centre_id'),
		'centre_name'=>$this->post('centre_name'),
		'status'=>$this->post('status')
	);
		if(empty($data)){
			$this->response("Please supply data correctly",400);
		}else{
		$this->load->model('api_register_model');
		$result=$this->api_register_model->user_register($data);
		log_message('data office name',$data['name']);
		 if($result === false){
            $this->response(array('status' => 'failed'));
        }else{
            $this->response(array('status' => 'success'));
        }
    }
	}


	//user get function
	public function user_get(){
		$this->load->model('api_login_model');
		 if(!$this->get('username')){
            $this->response("ID supplied not correct", 400);
        }
        $user = $this->api_login_model->user( $this->get('username') );     
        if($user){
            $this->response($user, 200); // 200 being the HTTP response code
        }else{
            $this->response(NULL, 400);
        }
	}

	//function for posting login
	public function login_post(){
		$username=$this->post('username');
		$password=$this->post('password');
		if(is_null($username) && ($password)){
			$this->response("Please supply Username and password",400);
		}else{
		$this->load->model('api_login_model');
		$data=$this->api_login_model->login($username,$password);
		if($data){
			$this->response($data,200);
		}else{
			$this->response(array('status' => 'failed'));
		}
	}
	}
    	

	//test function for testing api calls 
	public function userhello_get(){
		$data="jithin";
		$count=1;
		$tr=json_encode($data);
		if($count==1){
			$this->response($tr,200);
		}else{
			$this->response('Try again',400);
		}
	}

	//function for sending the search results to mobile app
	public function search_post()
	{	
		$office_name=$this->post('searchText');
		//$office_type=$this->post('searchText');
		$this->load->model('api_search_results');
		$data=$this->api_search_results->search($office_name);
		$final_data="[";
		if(count($data)>0)
		{
			foreach($data as $v)
			{
			$office_name=$v->office_name;
			$office_type=$v->office_type;
			$latitude=$v->latitude;
			$longitude=$v->longitude;
			$photo=$v->photo;
			$address=$v->address;
			$email=$v->email;
			$final_data .="{\"office_name\":\"".$office_name."\",\"office_type\":\"".$office_type."\",\"latitude\":\"".$latitude."\",\"longitude\":\"".$longitude."\",\"photo\":\"".$photo."\",\"address\":\"".$address."\",\"email\":\"".$email."\"},";		         
			}
			//trim 
			$final_data = rtrim($final_data, ',');
	    }
		$final_data .="]";
		header('Content-Type: application/json');
		//$this->response($final_data,200);
		echo $final_data;
		//	echo $v->office_name;
	}

	//new search with image file read
	public function searchsurvey_post()
	{	
		$office_name=$this->post('searchText');
		//$office_type=$this->post('searchText');
		$this->load->model('api_search_results');
		$data=$this->api_search_results->search($office_name);
		//$final_data="[";
		if(count($data)>0)
		{
			foreach($data as $v)
			{
				$fileDetails = FCPATH . 'assets/surveyimgs/'.$v->photo;
				$this->load->helper('file');
				$fileText = read_file($fileDetails);
			$office_name=$v->office_name;
			$office_type=$v->office_type;
			$latitude=$v->latitude;
			$longitude=$v->longitude;
			$photo=$fileText;
			$address=$v->address;
			$email=$v->email;
			$final_data = $photo;
			}
			//trim 
			$final_data = rtrim($final_data, ',');
	    }
		//$final_data .="]";
		header('Content-Type: application/json');
		//$this->response($final_data,200);
		echo $final_data;
		//	echo $v->office_name;
	}

	public function searchall_get(){	
		$this->load->model('api_search_results');
		$data=$this->api_search_results->searchall();
		$final_data="[";
		if(count($data)>0){
			foreach($data as $v){
			$office_name=$v->office_name;
			$office_type=$v->office_type;
			$latitude=$v->latitude;
			$longitude=$v->longitude;
			$photo=$v->photo;
			$address=$v->address;
			$email=$v->email;
			$final_data .="{\"office_name\":\"".$office_name."\",\"office_type\":\"".$office_type."\",\"latitude\":\"".$latitude."\",\"longitude\":\"".$longitude."\",\"photo\":\"".$photo."\",\"address\":\"".$address."\",\"email\":\"".$email."\"},";		         
			}
			//trim 
			$final_data = rtrim($final_data, ',');
	    }
		$final_data .="]";
		header('Content-Type: application/json');
		//$this->response($final_data,200);
		echo $final_data;
		//	echo $v->office_name;
	}


//function for posting the assets into db
	public function assets_post(){
		//API call for posting assets

		//image handling..
		$photo = $this->input->post('photo');
		$photo_str = preg_replace('/\s+/', '+', $photo);
		//$photo = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEA8QDxIQDw8QEA8QEA8PDw8PDxAPFREWFhURFRUYHSggGBolGxUVITUhJykrLjAuFx8zODMsNyotLisBCgoKDg0OGhAQFy0dHR0tLS0tLSstLS0rKy0rLS0tLS0tLi0tLSsrLS0rLSstLS0tKy4tLS0tLS0tLSsrLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAADBAECBQYABwj/xAA+EAACAgEDAgQDBwIEAgsAAAABAgADEQQSIQUxEyJBUQZhcRQjMkKBkbFSoRUzYsFy0QckNFNjgpKywvDx/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/EADcRAAICAQMCAwYFBAEEAwAAAAABAhEDBBIhMUEiUWEFE3GRofAygbHB0RQj4fFCFVKCsjNDRP/aAAwDAQACEQMRAD8A+LoJm2SGVZLYwqJJbAPUkhsuPUZRJNnVBjFSQs6YsaQQs2TDVrFZVjlKRDsZWIpMKpibL3FwZJLkViZyydlsyTMqYCAPGJsEzRWYykTW0tIy38jAGZaNIzGqKMy0aKZqabRy0h7zX0tWJaREpGzpDLRk2atIlEMZAgTZDCAgLwAVuMBCu/6RCPzRWshsA6JIbAYSuQ2MYSqS2NDSVSbN4sPXXGbxkECwNlILUOYF7hxIg3BAZLHuLhoh7z26BEshIMRi5F1MkVlWMQnIE4gZSmAMuKMJTCVrNUjKx/S15Mqi4yN3RaOWkabjZ0+mlpDsaWmUFjWnTEBM06TKIGVjESRAAFixCFLlgAtsiA/N9VcybAbqpkNiG66ZLGMLVJspBkrklxYRa47NUy/hx2aKRetYWWpDCCS2PcE2ybFuPARNkORbEncZuZ6Oxbz26KyXMruhRDmQZSRDkBCzSJnYxUk0EbXTNPKRSZ02jo7TRItM0q65RVhRXALDV1QAZrEYDKQEWzGANxAQrasQhfbAD860VzBiHqq5DGNJXEAZa5Iwi1RDCrXEUmX8OFmikeWqKy0w6JJsdhfDisLI8OBLIKxUTR7bAKAOsLJcSUSOzNoOteZSIaKinmWmQOabT8y0B0XTtNiaxGjboSaIscRYxh0WAWHRYDsIFjAuIATARVoCA2CAAMRAfnmhZgxDtSyRjSLJYB0SSVQdEiHQTw4rGSKohpF1SI1SDJVJLoJ4cYbSrJGPaCdYxbQTGDFRVVkMTQZa5JEohQuJojGSCVV5loijT0dE1SFRv6OvtNUFGlUktFDKrGAVFgAZBAYUCMCcQA9ACrCIATiAAsQA/PNAnOyR6oSRjVayWUM1pENDCVxGigHWuFGigFWqI0UC/gRM0WMIlck092VdYB7sE0YbAFgjIcAJWJkOJIWSyaDJJIaC7cy0ZSQ1p6prEyaNbR1zVEG1pVmqA0allIYyqxgEVIDCKsBBQsYydsAI2xAVKwAG6QAHsiGfnTTzFkGhSJBaH6a5LLUTQpphRvDGMrTCjoWMPXp4jWOIaTTRGqxEmmSaLGQa5JWwDYkBOAB0gTtF3SMhwKbIMxlEqVkMyaPLEjNoapE0SM2aWlSaRMWjW01c1RmzSoWaoRoVCNDGkWMAyrGARVjAuFgIttgB4rAZQrEBRlgBTEQH5romLJNXSrIZrFWammSI6YQNOlIHZGA1Wkk2URyquI2UQ+2BaRUpEWkDZYqDaL2LJDaLWCBDiLvAloGRGZSiVZZJyzieVYIxYeqWjNo0tI0tdTGRt6btNYmbRoUiaIlj1UoQ3XGMOojAIBGBcCAiQIhniIAVIjAowiAHAD83aVJixJGrp1kHVBGvpUiO7FA0Klks6VEbqWSaxiN1iM1SCQGkeEVFUDsEQ6FbYqChS2KiWhYmBm0RiBm0WaogZKsB7kED94UzlnXmQogc8i4EaMpDekaUYyNzSNNIszZq0TVEMcrlCG6zGhjKGUAVYAXEAPQA8YAVMABWGFgC3xWB+d9OkxbKguTU0qSTsxxNbTJJZ6EEP1LJN0husQNUhhIy0i4jKotEFArDEUkKWxDoUuiIaFiIjJjegosZlNSb33qiA/h8Rvw7ieJpijb+BxavJsh6s2adVUlT0OBfqwbltc2May+fKteOCB27TpfkeTy+bObrsVgHT8DZxnuCCQVPzBBE48kNrOiM9yLFpJEgunPMLMmbWis7S4shmzp2m8WZs0KmmghpDGgGEMYBVaAFw0APboAQWgBVnhYxe15LYAN0VgfCdLXMbNMSNXTVwO/GjUoWS2d0BlDEbpB1MDRB6zGWHWAyTGUhe1oF0K2GS0FClhksiSAExGEhq2uoUktbm37o10oCVYsx3b29CoH951Yo1GzxtVPfkpdhvoXSlsZiwxVSBbawc42AgsAfcjMjLl21fcz2V3Fdboqa2d6rkP2m226vSAYsorVgjM3Pqdvt+vMrMrhfkYwbU2hXE4my5MNVBGTZoaR5SZDZtaaybRZLNGl5qmSOVvLsBhHjsYQPHYFw0VgeLQsCC0LAGzxWMXtsk2AHxIrA+O6WuYXyaYjTorjs9DGOUryATgEgE4JwM8nA7wOpOlZyWq+JtQLNKuxKUvfaxdHZ6wLNrBhnuByRj3HpmbqEKl5o4Ja3Naqkn6DT/FFqFkrA1zI21rKKmSg+vFhY849NuPnM/AuZPbfm+Tox6vM+kd1eRs6X4ro8nih6mbuNruFPz8oI/b0kqn0Z1rVpcTi19/fY6HR6yu0E1WJYB32MGwfYjuD8jF0OmGSM/wALsOYWaoWuEo0QjdAYmzSGZyBExUYSNGg0C+tHC1VrYXtfPjW2A1q4QleAPTj3m+TcoeHqeHalJt+Y2ure9tUKVFWnNb2OgbYRUvuPX3x9ZhKoKO/lmipmZqBoqyyAtdqUbTVVXIeALq2LeJ/pz27nKj2M3xSnPE96p8/4OTOkpqiFWcbFJh60iIbG6kjJbH9M80ixGlRZNVIQ7XZLTGHV47AMrwsAgeOwPF4WBBeFgDZogF7WiADugB8u0qzmsvGalFco78bGPDgdUWcf8T9ENbLfpq23s7Wb6VzcmoGStjOxOKyTyABjZnPM6ITtVJ/Py/k4dRh2PdFcP6MzdPqHR6h9ora217jrE8Kx7ablJLu+fLj0yDjjPvMM+GM4t7Xx056ovTZ5Qko7lT68dDW6ppg1DH7TS7HlUsOnCk+m3AyD88zzsE6yf/G16q/v6HpZ4bocTTfrX+/qeb7PZ0tb0elNfpFrDbGfT6htjgMhKv8AeAjnIA7k5zPRUnDNtrwy+R5dXDdfiRGh+PbXsrSnTWW5Vt9LWeJZlRnNThcngEkMCfnOh41FXJmmLX5Yuqv9Tr9J1VLURiGqLnAWzaPPz5NwJXdwfLnPHaZeiPXw6vHk46PyZbUiS2dRnukmyJMGg8yghmG4ZCDLkZ5C/OC5Zy5ZVFs2NNUbmrp8IaehXvdASTa6l+zkDGRwOPXMnWZvc45TjyzzNNBTdyXr8RzrWgK7FpO02Bkz/Txkn9gRiefo9dKUJvLyo8nU8am1Xh/g4rU6VdPqfvgXQiux9h8xCWbWIzwDizj6z19JqY5oto4ddpXhkubTs3V5nNJU2jhsZpWIkdrWADCJKEHrMpMByqyaJiGUeVZSDq0djCB4WB7fCwogtGBRmgIA5gBTMAPm+mXE42OBoVNKTO2DGFaVZ1RYdP2+nELo3VNUzmtV8IEWG6nbYa8jTaR0rWhC+d28numT2788Ga+8tU+/XzOKelcXuhz6GV0i40a166BR9sD5pVKhVp1duLtKxdwVxtOCC3y9JnnwrJC5t135v4MjHk2+FJX27fkIfFyX6rqOy3Tpo9SyqrKXJDkDizdjngYyPb5Q02zT6dtScor7obhPPlUKps2uhfDT6blqdPfYHDpYL79PfUcAYV19OO2B3PvOLL7Rhl4TcV8E18j0cfsqcObTf5r6mJ8Y6PWX6kGxWCuQlb2NX2GMK9igBjntnmd2l1OCOPwvp1/0cefQaiWRKuvy+ZsaHWXaVaku+03LtAt3LXatbDHmRwwZl78EcemZKzYsv4Wl9Dvhp9Zp4q1vXpzX7mt07rNGoyKm84zmtgVfj1Cnkj5/xCcJR5ZUdTDJ04fk+o/oqWFniK61eEPENjlQF5Azz3PI4jxcy+By6rItjT78GrpOleKzWm6x8PZ4Vm9gWQux3YHAySTj5zj1ftCGOezbfmThxbYqTbTHX0TI9Zd2sALAF8cAqR/OP7zlllx5sE1CNPh/I1Te9O76r7+Rynxvp8gPz5Q4OO+GXj6eZVl+x8lT2+Ztr8W/S7u8X/sjoL79PQx7+EgP1AwT+4no5uMjPmTXrEysBykSgGkEaYF1EqxBkjTGHrMtMaQZXjsovvjA9vgBO+OxFWaOxAmaFhQPMkdHz5UnKKhysQTOmDDJKs6YyGazHZ0RkNVNCzZSKa/QLZVeqLVXdcm3x2qV2+QYHhx9e2cjBlKddTHLhU+Vwz52ei1UdR01Optv1t7B1YI4rdH4ah0stIDKUPYnggj5TebvFKkkvuzz43DIrbsa0fV9fTa2muRgfH8GoXL947NytZavyZ2lTnOOZ5+bQ6eXjjxxbrt688nq4PaGReHKrfZvr9OBr/G7n8eq8aeqyry26eyi+2zt/wCHuUg/WQtFijtlGT56O0v15NI+0W7Wzp1XLM7pWvvenUW11Pdp9PnxK7FStqhjJCk2FjgemJtl0sdyV033XN/HhG2D2rtTaTaXZqmvzt/X5in2rR6ghlfwbe6liarAc/lf1/eNQ1GDirXzR1+/9na1Lc0pevhfzNrR9YNQA1lX2tBzXqEO1kPPLqoO7uOV54/WbYMmNvjwtnn632Znxx3QfvIry6/Lv+XyOv6brNTZVX4LUeGVG2xWa1WGO/GJ5mpw6ZZJPJJ3fSqJg24p7V063f7Dr6LUbdzXb8EHwwiqMDvtP9XtziTh1Omi9sYcPhv4kyTl0fK9OPz7mX1xRbWwPqpzwe2O+JnGD0+Zc8Wqfoz0cX9zDK11T48mY3wec6Og/Jh+zmevquMrPjOToEEwsY3TGgG1EtAEAjEFVZSKSCqJRR6OwPb4WBG+FgT4kaYHi0YFGMY6K5gOjiwJxMug1YjRSDIso1TDKkDVSDoIGikHQxWXvKa2urw7msStvunHiNQLnVQpOFXHm5/LyCcS8cua8zHMlJX3R8u6ipQ16q9QWoSm27Q6rUvu1D2j/tNW07VXzKAq9gnabwdtwX/LpJLp6PucklXifbs319Toeg9Kto8W2paLftDCzaz21PWpGdm4hi2M+uPWeHrNTjzNRncdvHZp+vVHuaTBkwJuKT3fFfyN6zL5F+iuGVKmyh6nbb6jKMGA5meJ7H/azr4O1+qo0yvfxkxP4qn/AJMD/q1NfhMUs02C40+rrOn1KAnvX4gAsHPyPzJ4nobM+aW9eGf/AHRdxfxrp98HJvw4o7H4o+UlUl8L6/fIY6ZdSqWaHUh9mc1WY3uuANljEbzjHBbdjPHHETzzxeHU4/zX3Xyo6cHLU9Jlqv8Aj2/Pv87oa6X4tTu+kfwbVIN+jv4rcn8xK8DPOLB+ufTHLKE0lmVxfSS6/X6pm2VLJbgts+67P1/P/uXXvZ1vTfiqqzej76dRWPvNM4zaMdyuOHHzE5X7PnacPEn0a++DlU4cp8Ndb++TJ6t1w1m3xtPalYt8BnJrNqOaRcxNYbJUIwZjzj956X/TM0oRTatO/wDAv+o4Y9L/AGBfC9Arp8AMLGpdg2NwKgncu4OFYEqQfwjvOrVLx2+D52a5NpePT9d2P7YnPcfUVDNR9sfqM/7iVuj5fX/Ay9lligFVFoz5lUlX2/6Qcgn5ZEtbX6CNFRAAixplFwZVjIJjsAZMVjKF4rEV8SOxpBA8tFJEFpZVFcxjo5TZOOi9oWtIqFQ0iRjTDBYy0ycRFpl0aIe4KLSMFSQRyCO4Ik3XI7Pl3VNLR9q1n2Wut0ofWV6lrn8Sy6p7ci2lCRl6x3x+07pTkly+tbf2v4nNCMW+eK6/4+B0mm6fYtdYXW3GvaPDdU07b19DuKEmfOzzQlNt4VfdW+PqfQRxTjBJZHX5fwB1GqSsHf1YoR3Vhonf/wBATOZtjwyyf/mVf+S/c5smZQ/+/wD9X+whoel29QqW3V32tpvFL6eo101M6r5fEfavryMewnTl1ENFLZiglJrl22vqZY8UtX48km0nwqSOg13QtNcQXqRXByr1ZqsUjtgricGPWZodJt32fKO+WmxS6x/NcP5nN/E3R2pXxBrSDWrhK9SUFjA4yivwzdh5SCCcT0dJn3vY8PXuk/nX8UcuoxV41mprza+V/wA2IdTuNmqZKtRY5uLVNq9LWz0PU1KWV1bVyVYPX5gueM+2J6uDDHDj2rt5v1PLz5nmybu78unT8xb4f1pdVVWWpC+kt1t1tviX2Il4RhlQSlfKeU47evpve18mCipR4/U6n/o/0TL9sLDalepuprQYAUB8sPc84HJPacOvl4kjA6phOAZeqMdDlZlJiDo0tMEE3x2WQbYbgINse4CheG4AbPFYFd8EzSKLLbNkzTaWNkuwor4kdhRhoJzFh1ERIQQEFSKxWWIhY7BxWVuPbpLHuMz4i6Iusp8NmWpE3XWeHWgsvVBnbu9GA3EE8+nYzXDlalx1S4/giSXR9GcV0uxWQ7d3+G0nZqDp7gHZlYgag1nzpU64JA9f1hqcbvdFL3na1x8PK/Kzq0ubjZJvZ6fd150dSOiaAIziihaVUnc9aOWAGS25hkAD1nirV6tzUd73P1PVej08IW4qjC6J03VWiyzR6izQaN3J09LBrtyf94FY+QE+n+3f1dRrMOFqGWO+a6v1POw6XLkueOWyL6L9xXWnqL6v7DVrmuYLm5q1GmSlT+LeVHJwR255x3nXilgWH3zx7V8zmyLM8vulPcbyfC+k0dDajVN4zqpa6x+zk4GMd2yccEnJM4parLnnsx8X0r74OzHpsWGO/JzXW/vkxesVWqlhb7PRUllFl1ArYDcVqKJkdnVWZd425wex2z0cOSFxirbrh+fZnLmxZNspukuL+jRy/WdQV8SsXZ8WnSmz7o0KxrQgVsgUhmBxhw204PfPHVGPdrz+pyZJ1aUutX26H1WrUVV2gI9f3r6kWgMvFni2WqTz/qsH7Tzc6lKUrXR8ffyOdqh37ShONy5PpuGZz7H5AmHrMRQ5WZSAIDGM8WisYMvJspIqbIWBQ3Q3CB+NHuGkeNs0ibRRHjS0zSiDqJdhtK/aI7HtE6zMiWGUwEFEBMuGktEs8zyGQCZ5IrIFkTHZHiZ4PIPGD7SAcjG6/wDD4t0Y02jppR9+eBg7CCW28/j9sEZ7EHjHVp839xOXX9fiLda2nMugqCUO176Ss119RPnsrWwc1Orn8Ndg2EjnHbiaZsafjgl7znb2+Pxa5o7cGZ8Y8jezi+/w+Cfc6/r3UzTpi9IDWuUp0yjHmtfhcD5cn9J4Oh0/vc/j6R5Z7Gsy+6xeHq+F+YH4d6Omhpcuwa5gbdVqHJIO3LE5P5Rz9e89DVap58ihBeiRx6bTrDBzm/iYK2X9Sse9GNWk07btOreU3XL2ds8f7D9DOlyx6RLH1lLr6JmUYZNW3PpGPT1YDpWl1Gs8ayq5hXbYRYbqK7cIqhFD7TgEj8vr3kanNiwOMZRtxXFNr14/k3wxySwyyLJW99Gk7rj8vvzNbpPQGDA32u2pSqq6lgtTCxDWASSyksQ25cHnG33lZNc8qbgvR36duvlR5bwLHJRn06Wuv3YTqnTtM9676KmsaytrnCfjFgZskD0PiV/sJTy5VN8tKk18k/5G8UVjtdXa+PP2wOq6dottqUVVpcDsrZaxuW1lbZYv0IPtjES1ORNNu18exktLLv5X/g3+n69XCqTizADIe4baCw49s4mG65ONBPTShDczUR5ojAvvg2BBeIYNniKBPZJYAHskCKB5SRSLb5ujoigb2y0apATdLLUSvjwHsDVmQc4VWgIuGgB4vEyGUayQzNlTbJZDK+JENECyQxMYquwQRwQQQfYjtIba5RJyPWOgeFTrbK7Ha7V2Kn3ZasBHYKEsQZV05OTgEd+Z6GHUe9lFSrj7tGkWlF+bPdD0j13oDeNRXpCovpucePp9TYhrdh/UilQM9sPI10IxxycI033XdJ9Dv0eSU8iUnaXn24Or6p8NWa3T7FtWmprB4jkF2dV52KB6Zxz8vWcXsuDjeWXV8L+Tu1lSaxXx1dGf8RdKu0+mFFK4qIFZtqztqqxhic8g7QR9WzLjppxy+9n4u/xfb78jpjsyY1ix+G+Pgu7BaPxtL4Q0y1VWtUHR1QVs9O0barHIAY4xgZ+vPI5ZxWVuWVuk+/n5pLp6nPmliyN+7XHC+Qfq6vTt1D7g9BL2lzXhluKiytMcH8KkY5JH1l6Ta5OMXal8bVXV/Pv5nLmxvNj4VV+grpdWXY6kKpW4sVdx5vDRgqjHYEBU+c9p4uPX/B6ns3Q4nhTnzJN/U6rpPw9pfDTVsC1ybnA3N4YPp5e3acl80zk1Ea1G2MUvmKfCXQg1Wp1l7itbbbCrE4yosOeT6FifrOmGnUvE+hyZoqWXY47nfRDOuRa3C71OVDZHqD7CZ5MNSpMhezp5Zf21S9exfp+la78HHtu4z88ekyceaReo9mwwrmfPw4Ka+sVtsDbiAA59A/qB8h2kzSTpcnmTjtdCheQSDZ4mgAu0VBZ4NKSNYoozzdI6oICXlJG0UDd5VGiRTfHRW0bqaZHCHUwJJJgBBMTJYF7MTNsyYA25kmbLBoAe3SGIKLMCQxBtLrSjKynBUgj1EUW4vcuwxWr4fyVNmmGsRha5uurrFosdgdpPHiJ7cEieipzncuU3XwPV0UINpZI8etHU6Tq6LWtaoK2RRS9ZBC147HHzEzmpRfB6MtE913a6qu/+gHVdM1oCJYabByjAnY4xyGXsw+Rm+LJxRrhnGHicdy7+aDdD0lTIWucWXo+AgLKFZeAwHfP8Tj/tuTjJfkzzsuD3eZyxrwvm32vsJde6SiJ4u0JWGHiJtFgOWPn55DAuTn3nTOoRuK+XH30R34NQmtj5+n5cduD3Ruh6VqHAtss3M7AZXsccfh+Q7wx6mWR01TF/UZsMoxjFUkuRrqVgqratc4Kl2HGFVam/5Axbd0l8R4YvLLe/gvi2YXxNrdqaXR1nbXp6Kk2jOA+wb2I9T6fvOrUT2m+jgseOWZrxTbr4djD6cxNip/UwVS3dQe/PtOKSb5NI8PczvOo6+vTUjZw+zYhxje3cuT64l1fBx4dPPUZPF06v09Dka9W7uMFiSR+pPy9YbYxXKO+Wh0kOsF+fJ092iSmpzqMeI3+SFyrEjuxH9MT93tbr4Hzetjp+uKNefkYT2zA8wpvgUkeZ5cUbxQNmmiOiAIyzoiUJjNUU3RlGiqzE8xl8wETugBVmkszbFbTM6MmwYiICBogJzJYmVduwkMEH0xKOpdQeR5SwDZBB7eh49ZvjxTvc4npaXRZ29/u7XrwdTb1FrRmqs7Mc7uMH5EfxOpSd00erjwRx8ZJUzO1HSbHDOpC2qDtzk7jjhSfaaXfVHZDVwg1Fq4vr6epiL1KwFq7AyWoQVDn8J9h6kSFBdT0PcYpLdF2n19Q/WtS6+Hq6SUbyrco9G9HI9j2meXCpeNdTzfc7W4Pny9V/gf6d106quzT2AB3rcIw7McZAx6dpUHuWxmMsCxtZI9upz3TeoWadw6gEH8aHt+nzmMOqfkdzxRyx2s6PWaoWV69m7n7pffDVhcY+pndFXKznWN4/dpdlfydnNdSu3W2tnu7c/IGY5PFNs2ceEvJJGl8MdOBJ1FoGxc7dwHp68x0Z5pOK2x6sz+r9Ta+/ykBfwoCQorUepz795pSij0McIabFt79/X/QtqNatJK1MLbe29csoPynNKDn16HFOblywyaliynUWDccEs7FjgfTMir/CjKW1wca4GHsBJ2Hcvo2CMj35kOLR8xn08oSdRaRKtJsyiSWmkTZEbpojaJVmlnRFgyYzVFd0ZZp5mJ5bJBgBDNEyWwTvJMpSBNEZEFYmBTMhiLbsSGwIru2srDupDD6g5kbqdlQe2Sl5GtpOnXsn2yhS6hsFHVWOex2n831AE3Wq93HdLheZ9Jg9o48sdk/C2Paf4tc5psrFZHbBGf2I4lvfJboTuzT+ghe+LsvZ8RpXg2KVB/MVbb+4yBKhPKvxIHpfUDqtRotYvnZUsUELYGG5f19R8jOvHOMjXD/Uad3DxLujFOuWotTayXeUrlGBW1D6MPQ/KaKNHpe696t8Vt+Pb+UcroOsFLEsrBFa2+WwkAFc4I9+0y2xjPrz5HD/AFGCWRwT48+xuaPW0eOS2+ynLMHStiue+04+cwnsjPl0KepguIvn6fMxdX8TWOXrpUk2W1vkYUjYw7ZxySBOrco+JukRn9pYY7di3V+h67rCIhD12ePgYU9lBz5uMhu0yhUlafHmRH2nifL+Q7p/i6y2tq2Cqh4O0bSAOCuJq1Rtp9RilL3iXIpSptJwQiDksx8xEiTrryy55XJ2X2ADbUDnIy5GTn3A/wCcmr5kZOXkPUaMc7kzn8TWMAO3c5kSyVwvoRJmnS9QrxWVPcYQZXPzJnPKUr8SPO1OpULj3YIyEeVFHpojVEkTRGqBsZaNosExlI1TKYjKs0i8xs8xyRTfFZDmSbhFRm5At/MKM2wqGAWWJksLCaLpz3MVqAJGCcsqgZPuZhlywx/ikkVHHKfQ62v4WppSmy8M7KoNiKw8MvksSTjkAYGPlOfJnjBq+U/L7+R0YsO90uqL6b4dpXNoxYjX13A4UbasFgB/pyQCPlOLJmc7jGVNfp3/ADNIY43Vcv8AU1el3pZp7FrVa1rsatR+Ectn6DzEftNI/wB7T+6forfLpv8A127F5Mfusq7gtdpdPYNjqvh1uzWWYxhj+RPmT/YQjlhVRbSi3bXf0S738OEkjXHLLB7k+WuF+7OJ+ItL9lsAQGyqwF/s+4tbQCexB4II59/5nqabUuUfFw/uvzPQw+00vDkfPn5nK9Y1GkYb13V2ZClBWQSSfVfT6id8UpHoLUY4w3t8GDXTkKTZh2GTWq8rn0JJkPPJNpR48zyMntPKlVovVpmNfh7twG7GAQQD3/vMZZI791UeLPUt8D2l1g2bOVIAAHZQeO0wnie7d1F/UcVZ6nQsrtZgktjORlcD/wDYSzRlFR8jJ5JdaLnQpYzWMQSAVFZz2+fvEssoJRXzG8ja6iFnTEVns3JWK17MDgswztz6+n7zphqZNKNXZ2abI4vc+o9o7vIpZMMQCR2UH2z6zaTp0mfQRk3FOS5NChLGwVAUfQkn9e0ylJLqTLLCH4mkNafSMebWGc9uScTN5Uvwo55+0MK738Bx0QABMn3JULn9JDm5Lk8zVamOaqXQGRGkYooTLo0IZ5RaKGWjRMGxlo0TK7oyrGTZMKPIcgb2x0S5AjZAhslLIhWdF8PdE+0K1j2eFUG2ggZZnxnA9uPec+fUQwpOXc2xYnM6vTdIopRQoWx925jYqF3Geyk8Dgdp5uo9pwTTi2qfPHU6sWGrTRoJ1XTWMaWVa3zzXYgRj7Ee/b0j/rYZIU4VF/Cv8DWHLDxRdkGvw2FbEtRacVv3KN3CMf4nJl0yi7g/BPj4P/BopbluSqUevr6nNX69tMLtLgMBvC5JyqE7hjHtzJeB77flTOyMFJxyL4ldF1Za6bK99atdqMgvYq5XOcDJ78D95pFTlF44rl19P05Ky4171Tl0Qrf8Y1VcKGu8MiqpgVNZuY+azOckjsOOMemZ24dPSS6Vb/mX8enQ58tW/Xr+y/kxH1BsY2MSzPzk+vt/bE3S2qjzJtuTbAdR0XjKqkjaCSy4wXG0gLu7rzg/piaQyOF11NcWZ4+F0Zg9T6aFZQUdWCglkVhXn1AfGCfrLx5JJdU19fkY5OeUUruNSEocs2AVdCCBz2PrE4rJLlcLyZz7mlaIS+tV2vX94e3c5g4SbuMuCfClTXJrdEXepS5vDXblMnGcntzOXUeGW6CsuHKp8E/YwK3CnHIO8kZK/IevvF717lYlDh8lF6AbGSzyqm4khxl9hHoCD34/b5zqx5XFNHXiye75NMdMRSGU2ZBBGWyPpjGMR+8bLnq8slTYS1pBygjmUgKiWOJLS0dCFneXRaIUxlklpSGmCsMpFpgt0odkm2ZnkWVNkQipeAErZEI7TpfxcAlWnTTjHlTYrLtZjwDgjnPqSZ52p0ubI3tyUvJrg7MWaCpOPPodNZo3dSorLBfZhjOPy5wePlPHx+ztS7cFaX5fRnorLjT5ZzvUdQEQm4gomf8AOb/Lx/Td+JD/AMQ/Wa4NM5SpcP8AX4o6dtc9DIPxkx36eoLYgVT4t7tXtOfKBtB3EYPmGP1ndDSLHFuV0+y5/fgylKG+0cpqur3W23W2Pa1yvtVa3+7WvGByAAxx7+wnd7qO2KpU11fW/wBjknqNsnzVduxbp5zWRxYwXk2YYgH8vPbj95lmVT8l6Ef1Dl6i9NRDLuBNaZCJuPGScfyR+00lJU66vqzBZex0tXYZ74H8TG+TMMjRAHrf3ksYnr+lqyOax5sZC/l3D29o4umiXFUzlLayrIxzkg9+4I4nYuYtHHJ0zT0bK21WJOSq8ntlsY/vOWaado0i0+p1J0VeMFQcYHOe3t9JlBbTqSS6Eu81QmgLmUiQDGUiQbGUgBFpaKRbdKRrFi1olmiKLKRdkEyhooxjQ7Byh2UZpkeYU3QETmMQPdFQB6bCCCCQQcgg4IPvENcGn/jeo2geM+Bnuc9/nMJYMb7dPy/Q1WfIv+Rn9Use9NjuT34OFVsqRhsDkc5+oixQjiluiqNY6rJ3doxtRU2/ncMAKwbGTjs3Hf6zVNJULJndk+KuPMQpGcDIG4nt/tJ2yvhWc0pbuQyUlF3kt5xt4/MM9gJLkpvbXQlTkkay0GwCxRtRWUY7HI9TOTdte18tm0eenQeDSiwiGKmMIrQoY1S8KGjkuvEGwqPxK7+2MEkztj0PNy9WKaKzDr/xV/8AvEmS4Hi6nfWGciR6DAsJaRLAWGXRLF3aOiQRaUIEzSholWjNEz1ktM1TBGUi0yhMsdgyY0FkZlDsVNkzo88rujoRfdAQL1iAKjSRlzZIYEB5DGTbUrjDfoRwRItroD5QhToirt4g3pydwwG+WR6zSeS4rbwzJw556DFWsXcMAlVBCqeBkkf24mUscq68sLT4NqnUZrGNoDHJA55Hrmcvu6lfkdMPwkqZaLDIZQwywGGSMDmviejbdv8A61DduMjv/B/edWN3E8/URqZmaY/eL9U+n4xG1wTj6n0BjORI9EG7S0iWK2mWiGBeAhdzKSECZpQWQGgWmeayOirI8SUilIDY8tMpSBGyWirKeJGKwEk5C0BHhEI8YgLGIZ5ZmwLiQNBkksSK63/LP1H8GTD8Qp9DIs/F+3+06V0MWbvTf8sf/fUzkydWdUPwjYmaNQ9cZQwIwCrADF+LO1X/AJ/4nTg/Czj1PY5/Sd0+v/yEuRhj6nfmcp6QN5SJYvbKJYB4yADykJgGjAiMpA3jLJ9IxgnlIEBMpFFZYH//2Q==";

	    //$photo_str = (string) $photo;
	    $filePath = FCPATH . 'assets/surveyimgs/';
	  //  $photo_str = explode( ',', $photo );
	    //$decodedFile =  base64_decode($photo_str);
	    //$mime_type = finfo_buffer(finfo_open(), $decodedFile, FILEINFO_MIME_TYPE); // extract mime type
	    //$filExtension = preg_replace('!\w+/!', '', $mime_type); // extract extension from mime type
	    $filExtension = 'txt';
	    $surveyname = "survey_".time();
	    $fileName = $surveyname.'.'.$filExtension;
	    $fileDir = $filePath.$fileName;
	    //file_put_contents($fileDir, $decodedFile);
	    file_put_contents($fileDir, $photo_str);
		$data=array('employee_id'=>$this->input->post('employee_id'),
        'office_name' =>$this->input->post('office_name'),
        'administrative_department'=>$this->input->post('administrative_department'),
        'office_type'=>$this->input->post('office_type'),
        'address'=>$this->input->post('address'),
        'pincode'=>$this->input->post('pincode'),
        'building_type'=>$this->input->post('building_type'),
        'landline'=>$this->input->post('landline'),
        'district'=>$this->input->post('district'),
        'taluk'=>$this->input->post('taluk'),
        'village'=>$this->input->post('village'),
        'block'=>$this->input->post('block'),
        'localbody_type'=>$this->input->post('localbody_type'),
        'localbody_name'=>$this->input->post('localbody_name'),
        'latitude'=>$this->input->post('latitude'),
        'longitude'=>$this->input->post('longitude'),
        'email'=>$this->input->post('email'),
        'landmark'=>$this->input->post('landmark'),
        'photo'=>$fileName,
        'contact_person'=>$this->input->post('contact_person'),
        'contact'=>$this->input->post('contact'),
        'working_start' =>$this->input->post('working_start'),
        'working_end' =>$this->input->post('working_end'),
        'surveyor_name'=>$this->input->post('surveyor_name'),
        'status'=>$this->input->post('status')
        );
		if (count($data)==0){
			$this->response("Values not supplied correctly",400);
		}else{
		$this->load->model('asset_model');
		$result=$this->asset_model->assets($data);
		 if($result=='true'){
            $this->response("success",200);
        }else{
            $this->response("failed",400);
        }
    }
	}

	// api for getting department
	public function department1_get(){
		$this->load->model("api_getter_model");
		$data=$this->api_getter_model->department();
		if($data){
			$finalData=json_encode($data);// json encoded
			$this->response($finalData,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for getting districts
	public function districts_get(){
		$this->load->model('api_getter_model');
		$data=$this->api_getter_model->districts();
		if($data){
			$finalData=json_encode($data);
			$this->response($finalData,200);
		}else{
			$this->response("Cannot get districts name",400);
		}
	}

	//api for getting the localbody
	public function localbody_get(){
		$this->load->model('api_getter_model');
		$data=$this->api_getter_model->localbody();
		if($data){
			$finalData=json_encode($data);
			$this->response($finalData,200);
		}else{
			$this->response("Cannot get Localbody Types",400);
		}
	}

	//api for getting the insitution categories
	public function categories_get(){
		$this->load->model('api_getter_model');
		$data=$this->api_getter_model->categories();
		if($data){
			$finalData=json_encode($data);
			$this->response($finalData,200);
		}else{
			$this->response("Cannot get Category Types",400);
		}
	}

	//api for giving designation on department params
	public function designation_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('department')){
			$this->response("Department not supplied",400);
		}else{
			$data=$this->api_provider_model->apidesignation($this->get('department'));
			if($data){
				$this->response($data,200);
			}else{
				$this->response("Cannot get designation");
			}
		}
	}

	//api for posting messages from mobile view
	public function messages_post(){
		$initdata=array('employee_id'=>$this->input->post('employee_id'),
        'employee_messages'=>$this->input->post('employee_messages'));
        if(empty($initdata)){
        	$this->response("EmployeeID not supplied",400);
        }else{
		$this->load->model("api_messages_model");
		$data=$this->api_messages_model->messagespost($initdata);
		if($data){
			$this->response("success",200);
		}
		else{
			$this->response(NULL,400);
		}
	}
	}

	//api for giving server file path to mobile
	public function fileviewer_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('searchText')){
			$this->response("Search Text not supplied",400);
		}
		$data=$this->api_provider_model->filegetter($this->get('searchText'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response('Empty',200);
		}
	}

	//api for giving tenderfile path to mobile
	public function tenderfileviewer_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('searchText')){
			$this->response("Serarch text not supplied",400);
		}
		$data=$this->api_provider_model->tendergetter($this->get('searchText'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Empty",200);
		}
	}
	/*
	*main section for giving out values
	*/

	//api for giving out values to mobile forms
	public function panchayath_get(){
		$this->load->model('api_provider_model');
		 if(!$this->get('districts')){
            $this->response("district supplied not correct", 400);
        }
		$district= $this->get('districts');
		$data=$this->api_provider_model->panchayath($this->get('districts'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for department register mobile
	public function department_get(){
		$this->load->model('api_provider_model');
		$data=$this->api_provider_model->departmentgetter();
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for giving out userinfo
	public function userinfo_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('username')){
			$this->response("Username is not supplied",400);
		}
		$data=$this->api_provider_model->userinfo($this->get('username'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for getting the district pachayath
	public function districtpachayath_get(){
		$this->load->model('api_provider_model');
		$data=$this->api_provider_model->districtpachayath();
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for giving away all the emergency contacts
	public function emergency_get(){
		$this->load->model('api_provider_model');
		$data=$this->api_provider_model->emergency();
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for getting all the corporations
	public function corporation_get(){
		$this->load->model('api_provider_model');
		$data=$this->api_provider_model->corporation();
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}

	//api for getting all the muncipality
	public function muncipality_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('districts')){
            $this->response("district supplied not correct", 400);
        }
        //$user = $this->api_login_model->user( $this->get('username') );     
		$data=$this->api_provider_model->muncipality($this->get('districts'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);

		}
	}

	//api for getting all the block data
	public function block_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('districts')){
			$this->response("Districts was not supplied",400);
		}
		$data=$this->api_provider_model->block($this->get('districts'));
		if($data){
			$this->response($data,200);
		}else{
			$this->response("Something went wrong",400);
		}
	}
/*
*Support functions 
*/ 
	
	//function for checking if email exist in db
	public function email($email){
		$this->load->model('model_users');
		$data=$this->model_users->emailcheck($email);
		if($data==true){
			return 0;
		}else{
			return 1;
		}
	}
// End of support functions

	//api for checking if password already exists
	public function userchecker_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('username')){
			$this->response("Username not supplied correctly",400);
		}
		$data=$this->api_provider_model->emailfactory($this->get('username'));
		if($data){
			$this->response("Username is taken",200);
		}else{
			$this->response("Username not taken",200);
		}
	}

	//api for checking if the EmployeeID already exist
	public function employeeidCheck_get(){
	$this->load->model('api_provider_model');
	if(!$this->get('employee_id')){
		$this->response("EmployeeID not supplied",400);
		}
	$data=$this->api_provider_model->employeeidfactory($this->get('employee_id'));
	if($data){
		$this->response("Employee ID is taken",200);
	}else{
		$this->response("Employee ID is not taken",200);
	}
	}

	//api for checking if mobile number is already taken
	public function mobilenumbercheck_get(){
		$this->load->model('api_provider_model');
		if(!$this->get('mobilenumber')){
			$this->response("Mobile number not provided",400);
		}
		$data=$this->api_provider_model->mobilenumberfactory($this->get('mobilenumber'));
		if($data){
			$this->response("Mobile number already exist",200);
		}else{
			$this->response("Mobile number is not taken",200);
		}
	}

	// api function for resting the password
	public function resetpassword_post(){
	$this->load->model('api_provider_model');
	if(!$this->post('username')){
		$this->response("Username not supplied",400);
	 }elseif (!$this->post('currpwd')) {
	 	$this->response('Current password not supplied',400);
	 }elseif(!$this->post('newpwd')){
	 	$this->response('New password not supplied',400);
	 }else{
	 	$oldpwd=$this->api_provider_model->oldpwdFactory($this->post('currpwd'));
	 	if($oldpwd){
	 		$data=$this->api_provider_model->resetpwdFactory($this->post('username'),$this->post('newpwd'));
	 		if($data){
	 			$this->response("Password reset successfully",200);
	 		}else{
	 			$this->response("Error in resetting password",400);
	 		}
	 	}else{
	 		$this->response("Old password supplied is incorrect",400);
	 	}
	 }
	}

	//api for changing the password
	public function changepassword_get(){
		$sender_email='vibhavapp@gmail.com';
      	$sender_pass='Vibhav@123';
		$this->load->model('api_provider_model');
		$this->load->model('model_users');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|callback_validate_credentials');
		if(!$this->get('email')){
			$this->response("Please provide Old password",400);
		}else{
			$email=($_GET['email']);
			if($this->model_users->emailcheck($email)){
				function randomString($length = 8) {
      				$length = 8;        
      				$str = "";
      				$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
      				$max = count($characters) - 1;
      				for ($i = 0; $i < $length; $i++) {
      					$rand = mt_rand(0, $max);
      					$str .= $characters[$rand];
      				}
      				return $str;
      			}
      			$password = randomString();
      			$this->load->library('email');
      			$config['protocol']    = 'smtp';
      			$config['smtp_host']    = 'ssl://smtp.gmail.com';
      			$config['smtp_port']    = '465';
      			$config['smtp_timeout'] = '7';
      			$config['smtp_user']    = $sender_email;//sender email set here
      			$config['smtp_pass']    = $sender_pass;//sender email password. also set "Access for less secure apps" to ON in sender gmail account
		          //https://www.google.com/settings/security/lesssecureapps
         		 //https://accounts.google.com/DisplayUnlockCaptcha
          		$config['charset']    = 'utf-8';
          		$config['newline']    = "\r\n";
          		$config['mailtype'] = 'text'; // or html
          		$config['validation'] = TRUE; // bool whether to validate email or not      
          		$this->email->initialize($config);
          		$this->email->from('vibhavapp@gmail@gmail.com', 'sender_name');
          		$this->email->to($email); 
          		$this->email->subject('Reset Password');
          		$this->email->message($password);  
          		$this->email->send();
          		
          		$result = $this->model_users->pwdchange($email,$password);   
          		$this->response("Password successfully changed, Please login using the password sent to your mail address",200);
			}else{
				$this->response("Error occured while changing password",400);	
			}
			
		}
	}
}


 
?>
