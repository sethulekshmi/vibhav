<?php
//FIleName:loginController-Default controller
//Date:18/10/2017
//Author:Jtihin Zacharia
	class Vibhav extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->helper('form');
		}

		public function index(){
			$data['title']='Vibhav-Home';
			//$this->load->view('sidebar');
			$this->load->view('login',$data);
			$this->load->helper('url');	
			$this->load->model('Modelqw');
			$this->Modelqw->__construct();
		}

		public function register(){
			$data['register_title']='Vibhav-Register';
			//$this->load->view('sidebar');
			$this->load->view('register',$data);
			$this->load->helper('form');
			$this->load->helper('form','url');
			$this->load->model('register_model');
			$this->register_model->__construct();
		}

		public function insert_data(){
			$this->load->model('register_model');
			$this->load->helper('form');
			$this->load->helper('form','url');
			$this->load->database();
			$form_data=$this->input->post();
			$data=array(
				'email'=>$this->input->post('email'),	
				'username'=>$this->input->post('username'),
				'password'=>$this->input->post('password'),
				'employee_type'=>$this->input->post('employee_type'),
			);
			$this->db->insert('user_name',$data);
			//$this->load->view('sidebar');
			$this->load->view('login',$data);
		}

	public function contact(){   
		$this->load->helper('form','url');
		//$this->load->view('sidebar');
		$this->load->view('contact');
	}

	public function info(){       
		$this->load->helper('form','url');
		//$this->load->view('sidebar');
		$this->load->view('info');
	}

	public function home(){   
       //  $data['home_title']='Vibhav-Home';       
		$this->load->helper('form','url');
		$this->load->view('home');
	}

	public function contactadmin(){   
			$this->load->model('messages_model');
			$this->load->helper('form','url');
			$this->load->database();
			$form_data=$this->input->post();
			$data=array(
				'name'=>$this->input->post('name'),
				'email'=>$this->input->post('email'),	
				'mobile_number'=>$this->input->post('mobile_number'),
				'subject'=>$this->input->post('subject'),
				'message'=>$this->input->post('message'),
									);
			$sql=$this->messages_model->contact_admin($data);
			if($sql){
				return true;
			}
			else
			{
				return false;
			}

		
	}
}
?>

