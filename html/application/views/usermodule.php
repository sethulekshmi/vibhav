<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title margin-lt-4">User Management Module- Super Admin</h2>
          </div>
          <div class="users-list">
            <!--user search-->
            <div id="distance"> </div>
            <!--user search results list-->       
            <?php if(empty($main_data)) {?>
              <center> <h4> <?php echo "No users found";?> </h4> <center>
            <?php }
            else{
              foreach($main_data as $data){ ?>
              <div id="searchresult" class="container">
                <div class="row" >
                  <ul class="thumbnails" style="list-style-type:none;">
                    <li class="span5 clearfix">
                      <div class="thumbnail clearfix">
                        <div class="col-md-8">
                        <div class="profile-image">
                          <img src="/assets/imgs/user1.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:90px;'>
                          </div>
                        <div class="caption">
                        <h4>      
                          <a style="cursor:pointer;" id="<?php echo $data['employee_id'];?>" onclick="passid(this);return false" >
                          <?php echo $data['fullname'];?> </a>
                          </h4>
                          <small><b>Employee ID:&nbsp; </b><b id="employee_id"><?php echo $data['employee_id'];?></b> </small> <br/>
                          <small ><b>
                           Department:&nbsp;</b><b><?php echo $data['employee_department'];?></b></small><br/>
                        </div>
                        </div>
                         <div class="col-md-4">
                        <div class="buttons">
                        <?php if($data['status']=="pending"){ ?>
                         <a href="#" data-toggle="tooltip" data-placement="top" title="Approve" style="margin-left:5px;" class="btn btn-success icon pull-right" id="<?php echo $data['employee_id'];?>"
                         onclick="approve(this);return false"><i class="fa fa-check"></i>&nbsp;Approve</a>
                          <?php } ?>
                            
                          <a href="usermodule" data-toggle="tooltip"  data-placement="top" title="Delete"  style="margin-left:5px;" class="btn btn-danger icon  pull-right" id="<?php echo $data['employee_id'];?>" onclick="remove(this)">
                          <i class="fa fa-trash-o"></i>&nbsp;Delete</a>

                          <?php if($data['status']=="active"){ ?>
                          <a href="#"data-toggle="tooltip" data-placement="top" style="margin-right:5px;" title="Block" class="btn btn-warning icon  pull-right"id="<?php echo $data['employee_id'];?>" onclick="block(this);return false"><i class="fa fa-times"></i>&nbsp;Block</a>
                          &nbsp;
                          <?php } ?>
                        </div>
                        </div>
                      </div>
                    </li>
                      </ul>
                  </div>
                </div>
              <?php }} ?>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
</div>
<!-- /#wrapper -->
     <!-- Menu Toggle Script -->
    <script>
    function remove(el){
      var response=confirm("Do you really want to delete the user?");
      if(response==true){
    	var id=el.id;
			console.log(id);
			$.ajax({
			type:'POST',
			url:"<?=base_url()?>index.php/logincontroller/deleteuser",
			data:{"id":id},
			dataType:"json",
    		success: function () {   // success callback function
       	console.log("success");
    		$(el).parent().remove();
    		},
    		error: function () { // error callback 
       		console.log("fail");
    		}
	});
    }else{
      location.reload();
    }
}
 // function for passing prof id
    function passid(el){
      var idval=el.id; 
      console.log(idval);
      post("<?=base_url()?>index.php/logincontroller/pass_id",{id:idval});
}

function approve(el){
      var idval=el.id;
      console.log(idval);
      post("<?=base_url()?>index.php/logincontroller/approve",{id:idval});
}
function block(el){
      var idval=el.id;
      console.log(idval);
      post("<?=base_url()?>index.php/logincontroller/block",{id:idval});
}
    
function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}

	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
   	});
   
    </script>
<?php $this->view('templates/footer'); ?>