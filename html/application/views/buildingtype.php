<?php $this->view('templates/header',"Building Type"); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | Building Type</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-responsive table-hover table-bordered" id="buildingtypeTable" >
              <thead>
                <tr>
                  <td>Building Types</td>
                  <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#addbuildingtype"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
                </tr>
              </thead>
              <tbody>
                 <?php foreach($buildingtype_data as $data){?>
                 <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['buildingtype_name'];?></td>
                  <td><button type="button"  id="updatebuildingtype" class="btn btn-info" data-toggle="modal"  data-target="#buildingtypeupdate"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
                </tr>
              <?php }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Modal for adding builing type-->
  <!-- Modal -->
  <div class="modal fade" id="addbuildingtype" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add a Building Type</h4>
        </div>
        <div class="modal-body">
         <form class="form-horizontal">
          <div class="form-group">
           <label class="col-sm-4 control-label">Building Type</label>
           <div class="col-sm-8"> 
            <input type="text"  id="addbuildingtypename"></br>
          </div>
        </div>
      </form>
      <script type="text/javascript">
        var x = document.getElementById("buildingtypeTable").rows.length;
        $('table tbody tr td').on('click',function(){
          $("buildingtypeupdate").modal("show");
          $("#id").val($(this).closest('tr').children()[0].textContent);
          $("#buildingtype_name").val($(this).closest('tr').children()[1].textContent);
        });
      </script>
      <div class="modal-footer">
        <a href="messages" data-dismiss="modal" class="btn btn-success" id="addbuildingtypebutton"><i class="fa fa-plus"></i>&nbsp;Add</a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>
  </div>
</div>
</div>



<script type="text/javascript">

  $('#addbuildingtypebutton').click(function() 
  {
   var buildingtype_name = $("#addbuildingtypename").val();
   $.ajax({
     type: "POST",
     url: "<?php echo base_url('index.php/logincontroller/add_buildingtype'); ?>",
     data: { 'buildingtype_name':buildingtype_name},
     success: function(response){
      location.reload();
      },
     error: function(err){
      console.log(err);
     },
    });
  });
</script>
<!--end of add_modal--> 

<!--begin of buildingtype update modal-->
<div class="modal fade" id="buildingtypeupdate" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Building Type</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
             <input type="text" disabled="true" id="id"></br>
           </div>
         </div>
         <div class="form-group">
          <label class="col-sm-4 control-label">Building Type</label>
          <div class="col-sm-8"> 
           <input type="text"  id="buildingtype_name"></br>
         </div>
       </div>
     </form>
     <div class="modal-footer">
      <a href="dataupdate" data-dismiss="modal" class="btn btn-success" id="updateButton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
      <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
      <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
    </div>
  </div>
</div>
</div>




<script type="text/javascript">

  $('#deleteButton').click(function() 
  {
   var id = $("#id").val();
   $.ajax({
     type: "POST",
     url: "<?php echo base_url('index.php/logincontroller/delete_buildingtype'); ?>",
     data: { 'id':id},
     success: function(response){
      location.reload();
     },
     error: function(err){
     console.log(err);
     },
    });
  });

  $('#updateButton').click(function() 
  {    
   var id = $("#id").val();
   var buildingtype_name = $("#buildingtype_name").val();
   $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/update_buildingtype'); ?>",
      data: { 
        'id':id,
        'buildingtype_name': buildingtype_name,
      },
      success: function(response)
      {
       location.reload()
      },
      error: function(err){
       console.log(err);
      },
    });
 });
</script>
<!--end of update modal-->
<!-- Page Content inside this  -->
<script>
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>
<?php $this->view('templates/footer'); ?>