<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | Designation</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="districtTable">
              <tr>
                <td><strong>Designation</strong></td>
                <td><strong>Department</strong></td>
                <td class="actions-col"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#adddistrict"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
              </tr> 
              <tbody>
                <?php foreach($designation_results as $data){?>
                <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['designation'];?></td>
                  <td><?php echo $data['department'];?></td>
                  <td><button type="button"  id="updatedistrict" class="btn btn-info" data-toggle="modal"  data-target="#districtupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
                </tr>
                <?php }?>
              </tbody>     
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="adddistrict" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add a district</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Designation</label>
            <div class="col-sm-8"> 
              <input type="text"  id="addesignation"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Department</label>
            <div class="col-sm-8"> 
              <input type="text"  id="addepartment"></br>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <a href="messages" data-dismiss="modal" class="btn btn-success" id="adddistrictbutton">add</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var x = document.getElementById("districtTable").rows.length;
$('table tbody tr  td').on('click',function(){
  $("districtupdateModal").modal("show");
  $("#id").val($(this).closest('tr').children()[0].textContent);
  $("#designation_name").val($(this).closest('tr').children()[1].textContent);
  $('#department_name').val($(this).closest('tr').children()[2].textContent);
});
function timmedinput(x){
  return x.replace(/(<([^>]+)>)()/ig,"");
}

$('#adddistrictbutton').click(function() {
  debugger;
  var designation_name = $("#addesignation").val();
  var department_name=$('#addepartment').val();
  var sanitized=timmedinput(designation_name);
  var sanitizedin=timmedinput(department_name);
  if(sanitized==null || sanitized===""){
    alert("Enter a District");
  }else{
    console.log(sanitized);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/add_designation'); ?>",
      data:{
        'designation_name':sanitized,
        'department_name':sanitizedin
      },
      success: function(response){
        console.log("success");
        location.reload();
      },
      error: function(err){
        console.log('error');
        console.log(err);
      },
    });
  }
});
</script>
<!-- Modal -->
<div class="modal fade" id="districtupdateModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">update district</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group"  style="display:none" >
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
               <input type="text" disabled="true" id="id"></br>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-4 control-label">Designation Name:</label>
            <div class="col-sm-8"> 
               <input type="text"  id="designation_name"></br>
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-4 control-label">Department Name:</label>
            <div class="col-sm-8"> 
               <input type="text"  id="department_name"></br>
            </div>
         </div>
        </form>
      </div>       
      <div class="modal-footer">
        <a  data-dismiss="modal" class="btn btn-success" id="updateButton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
        <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>
    
  </div>
</div>
<script type="text/javascript">
function timmedinput(x){
    return x.replace(/(<([^>]+)>)/ig,"");
  }

$('#deleteButton').click(function() {
  var confi=confirm('Are you sure you want to Delete?');
  if(confi==true){
    var id = $("#id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/delete_designation'); ?>",
      data: { 'id':id},
      success: function(response)
      {
        console.log("success");
        location.reload();
      },
      error: function(err)
      {
        console.log('error');
        console.log(err);
       },
    });
  }else{
   console.log("Designation Deletion blocked by user");
  }
});
$('#updateButton').click(function() {
  debugger; 
  var id = $("#id").val();
  var designation_name = $("#designation_name").val();
  var department_name=$("#department_name").val();
  var sanitizedInput=timmedinput(designation_name);
  var sanitizedin=timmedinput(department_name);
  if(sanitizedInput.length===0){
    alert("Enter a valid District Name");
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/update_designation'); ?>",
      data: { 
        'id':id,
        'designation_name':sanitizedInput,
        'department_name':sanitizedin
      },
      success: function(response)
      {
        console.log("success");
        location.reload()
      },
      error: function(err){
        console.log('error');
      },
    });
  }
});
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>
<?php $this->view('templates/footer'); ?>