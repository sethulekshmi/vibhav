<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title margin-lt-4">Super Admin- Messages module</h2>
          </div>
          <div class="messages-list">
          <?php if(empty($main_data)) {?>
          <center> <h4> <?php echo "No Messages found";?> </h4> <center>
            <?php }
            else{?>
            <?php foreach($main_data as $data){ ?>
            <div id="searchresult" class="">
             <div class="row">
              <ul class="thumbnails"style="list-style-type:none;">
                <li class="span5 clearfix">
                  <div class="thumbnail clearfix">
                  <div class="col-md-10">
                    <div class="profile-image">
                      <img src="/assets/imgs/user1.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:90px;'>
                    </div>
                    <div class="caption">
                      <p><b>Employee ID:&nbsp; </b><span id="employee_id"><?php echo $data['employee_id'];?></span></p>
                      <p id="employee_messages"><b>Employee Message:&nbsp;</b><?php echo $data['employee_messages'];?></p>
                      <p id="message_time"><?php echo $data['message_time'];?></p>
                  </div>
                  </div>
                  <div class="col-md-2">
                    <div class="buttons">
                      <a href="messages" onclick="remove(this)" data-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger pull-right"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <?php }}?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Menu Toggle Script -->
<script>
  function remove(el){
    debugger;
    var id=$('#employee_id').text();
    var time=$('#message_time').text();
    console.log(id);
    var c_obj = $(this).parents("div");
    c_obj.remove();
    $.ajax({
      type:'POST',
      url:"<?=base_url()?>index.php/logincontroller/deletemessage",
      data:{"id":id,'time':time},
        success: function (result) {   // success callback function
          console.log("success");
          $(el).parent().remove();
        },
        error: function (err) { // error callback 
          console.log("fail");
        }
      }); 
  }
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
</script>
<?php $this->view('templates/footer'); ?>