<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin| &nbsp;Categories</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="category_table" >
     <tr><td><strong>Categories</strong></td>  
     <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#addcategorymodal"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
     </tr> 
<tbody>
<?php foreach($category_data as $data){?>
            <tr>
            <td style="display:none"><?php echo $data['id'];?></td>
            <td><?php echo $data['institution_category'];?></td>            
           <td><button type="button"  id="updateinstitution_category" class="btn btn-info btn-md" data-toggle="modal"  data-target="#categoryupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
         </tr>
         </tbody>
        <?php }?> 
</table> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 <!-- Modal -->
  <div class="modal fade" id="addcategorymodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add a institution category</h4>
        </div>
        <div class="modal-body">
           <form class="form-horizontal">
              <div class="form-group">
                 <label class="col-sm-4 control-label">Category</label>
                     <div class="col-sm-8"> 
                        <input type="text"  id="add_institution_category"></br>
                    </div>
              </div>
            </form>
 <script type="text/javascript">
                      var x = document.getElementById("category_table").rows.length;
                     $('table tbody tr  td').on('click',function()
                            {
                                $("categoryupdateModal").modal("show");
                                $("#id").val($(this).closest('tr').children()[0].textContent);
                                $("#category_name").val($(this).closest('tr').children()[1].textContent);
                            });
                  </script>

        <div class="modal-footer">

            <a href="messages" data-dismiss="modal" class="btn btn-success" id="addcategorybutton"><i class="fa fa-plus"></i>&nbsp;Add</a>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
        </div>
      </div>
      
    </div>
  </div>
  </div>

   <script type="text/javascript">
 function timmedinput(x){
    return x.replace(/(<([^>]+)>)/ig,"");
  }
$('#addcategorybutton').click(function() {

                 var institution_category = $("#add_institution_category").val();
                 var sanitized=timmedinput(institution_category);
                 if(sanitized==null || sanitized===""){
                  alert("Please provide an Institution category");
                }else{
                 console.log(sanitized);
                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/add_category'); ?>",
                             data: { 'institution_category':sanitized},

                              
                             success: function(response)
                                     {
                                        console.log("success");
                                        location.reload();
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },

                        });
                 }
            });
</script>
 <!-- Modal -->
  <div class="modal fade" id="categoryupdateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update category</h4>
        </div>
        <div class="modal-body">
          
        
            <form class="form-horizontal">
              <div class="form-group" style="display:none">
                <label class="col-sm-4 control-label">ID</label>
                  <div class="col-sm-8"> 
                     <input type="text" disabled="true" id="id"></br>
                  </div>
             </div>
             <div class="form-group">
                <label class="col-sm-4 control-label">Category</label>
                  <div class="col-sm-8"> 
                     <input type="text"  id="category_name"></br>
                  </div>
             </div>
            </form>      
           <div class="modal-footer">
          <a href="dataupdate" data-dismiss="modal" class="btn btn-success" id="category_updateButton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
          <button  data-dismiss="modal" type="button" id="category_deleteButton" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
          </div>
      </div>
    </div>
    </div>
  </div>
<!--end of update category modal-->
<script type="text/javascript">
function timmedinput(x){
    return x.replace(/(<([^>]+)>)/ig,"");
  }
  
$('#category_deleteButton').click(function() {
                 var id = $("#id").val();
                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/delete_category'); ?>",
                             data: {'id':id},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                          location.reload();
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });
$('#category_updateButton').click(function() {    
                 var id = $("#id").val();
                var institution_category = $("#category_name").val();
              var sanatizedInput=timmedinput(institution_category);
              if(sanatizedInput.length===0){
                alert("Please enter a valid Institution category");
              }else{
                 $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('index.php/logincontroller/update_category'); ?>",
                          data: { 
                                  'id':id,
                                  'institution_category':sanatizedInput
                                },
                     success: function(data)
                       {
                         console.log("success");
                          location.reload();
                        },
                    error: function(err){
                                       console.log('error');
                                        },
                              });
                 }
      });

</script>













<!-- Page Content inside this  -->

     <script>

  
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

 
   
    </script>

</body>
</html>
