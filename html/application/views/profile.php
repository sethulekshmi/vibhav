<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-md-10 main-content">
          <div class="row well">
            <div class="col-md-2">
              <ul class="nav nav-pills nav-stacked well">
                <li  class="active"><a href="#"><i class="fa fa-user-circle"></i> Profile</a></li>
                <li><a href="#" id="<?php echo $profile_data['employee_id'];?>" onclick="usr_asset(this);return false">
                  <i class="fa fa-snowflake-o"></i>&nbsp;Assets</a>
                </li>
                <li><a href="#" id="<?php echo $profile_data['employee_id'];?>" onclick="remove(this);return false">
                  <i class="fa fa-trash"></i>&nbsp;Delete User</a></li>
              </ul>
            </div>
            <div class="col-md-8">
              <div class="panel">
                <img class="pic img" id="profile" style="height: 120px;
                width: 120px;" src="/assets/imgs/user1.png" alt="...">
              </div>
            </div>
          </div>
          <div class="user-assets">
            <table class="table table-hover table-bordered" id="myTable">
           <tr>
              <td style="display:none"><strong>Id</strong></td> 
              <td><strong> Employee Id</strong></td>
              <td><strong>Office Name</strong></td>
              <td><strong>Administrative Department</strong></td>
              <td><strong>Office Type</strong></td>
              <td><strong>Address</strong></td>
              <td><strong>Pincode</strong></td>
              <td><strong>Building Type</strong></td> 
              <td><strong>Landline</strong></td>
              <td><strong>District</strong></td>
              <td><strong>Taluk</strong></td>
              <td><strong>Village</strong></td>
              <td><strong>Block</strong></td>
            </tr> 
            <tbody>
              <?php if(empty($asset_data)){?>
              <tr>
                <td colspan="24">
                  <div style="margin-left:400px; margin-top:40px;" ><p><h3>No assets</h3></p></div>
                </td>
              </tr>
              <?php }
              else{
               foreach($asset_data as $data){ ?>           
               <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['employee_id'];?></td>
                  <td><?php echo $data['office_name'];?></td>
                  <td><?php echo $data['administrative_department'];?></td>  
                  <td><?php echo $data['office_type'];?></td>
                  <td><?php echo $data['address'];?></td>
                  <td><?php echo $data['pincode'];?></td>
                  <td><?php echo $data['building_type'];?></td>
                  <td><?php echo $data['landline'];?></td>
                  <td><?php echo $data['district'];?></td>
                  <td><?php echo $data['taluk'];?></td>
                  <td><?php echo $data['village'];?></td>
                  <td><?php echo $data['block'];?></td>
                </tr>
                <?php }}?> 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>  
<script type="text/javascript">
    function remove(el){
      var deluser=confirm("Do you want to delete the user");
      if(deluser==true){
        var idval=el.id;     
    console.log(idval);
      post("<?=base_url()?>index.php/logincontroller/deleteuser",{id:idval});
    }else{
      console.log("User delete blocked by user");
    }
    
}

    function usr_asset(el){
      var idval=el.id;   
      console.log(idval);
      debugger;
      post("<?=base_url()?>index.php/logincontroller/user_asset",{id:idval});
}

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}
   </script>
<?php $this->view('templates/footer'); ?>