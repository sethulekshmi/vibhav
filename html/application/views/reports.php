<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin- Reports Generation Module</h2>
          </div>
          <div>
            <!--  datepicker Script -->
            <!--Dry Code-->
            <form id="datepickform"  class="form-inline" action="<?=base_url();?>index.php/logincontroller/report_date" method="POST" onsubmit="cleared()">
              <div class="form-group">
                <label for="date">From Date:</label> 
                <input type="text" class="form-control" id="from_date" name="from_date" required>
              </div>
              <div class="form-group">
                <label for="date">To Date:</label>
                <input type="text" class="form-control" id="to_date" name="to_date" required >
              </div>
              <div class="form-group">
                <button class="btn btn-info" type="submit" data-toggle="tooltip" data-placement="top" title="Generate Report" id="report_generate">Generate Report</button>
              </div>
            </form> 
            <i>Format(YYYY/MM/DD)</i>
            <script>
              function cleared(){
                  $("#centred").hide(); // hide body div tag
              }
            </script>
            <script>
              $(function(){
            $("#from_date").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#to_date").datepicker({ dateFormat: 'yy-mm-dd' });
              });
            </script>
            </div>
            <?php if ($this->session->flashdata('entry')) { ?>
                  <div class="alert alert-danger" id="centred"> <center><h4> <?= $this->session->flashdata('entry') ?> </h4></center>  </div>
            <?php } ?>                   
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /#wrapper -->
  <!-- Menu Toggle Script -->
  <!-- for disable buttons  -->    
     <script>
    $(document).ready(function(){
       validate();
   $('input').on('keyup', validate);
    });
function validate() {
  var inputsWithValues = 0;
  
  // get all input fields except for type='submit'
  var myInputs = $("input:not([type='submit'])");

  myInputs.each(function(e) {
    // if it has a value, increment the counter
    if ($(this).val()) {
      inputsWithValues += 1;
                        }
  });

  if (inputsWithValues == myInputs.length) {
    $("input[type=submit]").prop("disabled", false);
  } else {
    $("input[type=submit]").prop("disabled", true);
  }
}
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
<?php $this->view('templates/footer'); ?>