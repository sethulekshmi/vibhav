<?php $this->view('templates/header',$pageTitle); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row content">
          <div class="col-sm-2 sidenav">
            <?php $this->view('templates/adminsidebar'); ?>
          </div>
          <div class="col-sm-10 main-content">
            <div class="header-maker">
              <h2 class="content-title">Super Admin |&nbsp;Analytics Module</h2>
            </div>
            <div class="container"> 
              <div id="piechart">
                <script type="text/javascript">
                // Load google charts
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                ngConstructor = "test".constructor;
                var arrayConstructor = [].constructor;
                var objectConstructor = {}.constructor;
                function whatIsIt(object) {
                    if (object === null) {
                        return "null";
                    }else if (object === undefined) {
                        return "undefined";
                    }else if (object.constructor === stringConstructor) {
                        return "String";
                    }else if (object.constructor === arrayConstructor) {
                        return "Array";
                    }else if (object.constructor === objectConstructor) {
                        return "Object";
                    }else {
                        return "don't know";
                    }
                }
                function drawChart() {

                  var abc = JSON.parse('<?php echo json_encode($main_data); ?>');
                  var data2="[";
                  for ( i=0;i< abc.length-1;i++) {   
                    data2+="[\""+abc[i].employee_department+"\","+ abc[i].count+"],";
                  }
                    data2+="[\""+abc[abc.length-1].employee_department+"\","+ abc[abc.length-1].count+"]";
                    data2+="]";

                  var parsedData=JSON.parse(data2);
                  var data = google.visualization.arrayToDataTable(parsedData,true);
                  // Optional; add a title and set the width and height of the chart
                  var options = {'title':'User Analytics', 'width':550, 'height':400};

                  // Display the chart inside the <div> element with id="piechart"
                  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                  chart.draw(data, options);
                }
                $("#menu-toggle").click(function(e) {
                        e.preventDefault();
                        $("#wrapper").toggleClass("toggled");
                    });
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $this->view('templates/footer'); ?>