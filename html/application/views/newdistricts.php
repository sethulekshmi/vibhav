<!DOCTYPE html>
<head>
<meta charset='utf-8'>
        <!-- Dependend CSS Files -->
        <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!--Dependend JS Files-->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/script.js"></script>
        <title>Super Admin-edit</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand"style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
      <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<div id="wrapper">
         <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  
          </div>
          <div class="head_title">
          <h2 style="padding-left:245px;color:white;">Super Admin | Districts
          </div>
        </div>
    </div>
</div>
<!-- Page Content inside this  -->
<!--beginning of district modal-->
<table class="table table-hover table-bordered" id="districtTable" style="margin-left:20px;" >
     <tr><td><strong>Districts</strong></td>  <td><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#adddistrict">Add</button></td></tr> 
   <tbody>
           <?php foreach($new_districts as $data){?>
           <tr>
            <td style="display:none"><?php echo $data['id'];?></td>
            <td><?php echo $data['district_name'];?></td>
            <td><button type="button"  id="updatedistrict" class="btn btn-info btn-md" data-toggle="modal"  data-target="#districtupdateModal">Update</button></td>
           
           </tr>
    </tbody>
           <?php }?> 
</table>
</div>