<?php $this->view('templates/header','Signup'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('sidebar'); ?>
        </div>
        <div class="col-sm-10 login-content">
          <div class="header-maker">
            <h2 class="content-title">Signup</h2>
          </div>
          <form id="frmRegister">
            <div class="form-group" >
              <label for="email">Email:</label>
              <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
              <span style="display:none;">Email already exist</span>
              <span class="spnemail" id="spnemail"></span>
            </div>
            <div class="form-group">
              <label for="username">Username:</label>
              <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" required>
              <span class="spnFirstName" id="spnFirstName"></span>
            </div>
            <div class="form-group">
              <label for="pwd">Password:</label>
              <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required><span class="errorpassword" id="errorpassword"></span>
            </div>
            <div class="form-group">
              <label for="pwd">Repeat Password:</label>
              <input type="password" class="form-control" id="re_pwd" placeholder="Repeat Enter password" name="re_pwd" required>
              <span id="confirmpassword"></span>
            </div>
            <div class="form-group">
              <label for="employee">Select Employee Type:</label>
                <select class="form-control" id="employee" name='role'>
                  <option>Super Admin</option>
                  <option>KSDI Admin</option>
                  <option>Akshya Admin</option>
                </select>
              <span class="selection" id="selection" name="selection"></span>
            </div>
            <div><p>Already a user?</p></div>
            <a class="btn btn-success" href="<?=base_url()?>index.php/logincontroller/" role="button">Login</a>
            <button type="button" id="submit" class="btn btn-primary pull-right" >Signup</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
      <script type="text/javascript">
      function re_direct(){
        window.location.replace("http://www.localhost/index.php/vibhav");
      }
      // $("#username").change(function(){
      //   var email=$("#email").val();
      //   $.ajax({
      //     type:"POST",
      //     url:'<?php echo base_url("index.php/logincontroller/email_comparison");?>',
      //     data:{
      //       'email':email
      //     },
      //     success:function(data){
      //       if(data==false){
      //         alert("Email already exists");
      //         location.reload();
      //       }else{
      //         console.log(email);
      //       }
      //     },
      //     error:function(err){
      //       alert("Something went wrong");
      //     }
      //   })
      // });

      // $("#pwd").change(function(){
      //   var username=$("#username").val();
      //   $.ajax({
      //     type:"POST",
      //     url:'<?php echo base_url("index.php/logincontroller/username_exist");?>',
      //     data:{
      //       'username':username
      //     },
      //     success:function(data){
      //       if(data==false){
      //         alert("Username already exists");
      //         location.reload();
      //       }else{
      //         console.log(username);
      //       }
      //     },
      //     error:function(err){
      //       alert("Something went wrong");
      //     }
      //   })
      // })

         $("#submit").click(function(){
          var email=$('#email').val();
          var username=$('#username').val();
          var password=$('#pwd').val();
          var employee=$('#employee').find(":selected").text();
          $.ajax({
            type:"POST",
            url:'<?php echo base_url("index.php/vibhav/insert_data");?>',
            data:{
              'email':email,
              'username':username,
              'password':password,
              'employee_type':employee
            },
            success:function(data){
              alert("User successfully Registered");
              window.location.replace('<?php echo base_url("index.php/vibhav");?>');
            },
            error:function(err){
              alert("Error in registering a user");
            }
          })
         });

     </script>
<?php $this->view('templates/footer'); ?>