<!-- nav bar in Templates -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img src="/assets/imgs/logo.png" alt="Vibhav Logo" class="pull-left span2 clearfix">
      </a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#" id="navbarDropdownMenuLink" aria-haspopup="true" aria-expanded="false">Admin
          <i class="fa fa-chevron-down"></i></a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a href="change"><i class="fa fa-key"></i>&nbsp;&nbsp;Change Password</a></li>
            <li><a href="logout"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Logout</a></li>
          </ul>
        </li>
      </ul>
  </div>
</nav>