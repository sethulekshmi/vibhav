<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <!--<li class="sidebar-brand">
            <a href="#" id="main_li">Dashboard</a>
        </li>-->
        <li>
            <a href="admin" data-toggle="tooltip" data-placement="top" title="Admin Home Page"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;Home</a>
        </li>
        <li>
            <a href="usermodule" data-toggle="tooltip" data-placement="top" title="Admin User Management Module"><i class="fa fa-user" aria-hidden="true" class="dcjq-parent active">
            </i>&nbsp;&nbsp;User Module</a>
        </li>
        <li>
            <a href="reports" data-toggle="tooltip" data-placement="top" title="Admin Reports Generation Module"><i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Reports</a>
        </li>
        <li>    
            <a href="analytics" data-toggle="tooltip" data-placement="top" title="Admin Analytics Module"><i class="fa fa-line-chart" aria-hidden="true"></i>&nbsp;&nbsp;Analytics</a>
        </li>
        <li>
            <a href="dataupdate" data-toggle="tooltip" data-placement="top" title="Admin Data Update Module"><i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp;Data Update</a>
        </li>
        <li>
            <a href="messages" data-toggle="tooltip" data-placement="top" title="Admin Messages Module"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;&nbsp;Messages</a>
        </li>
        <li>
            <a href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1">
                <i class="fa fa-mobile fa-lg"></i>&nbsp;&nbsp;Mobile Data Update
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-down"></i>
            </a>
            <ul id="submenu-1" class="collapse">
                <li>
                    <a href="districts" data-toggle="tooltip" data-placement="top" title="Admin Districts Update Module"><i class="fa fa-user-secret" aria-hidden="true"></i>&nbsp;&nbsp;Districts</a>
                </li>
                <li>
                    <a href="districtpanchayath" data-toggle="tooltip" data-placement="top" title="Admin District Panchayath Update Module"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;District Panchayath</a>
                </li>
                <li>
                    <a href="corporation" data-toggle="tooltip" data-placement="top" title="Admin Corporation Update Module"><i class="fa fa-object-group" aria-hidden="true"></i>&nbsp;&nbsp;Corporation</a>
                </li>
                <li>
                    <a href="blockpanchayath" data-toggle="tooltip" data-placement="top" title="Admin Block Panchayath Update Module"><i class="fa fa-th" aria-hidden="true"></i>&nbsp;&nbsp;Block Panchayath</a>
                </li>
                <li>
                    <a href="gramapanchayath" data-toggle="tooltip" data-placement="top" title="Admin Grama Panchayath Update Module"><i class="fa fa-map-pin" aria-hidden="true"></i>&nbsp;&nbsp;Grama Panchayath</a>
                </li>
                <li>
                    <a href="muncipality" data-toggle="tooltip" data-placement="top" title="Admin District Panchayath Update Module"><i class="fa fa-street-view" aria-hidden="true"></i>&nbsp;&nbsp;Muncipality</a>
                </li>
                <li>
                    <a href="category" data-toggle="tooltip" data-placement="top" title="Admin Category Update Module"><i class="fa fa-chain" aria-hidden="true"></i>&nbsp;&nbsp;Categories</a>
                </li>
                <li>
                    <a href="localbody" data-toggle="tooltip" data-placement="top" title="Admin Localbody Update Module" ><i class="fa fa-building-o" aria-hidden="true"></i>&nbsp;&nbsp;Localbody</a>
                </li>
                <li>
                    <a href="department" data-toggle="tooltip" data-placement="bottom" title="Admin Department Update Module"><i class="fa fa-university" aria-hidden="true"></i>&nbsp;&nbsp;Department</a>
                </li>
                <li>
                    <a href="designation" data-toggle="tooltip" data-placement="bottom" title="Admin Designation Update Module"><i class="fa fa-shield" aria-hidden="true"></i>&nbsp;&nbsp;Designation</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="emergency" data-toggle="tooltip" data-placement="top" title="Admin Emergency Number Update Module"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;Emergency Numbers</a>
        </li>
        <li>
            <a href="fileupload" data-toggle="tooltip" data-placement="top" title="Admin Help documents upload"><i class="fa fa-question-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Help Documents</a>
        </li>
        <li>
            <a href="govtenders" data-toggle="tooltip" data-placement="top" title="Admin Tenders Documents Upload"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;&nbsp;Goverment Tenders</a>
        </li>
        <li>
            <a href="contact" data-toggle="tooltip" data-placement="top" title="All the Contacted People"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;Request from Public</a>
        </li>
    </ul>
</div>
