<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | Contact Numbers Update</h2>
          </div>
          <table class="table table-hover table-bordered" id="contactTable" >
             <tr><td><strong>Contact Name</strong>
             <td><strong>Contact Number</strong></td>
             <td><button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#adddistrict"><i class="fa fa-plus"></i>&nbsp;Add</button></td></tr> 
             <tbody>
               <?php foreach($emergency_numbers as $data){?>
               <tr>
                <td style="display:none"><?php echo $data['id'];?></td>
                <td><?php echo $data['contactname'];?></td>
                <td><?php echo $data['contactnumber'];?></td>
                <td><button type="button"  id="updatedistrict" class="btn btn-info btn-md" data-toggle="modal"  data-target="#contactupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
               </tr>
               <?php }?>      
              </tbody>
           </table>
        </div> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="adddistrict" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add a Contact</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
             <label class="col-sm-4 control-label">Contact Name</label>
              <div class="col-sm-8"> 
                <input type="text"  id="addcontactname"></br>
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Contact Number</label>
            <div class="col-sm-8"> 
              <input type="text"  id="addcontactnumber"></br>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <a href="messages" data-dismiss="modal" class="btn btn-success" id="addcontact"><i class="fa fa-plus"></i>&nbsp;Add</a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
  var x = document.getElementById("contactTable").rows.length;
   $('table tbody tr  td').on('click',function(){
              $("contactupdateModal").modal("show");
              $("#id").val($(this).closest('tr').children()[0].textContent);
              $("#contact_name").val($(this).closest('tr').children()[2].textContent);
              $("#contact_number").val($(this).closest('tr').children()[1].textContent);

  });
  function timmedinput(x){
    return x.replace(/(<([^>]+)>)()/ig,"");
  }
  function timcontactnumber(y){
    return y.replace(/(<[^>]+>)()/ig,"");
  }

$('#addcontact').click(function() {
  debugger;
  var contact_name = $("#addcontactname").val();
  var contact_number=$("#addcontactnumber").val();
  var sanitized=timmedinput(contact_name);
  var santizednumber=timcontactnumber(contact_number);
  if(sanitized==null || sanitized==="" || santizednumber==null || santizednumber==""){
    alert("Enter a Contact to Proceed");
  }else{
    console.log(sanitized,santizednumber);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/add_contact'); ?>",
      data: {
      'contactname':sanitized,
      'contactnumber':santizednumber
      },
      success: function(response){
        console.log("success");
        location.reload();
      },
      error: function(err){
       console.log(err);
       location.reload();
      },
    });
  }
});
</script>
 <!-- Modal -->
  <div class="modal fade" id="contactupdateModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Contact</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="form-group"  style="display:none" >
              <label class="col-sm-4 control-label">ID</label>
                <div class="col-sm-8"> 
                   <input type="text" disabled="true" id="id"></br>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Contact Person</label>
              <div class="col-sm-8"> 
                <input type="text"  id="contact_number"></br>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label">Contact Number</label>
              <div class="col-sm-8"> 
                <input type="text"  id="contact_name"></br>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <a  data-dismiss="modal" class="btn btn-success" id="updateButton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
          <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
        </div>
      </div>
    </div> 
  </div>
</div>
<script type="text/javascript">
  function timmedinput(x){
    return x.replace(/(<([^>]+)>)/ig,"");
  }
  $('#deleteButton').click(function() {
    debugger;
    var id = $("#id").val();
    var contact_person=$('#contact_number').val();
    var contact_number=$("#contact_name").val();
    $.ajax({
     type: "POST",
     url: "<?php echo base_url('index.php/logincontroller/delete_contact'); ?>",
     data: {
      'id':id,
      'contactname':contact_person,
      'contact_number':contact_number
      },
      success: function(response)
     {
        console.log("success");
        location.reload();
      },
      error: function(err)
      {
         console.log('error');
         console.log(err);
         location.reload();
       },
    });
  });
  $('#updateButton').click(function() {  
    debugger;  
    var id = $("#id").val();
    var contact_person=$('#contact_number').val();
    var contact_number=$("#contact_name").val();
    var sanitizedInput=timmedinput(contact_person);
    if(sanitizedInput.length===0){
      alert("Enter a valid Contact Name");
    }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/update_contact'); ?>",
      data: { 
        'id':id,
        'contactname':sanitizedInput,
        'contactnumber':contact_number   
      },
      success: function(response){
       console.log("success");
       location.reload();
      },
      error: function(err){
        console.log('error');
        location.reload();
      },
    });
    }  
  });
</script>
<?php $this->view('templates/footer'); ?>