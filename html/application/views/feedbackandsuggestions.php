<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                  <i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
<!--user search results list ends-->
    <!-- /#wrapper -->
     <!-- Menu Toggle Script -->

     <script>

  
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


   
    </script>
<?php $this->view('templates/footer'); ?>