<?php $this->view('templates/header','Forgot Password'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('sidebar'); ?>
        </div>
        <div class="col-sm-10 login-content">
          <div class="header-maker">
            <h2 class="content-title">Forgot Password</h2>
          </div>
          <div>
            <form class="" action="<?=base_url();?>index.php/logincontroller/forgot_password" method="POST">
              <div class="form-group" >
                <label for="email">Email:&nbsp;</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter your Email Address">
              </div>
              <div class="form-group">
                <button class="btn btn-info" type="submit" id="email1 ">Submit</button>
              </div>
            </form>
            <?php if ($this->session->flashdata('entry')) { ?>
              <div class="alert alert-danger" id="centred"> <center><h4> <?= $this->session->flashdata('entry') ?> </h4></center>  </div>
            <?php } ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>      
<?php $this->view('templates/footer'); ?>