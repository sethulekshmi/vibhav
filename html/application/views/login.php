<?php $this->view('templates/header','Login'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('sidebar'); ?>
        </div>
        <div class="col-sm-10 login-content">
          <div class="header-maker">
            <h2 class="content-title">Login</h2>
          </div>
          <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger"> 
              <center> <?= $this->session->flashdata('error') ?> </center>  
            </div>
          <?php } ?>
          <div class="login">
            <?php echo form_open('logincontroller/index');?>
            <div class="form-group">
              <label class="control-label col-sm-2" for="user_name">Username:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="pwd">Password:</label>
              <div class="col-sm-10"> 
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-2 col-sm-10">
                <div>
                  <a href="<?=base_url()?>index.php/logincontroller/doforget">Forgot password</a>
                </div>
              </div>
            </div>
            <div class="form-group"> 
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Login</button>
              </div>
            </div>
            <?php echo form_close();?>
            <label>New to Vibhav?&nbsp;&nbsp;<a href="<?=base_url()?>index.php/logincontroller/register">Register Now</a></label>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->view('templates/footer'); ?>