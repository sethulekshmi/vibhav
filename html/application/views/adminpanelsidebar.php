<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#" id="main_li">Dashboard</a>
        </li>
        <li>
            <a href="admin" data-toggle="tooltip" data-placement="top" title="Admin Home Page"><i class="fa fa-home fa-lg" aria-hidden="true"></i>&nbsp;Home</a>
        </li>
        <li>
            <a href="usermodule" data-toggle="tooltip" data-placement="top" title="Admin User Management Module"><i class="fa fa-user fa-lg" aria-hidden="true" class="dcjq-parent active">
            </i> <span>&nbsp;User Module</span></a>
        </li>
        <li>
            <a href="reports" data-toggle="tooltip" data-placement="top" title="Admin Reports Generation Module"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"></i>&nbsp;Reports</a>
        </li>
        <li>    
            <a href="analytics" data-toggle="tooltip" data-placement="top" title="Admin Analytics Module"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i>&nbsp;Analytics</a>
        </li>
        <li>
            <a href="dataupdate" data-toggle="tooltip" data-placement="top" title="Admin Data Update Module"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Data Update</a>
        </li>
        <li>
            <a href="messages" data-toggle="tooltip" data-placement="top" title="Admin Messages Module"><i class="fa fa-comments fa-lg" aria-hidden="true"></i>&nbsp;Messages</a>
        </li>
        <li>
            <a href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1">
                <i class="fa fa-lg fa-search"></i>Mobile Data Update
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-down"></i>
            </a>
            <ul id="submenu-1" class="collapse">
                <li>
                    <a href="districts" data-toggle="tooltip" data-placement="top" title="Admin Districts Update Module"><i class="fa fa-flag-o fa-lg" aria-hidden="true"></i>&nbsp;Districts</a>
                </li>
                <li>
                    <a href="districtpanchayath" data-toggle="tooltip" data-placement="top" title="Admin District Panchayath Update Module"><i class="fa fa-flag-o fa-lg" aria-hidden="true"></i>&nbsp;District Panchayath</a>
                </li>
                <li>
                    <a href="corporation" data-toggle="tooltip" data-placement="top" title="Admin Corporation Update Module"><i class="fa fa-gavel" aria-hidden="true"></i>&nbsp;Corporation</a>
                </li>
                <li>
                    <a href="blockpanchayath" data-toggle="tooltip" data-placement="top" title="Admin Block Panchayath Update Module"><i class="fa fa-flag-o fa-lg" aria-hidden="true"></i>&nbsp;Block Panchayath</a>
                </li>
                <li>
                    <a href="gramapanchayath" data-toggle="tooltip" data-placement="top" title="Admin Grama Panchayath Update Module"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Grama Panchayath</a>
                </li>
                <li>
                    <a href="muncipality" data-toggle="tooltip" data-placement="top" title="Admin District Panchayath Update Module"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;Muncipality</a>
                </li>
                <li>
                    <a href="category" data-toggle="tooltip" data-placement="top" title="Admin Category Update Module"><i class="fa fa-th-list fa-lg" aria-hidden="true"></i>&nbsp;Categories</a>
                </li>
                <li>
                    <a href="localbody" data-toggle="tooltip" data-placement="top" title="Admin Localbody Update Module" ><i class="fa fa-university" aria-hidden="true"></i>&nbsp;Localbody</a>
                </li>
                <li>
                    <a href="department" data-toggle="tooltip" data-placement="bottom" title="Admin Department Update Module"><i class="fa fa-building-o fa-lg" aria-hidden="true"></i>&nbsp;Department</a>
                </li>
                <li>
                    <a href="designation" data-toggle="tooltip" data-placement="bottom" title="Admin Designation Update Module"><i class="fa fa-star" aria-hidden="true"></i>&nbsp;Designation</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="emergency" data-toggle="tooltip" data-placement="top" title="Admin Emergency Number Update Module"><i class="fa fa-ambulance" aria-hidden="true"></i>&nbsp;Emergency Numbers</a>
        </li>
        <li>
            <a href="fileupload" data-toggle="tooltip" data-placement="top" title="Admin Help documents upload"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Help Documents</a>
        </li>
        <li>
            <a href="govtenders" data-toggle="tooltip" data-placement="top" title="Admin Tenders Documents Upload"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Goverment Tenders</a>
        </li>
        <li>
            <a href="contact" data-toggle="tooltip" data-placement="top" title="All the Contacted People"><i class="fa fa-address-book" aria-hidden="true"></i>&nbsp;Request from Public</a>
        </li>
    </ul>
</div>
