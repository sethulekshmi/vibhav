<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="form-inline">
            <div class="form-group">
              <input ref="name" id="name" type="text" class="form-control" aria-label="search by name" required >
            </div> 
            <div class="form-group">
              <div class="dropdown" >
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Asset Type <i class="fa fa-chevron-down"></i></button>
                <ul class="dropdown-menu" id="asset-type">
                  <?php foreach($asset_type as $data){ ?>
                  <li  id="dropdownli" ><a href="#" id="<?php echo $data['office_type'];?>" onclick="assetsearch(this);return false">
                    <?php echo $data['office_type'];?> </a>
                  </li>
                  <?php } ?> 
                </ul>
              </div>
            </div>
          </div>
          <!-- admin main content starts-->
         <div id="map" style="height: 500px; border:5px solid rgb(255, 255, 255);" >     
           <!-- google map content starts-->               
            <script type="text/javascript">
              function initMap() {
                var mapData=JSON.parse('<?php echo json_encode($map_data); ?>');
                if(mapData.length>0){
                  var uluru = {lat: parseFloat(mapData[0].latitude), lng: parseFloat(mapData[0].longitude)};
                  var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 7,
                    center: uluru
                    });
                for ( i=0;i< mapData.length;i++) {
                  var markerLatLong = {lat: parseFloat(mapData[i].latitude), lng: parseFloat(mapData[i].longitude)};
                  console.log(mapData[i].office_name);
                  var marker = new google.maps.Marker({
                  position: markerLatLong,
                  map: map
                }); 
                  var contentString = mapData[i].office_name
                  alertMarkerMessage(marker,contentString);
        }
                }
              }
           function alertMarkerMessage(marker,contentString){
              google.maps.event.addListener(marker, "click", function () {
              var infowindow = new google.maps.InfoWindow();
              infowindow.setContent(contentString);
              infowindow.open(map, this);
          });
           }   
              
            </script>
           <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA283iCG_L0z7_Q2YRrvkYkUXgSVT5EgsA&language=en&region=IN&callback=initMap">
                  </script>
         </div>     
           </div>
         <!-- google map content ends-->
        </div>
      </div>
    </div>
  </div>
</div>
   
    <script>
   function assetsearch(el){
      var idval=el.id;   
      console.log(idval);
      post("<?=base_url()?>index.php/logincontroller/searchdropdown",{id:idval});
}

   function searchdb(el){
      var idval = document.getElementById('name').value;
      console.log(idval);      
      post("<?=base_url()?>index.php/logincontroller/searchdb",{id:idval});
}

  function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}
    
$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    </script>
<?php $this->view('templates/footer'); ?>