<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content pwdreset">
          <div class="header-maker">
            <h2 class="content-title">User Management Module- Super Admin</h2>
          </div>
          <div class="changepwd">
            <form id="form1">
              <div class="form-group">
              <label for="email">Email:&nbsp;</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
              </div>
              <div class="form-group">
              <label for="password">Old Password:&nbsp;</label>
                <input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Old Password" required>
                <span ></span>
              </div>
              <div class="form-group">
              <label for="password">Password:&nbsp;</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                <span id='remainingD'></span>
              </div>
              <button type="button" id="button_submit" class="btn btn-primary">Submit</button>     
        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$("#button_submit").click(function(){
  debugger;
  var email=$("#email").val();
  var newpwd=$("#password").val();
  var oldpwd=$("#oldpassword").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('index.php/logincontroller/change'); ?>",
    data: { 'email':email,'newpwd':newpwd,'oldpwd':oldpwd},
    success: function(response){
      console.log("success");
      alert("Passchanged successfully");
      location.reload();
    },
    error: function(err){                      
      console.log('error');
      console.log(err);
      alert("Error in changing the password");
    },
  });
})
</script>
<?php $this->view('templates/footer'); ?>