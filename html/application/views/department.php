<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin Departments Data</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="departmentTable">
              <tr>
                <td><strong>Departments</strong></td> 
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#adddepartment"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
              </tr> 
              <tbody>
                <?php foreach($department_data as $data){?>
                <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['department_name'];?></td>
                  <td><button type="button"  id="updatedepartment" class="btn btn-info" data-toggle="modal"  data-target="#departmentupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
                </tr>
                <?php }?> 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <!-- Modal -->
  <div class="modal fade" id="adddepartment" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add a department</h4>
        </div>
        <div class="modal-body">
         <form class="form-horizontal">
            <div class="form-group">
               <label class="col-sm-4 control-label">Department</label>
                   <div class="col-sm-8"> 
                      <input type="text"  id="add_department"></br>
                  </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <a  data-dismiss="modal" class="btn btn-success" id="adddepartmentbutton"><i class="fa fa-plus"></i>&nbsp;Add</a>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
var x = document.getElementById("departmentTable").rows.length;
$('table tbody tr  td').on('click',function(){
  $("departmentupdateModal").modal("show");
  $("#id").val($(this).closest('tr').children()[0].textContent);
  $("#department_name").val($(this).closest('tr').children()[1].textContent);       
});
function timmedinput(x){
  return x.replace(/(<([^>]+)>)/ig,"");
}
$('#adddepartmentbutton').click(function(){
  var department_name = $("#add_department").val();
  var sanitizedInput=timmedinput(department_name);
  console.log(department_name);
  if(department_name==null || department_name===""){
    alert("Enter a Department name");
  }else{  
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/add_department'); ?>",
      data: { 'department_name':sanitizedInput },      
      success: function(response)
      {
        console.log("success");
        location.reload();
      },
      error: function(err)
      {
        console.log('error');
        console.log(err);
      },
    });
  }
});
</script>
<!-- Modal -->
<div class="modal fade" id="departmentupdateModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">update department</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group" style="display:none">
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
               <input type="text" disabled="true" id="id"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Department</label>
            <div class="col-sm-8"> 
               <input type="text"  id="department_name"></br>
            </div>
          </div>
          </form>
        </div>  
        <div class="modal-footer">
          <a  data-dismiss="modal" class="btn btn-success" id="updatedepartmentbutton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
          <button  data-dismiss="modal" type="button" id="deletedepartmentbutton" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('#deletedepartmentbutton').click(function(){
  var id = $("#id").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('index.php/logincontroller/delete_department'); ?>",
    data: { 'id':id}, 
    success: function(response)
    {
      console.log("success");
      location.reload();
    },
    error: function(err)
    {
     console.log('error');
     console.log(err);
    },
  });
});
function timmedinput(x){
  return x.replace(/(<([^>]+)>)/ig,"");
}
$('#updatedepartmentbutton').click(function(){    
  var id = $("#id").val();
  var department_name = $("#department_name").val();
  var sanitizedInput=timmedinput(department_name);
  console.log(id +""+department_name);
  if(sanitizedInput.length===0){
    alert("Enter a valid Department Name");
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/update_department'); ?>",
      data: { 
        'id':id,
        'department_name':sanitizedInput, 
      },
      success: function(response)
      {
        console.log("success");
        location.reload()
      },
      error: function(err){
        console.log('error');
      },
    });
  }
});
$("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});
</script>
<?php $this->view('templates/footer'); ?>