<!-- function for generating pdf
 -->
<?php

tcpdf();


$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);

$obj_pdf->SetTitle('Report');
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 8);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();
$tbl = '<table border="1" cellspacing="1" cellpadding="1">
      <tr> 
      <td><strong> Employee Id</strong></td>
      <td><strong>Office Name</strong></td>
      <td><strong>Administrative Department</strong></td>
      <td><strong>Office Type</strong></td>
      <td><strong>Address</strong></td>
      <td><strong>Pincode</strong></td>
      <td><strong>Building Type</strong></td> 
      <td><strong>Landline</strong></td>
      <td><strong>District</strong></td>
      <td><strong>Taluk</strong></td>
      <td><strong>Village</strong></td>
      <td><strong>Block</strong></td>
      <td><strong>Local Body Type</strong></td>
      <td><strong>Local Body Name</strong></td>
      <td><strong>Email</strong></td>
      <td><strong>Landmark</strong></td>
      <td><strong>Contact person</strong></td>
      <td><strong>Contact</strong></td>
      <td><strong>Working Start</strong></td>
      <td><strong>Working End</strong></td>
      <td><strong>Status</strong></td>
    

      </tr> ';

foreach($main_data as $data){



$empid= $data['employee_id'];
$office= $data['office_name'];
$administrative_department= $data['administrative_department'];
$officetype=$data['office_type'];
$officeaddress= $data['address'];
$pin=$data['pincode'];
$buildingtype=$data['building_type'];
$land_line=$data['landline'];
$districtname=$data['district'];
$talukname=$data['taluk'];
$villagename=$data['village'];
$blockname=$data['block'];
$localbodytype=$data['localbody_type'];
$localbodyname=$data['localbody_name'];
$Email= $data['email'];
$contactdetail= $data['contact'];
$Email= $data['email'];
$Landmark=$data['landmark'];
$contactperson=$data['contact_person'];
$Contact=$data['contact'];
$start= $data['working_start'];
$end= $data['working_end'];
$Status= $data['status'];


$tbl=$tbl.'<tr><td>'.$empid.'</td><td>'.$office.'</td><td>'.$administrative_department.'</td><td>'.$officetype.'</td><td>'.$officeaddress.'</td><td>'.$pin.'</td><td>'.$buildingtype.'</td><td>'.$land_line.'</td><td>'.$districtname.'</td><td>'.$talukname.'</td><td>'.$villagename.'</td><td>'.$blockname.'</td><td>'.$localbodytype.'</td><td>'.$localbodyname.'</td><td>'.$Email.'</td><td>'.$Landmark.'</td><td>'.$contactperson.'</td><td>'.$Contact.'</td><td>'.$start.'</td><td>'.$end.'</td><td>'.$Status.'</td></tr>';
}
$tbl=$tbl.'</table>';
$obj_pdf->writeHTML($tbl, true, false, false, false, '');
$obj_pdf->Output('report.pdf', 'FD');


//ob_end_clean();
?>

