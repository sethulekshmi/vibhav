<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Admin Govt Tenders Upload</h2>
          </div>
          <div class="upload-form">
            <?php echo form_open_multipart('logincontroller/govtenders_upload');?>
            <input type="file" name="userfile" />
            <br />
            <input type="submit" class="btn btn-info" value="upload"  />
            </form>
          </div>
          <div class="table_content table-responsive">
           <table class="table table-hover table-bordered" id="contactTable">
               <tr><td><strong>File Name</strong>
               <td><strong>File Path</strong></td>
               <td><strong>Actions</strong></td>
             <tbody>
                     <?php foreach($tenders as $data){?>
                     <tr>
                      <td class="filename"><?php echo $data['filename'];?></td>
                      <td class="filepath"><?php echo $data['filepath'];?></td>
                      <td><button class="btn btn-danger" id="deletebutton"><i class="fa fa-trash-o"></i>&nbsp;Delete</button></td>
                     </tr>
                     <?php }?>      
              </tbody>
                     </table>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Listing all the files-->

<script>

$('#deletebutton').click(function(){
  debugger;
  var makesure=confirm("Are you sure you want to delete the file");
  if(makesure==true){
  var item=$(this).closest('tr')
                  .find('.filename')
                  .text();
  var fullpath=$(this).closest('tr')
                      .find('.filepath')
                      .text();
    console.log(item);
  $.ajax({
    type:"POST",
    url:"<?php echo base_url('index.php/logincontroller/govtenders_pdfiles');?>",
    data:{
      "fileName":item,
      "filePath":fullpath
    },
    success:function(response){
      alert("The File has been successfully deleted");
      
    },
    error:function(err){
      alert("Error in deleting the file,Try again later");
    }
  });
}else{
  console.log("Deleting users blocked by user");
}
});
</script>
<?php $this->view('templates/footer'); ?>