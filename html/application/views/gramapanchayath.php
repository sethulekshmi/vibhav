<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | GramaPanchayath</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="localbody_table" >
              <tr>
                <td><strong>Local body</strong></td>
                <td><strong>Block</strong></td>
                <td><strong>District</strong></td>
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#addlocalbodymodal"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
              </tr> 
              <tbody>
              <?php foreach($gramapanchayath_data as $data){?>
                <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['localbodyname'];?></td>
                  <td><?php echo $data['block'];?></td>
                  <td><?php echo $data['district'];?></td>
                  <td><button type="button"  id="uplocalbody" class="btn btn-info" data-toggle="modal"  data-target="#localbodyupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>  
                </tr>
                <?php }?>
              </tbody>
            </table> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addlocalbodymodal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add a  local body type</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">local body </label>
            <div class="col-sm-8"> 
              <input type="text"  id="add_localbody"></br>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" class="btn btn-success" id="addlocalbodybutton"><i class="fa fa-plus"></i>&nbsp;Add</a>
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>  
  </div>
</div>
<script type="text/javascript">
var x = document.getElementById("localbody_table").rows.length;
$('table tbody tr  td').on('click',function(){
  $("localbodyupdateModal").modal("show");
  $("#id").val($(this).closest('tr').children()[0].textContent);
  $("#local_body_type").val($(this).closest('tr').children()[1].textContent);
});
$('#addlocalbodybutton').click(function(){
  var local_body_type = $("#add_localbody").val();
  console.log(local_body_type);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('index.php/logincontroller/add_localbody'); ?>",
    data: { 'local_body_type':local_body_type},
    success: function(response)
    {
      console.log("success");
      location.reload();
    },
    error: function(err)
    {
      console.log('error');
      console.log(err);
    },
  });
});
</script>
<!-- Modal -->
<div class="modal fade" id="localbodyupdateModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">update localbody</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group"style="display:none">
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
               <input type="text" disabled="true" id="id"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">localbody</label>
            <div class="col-sm-8"> 
               <input type="text"  id="local_body_type"></br>
            </div>
          </div>
        </form>
      </div>     
      <div class="modal-footer">
        <a href="dataupdate" data-dismiss="modal" class="btn btn-success" id="updatelocalbody"><i class="fa fa-refresh"></i>&nbsp;Update</a>
        <button  data-dismiss="modal" type="button" id="deletelocalbody" class="btn btn-danger" ><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>

      </div>
    </div> 
  </div>
</div>
<script type="text/javascript">
$('#deletelocalbody').click(function(){
  var id = $("#id").val();
  console.log(id);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('index.php/logincontroller/delete_local'); ?>",
    data: { 'id':id}, 
    success: function(response)
    {
      console.log("success");
      location.reload();
    },
    error: function(err)
    {
     console.log('error');
     console.log(err);
     },
  });
});
$('#updatelocalbody').click(function(){   
  var id = $("#id").val();
  var local_body_type = $("#local_body_type").val();
  console.log(id + ""+ local_body_type);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url('index.php/logincontroller/update_localbody'); ?>",
    data: { 
      'id':id,
      'local_body_type':local_body_type,
      
    },
    success: function(response)
    {
      console.log("success");
      location.reload()
    },
    error: function(err){
      console.log('error');
    },
  });
});
$("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});
</script>
<?php $this->view('templates/footer'); ?>