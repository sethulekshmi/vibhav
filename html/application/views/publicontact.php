<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Admin | Public Contact Details</h2>
          </div>
           <div class="table_content table-responsive">
             <table class="table table-hover table-bordered">
               <tr><td><strong>Name</strong></td>
               <td><strong>Email</strong></td>
               <td><strong>Mobile Number</strong></td>
               <td class="actions-col"><strong>Actions</strong></td>
               <tbody>
                 <?php foreach($public_relations as $data){?>
                 <tr>
                    <td class="name"><?php echo $data['name'];?></td>
                    <td class="email"><?php echo $data['email'];?></td>
                    <td class="mobile_number"><?php echo $data['mobile_number'];?></td>
                    <td class="delete_button"><button class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;Delete</button></td>
                  </tr>
                 <?php }?>      
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->view('templates/footer'); ?>