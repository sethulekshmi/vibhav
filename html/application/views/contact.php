<?php $this->view('templates/header','Contact'); ?>
<div id="wrapper">
  <div id="page-content-wrapper1">
    <div class="container-fluid">    
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('sidebar'); ?>
        </div>
        <div class="col-sm-10 login-content">
          <div class="header-maker">
            <h2 class="content-title">Contact</h2>
          </div>
          <div id="contactform">
            <div class="form-area">
              <form id="form1">
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" required>
                  <span id='remainingD'></span>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
                </div>
                <div class="form-group">
                  <textarea class="form-control" type="textarea" id="message" maxlength="200" placeholder="Message (limited to 200 characters)"  rows="7"></textarea>
                  <span id='remainingC'></span>                       
                </div>
                <button type="button" id="button_submit" class="btn btn-primary pull-right">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
	<script type="text/javascript">

  function validateEmail(email){
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(form1.email.value)){
        return true;
      }else{
        alert("Please enter a valid email");
        return false;
      }
    } 
 function timmedinput(x){
    return x.replace(/(<([^>]+)>)/ig,"");
  }
    function validatedata(name,mobile_number,subject,message){
      if(!name.length){
        alert("Please provide a name");
      }else if(!mobile_number.length){
        alert("Please provide a mobile number");
      }else if(mobile_number.length>10 && mobile_number.length<10){
        alert("Please enter a 10 digit mobile number");
      }else if(typeof(mobile_number) == "number"){
        alert("Please enter a valid mobile number");
      }else if(!subject.length){
        alert("Enter a subject");
      }else if(!message.length){
        alert("Enter a message for admin");
      }else{
        return true;
      }
    }

    //function for validation and adding to db
    $('#button_submit').click(function(){
      debugger;
      var name = $("#name").val();
      var email = $("#email").val();
      var mobile_number = $("#mobile_number").val();
      var trimmedname=timmedinput(name);
      var trimmedemail=timmedinput(email);
      var trimmedmobile_number=timmedinput(mobile_number);
      if(mobile_number.length!=10){
        alert("Enter a valid mobile number");
      }else{
        var subject = $("#subject").val();
      var message = $("#message").val();
      if(validateEmail(email) && validatedata(name,mobile_number,subject,message)){
        $.ajax({
                type: "POST",
                url: "<?php echo base_url('index.php/vibhav/contactadmin'); ?>",
                data: { 'name':name,'email':email,'mobile_number':mobile_number,'subject':subject,'message':message},
                success: function(response){
                console.log("success");
                alert("Your Message has been sent");
                location.reload();
                },
                error: function(err){
                console.log('error');
                console.log(err);
                },
              });
      }
    }
      
    });
	</script>
<?php $this->view('templates/footer'); ?>