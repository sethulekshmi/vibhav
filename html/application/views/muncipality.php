<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | Municipality</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="districtTable">
              <tr>
                <td><strong>District Panchayath</strong></td>
                <td><strong>District</strong></td>
                <td class="actions-col"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#adddistrict"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
              </tr> 
              <tbody>
              <?php foreach($muncipality_data as $data){?>
                <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['localbodyname'];?></td>
                  <td><?php echo $data['district'];?></td>
                  <td><button type="button"  id="updatedistrict" class="btn btn-info" data-toggle="modal"  data-target="#districtupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
                </tr>
              <?php }?>  
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script type="text/javascript">
debugger;
var x = document.getElementById("districtTable").rows.length;
$('table tbody tr  td').on('click',function(){
  $("#id").val($(this).closest('tr').children()[0].textContent);
  $("#local_body_type").val($(this).closest('tr').children()[1].textContent);
});         
</script>
<?php $this->view('templates/footer'); ?>