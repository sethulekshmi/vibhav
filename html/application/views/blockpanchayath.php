<?php $this->view('templates/header',$pageTitle); ?>
<?php $this->view('templates/navbar'); ?>
<div id="wrapper">
  <!-- Page Content -->
  <div id="page-content-wrapper1">
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-2 sidenav">
          <?php $this->view('templates/adminsidebar'); ?>
        </div>
        <div class="col-sm-10 main-content">
          <div class="header-maker">
            <h2 class="content-title">Super Admin | Block Panchayath</h2>
          </div>
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="blockpanchayathTable" >
              <tr>
                <td><strong>District Panchayath</strong></td>
                <td><strong>District</strong></td>
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#adddistrict"><i class="fa fa-plus"></i>&nbsp;Add</button></td>
              </tr> 
              <tbody>
                <?php foreach($blockpanchayath_data as $data){?>
                <tr>
                  <td style="display:none"><?php echo $data['id'];?></td>
                  <td><?php echo $data['localbodyname'];?></td>
                  <td><?php echo $data['district'];?></td>
                  <td><button type="button"  id="updatedistrict" class="btn btn-info" data-toggle="modal"  data-target="#districtupdateModal"><i class="fa fa-refresh"></i>&nbsp;Update</button></td>
               </tr>
               <?php }?>
              </tbody>   
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>
<div class="modal fade" id="adddistrict" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add a District Panchayath</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-4 control-label">Corporation Name:</label>
            <div class="col-sm-8"> 
              <input type="text"  id="addcorporation_name"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">District Name:</label>
            <div class="col-sm-8"> 
              <input type="text"  id="adddistrictname"></br>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a href="messages" data-dismiss="modal" class="btn btn-success" id="adddistrictbutton"><i class="fa fa-plus"></i>&nbsp;Add</a>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var x = document.getElementById("blockpanchayathTable").rows.length;
$('table tbody tr  td').on('click',function(){
  $("districtupdateModal").modal("show");
  $("#id").val($(this).closest('tr').children()[0].textContent);
  $("#corporation_name").val($(this).closest('tr').children()[1].textContent);
  $("#district_named").val($(this).closest('tr').children()[2].textContent);
 });
function timmedinput(x){
  return x.replace(/(<([^>]+)>)()/ig,"");
}
$('#adddistrictbutton').click(function() {
  debugger;
  var corporation_name = $("#addcorporation_name").val();
  var district_name=$('#adddistrictname').val();
  var sanitized=timmedinput(corporation_name);
  var sanitizedin=timmedinput(district_name);
  if(sanitized==null || sanitized===""){
    alert("Enter a District");
  }else{
    console.log(sanitized);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/add_corporation'); ?>",
      data: {
        'corporation_name':sanitized,
        'district_name':sanitizedin
      },
      success: function(response){
        console.log("success");
        location.reload();
      },
      error: function(err){
        console.log('error');
        console.log(err);
      },
    });
  }
});
</script>
 <!-- Modal -->
<div class="modal fade" id="districtupdateModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Block Panchayath</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group"  style="display:none" >
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
               <input type="text" disabled="true" id="id"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Corporation Name:</label>
            <div class="col-sm-8"> 
               <input type="text"  id="corporation_name"></br>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">District Name:</label>
            <div class="col-sm-8"> 
               <input type="text"  id="district_named"></br>
            </div>
         </div>
        </form>
      </div>   
      <div class="modal-footer">
        <a  data-dismiss="modal" class="btn btn-success" id="updateButton"><i class="fa fa-refresh"></i>&nbsp;Update</a>
        <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;Delete</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>
      </div>
    </div>  
  </div>
</div>
<script type="text/javascript">
function timmedinput(x){
  return x.replace(/(<([^>]+)>)/ig,"");
}
$('#deleteButton').click(function() {
  var confi=confirm('Are you sure you want to Delete?');
  if(confi==true){
    debugger;
    var id = $("#id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/delete_corporation'); ?>",
      data: { 'id':id},
      success: function(response)
      {
        console.log("success");
        location.reload();
      },
      error: function(err)
      {
        console.log('error');
        console.log(err);
      },
    });
  }else{
    console.log("Designation Deletion blocked by user");
  }
});
$('#updateButton').click(function() {
  debugger; 
  var id = $("#id").val();
  var corporation_name = $("#corporation_name").val();
  var district=$("#district_named").val();
  var sanitizedInput=timmedinput(corporation_name);
  var sanitizedin=timmedinput(district);
  if(sanitizedInput.length===0){
    alert("Enter a valid District Name");
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('index.php/logincontroller/update_corporation'); ?>",
      data: { 
        'id':id,
        'corporation_name':sanitizedInput,
        'district_name':sanitizedin
      },
      success: function(response)
      {
        console.log("success");
        location.reload();
      },
      error: function(err){
        console.log('error');
      },
    });
  }
});
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>
<?php $this->view('templates/footer'); ?>