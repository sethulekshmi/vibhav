<?php

/*
* @fileOverview:Model for viewing, updating and deleting files uploaded into server
* @date:22/02/2018
* @author:Jithin Zacharia
*/

class Fileupload_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();
    }

    public function viewallfiles(){
    	$this->db->select('*');
    	$this->db->from('helpdocuments');
    	$sql=$this->db->get();
    	$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'filename'=>$row->filename,
				'filepath'=>$row->filepath
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
    }

    //function for viewing all gov tenders pdf files
    public function govtendersupload(){
    	$this->db->select('*');
    	$this->db->from('tenderdocuments');
    	$sql=$this->db->get();
    	$results=array();
    	foreach ($sql->result() as $row) {
    		$results[]=array(
    			'id'=>$row->id,
    			'filename'=>$row->filename,
    			'filepath'=>$row->filepath
    			);
    	}
    	if(!empty($results)){
    		return $results;
    	}else{
    		return false;
    	}
    }

    //function for deleting the gov tenders pdf files
    public function deletegovtenders($filename,$filepath){
    	$this->db->where('filename',$filename);
		$this->db->delete('tenderdocuments');
		delete_files($pdfullpath); 
		return true;
    }
}