<?php
class Api_search_results extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}
public function search($office_name){
		$this->db->like('office_name',$office_name);
		$this->db->or_like('office_type',$office_name);
		$this->db->or_like('district',$office_name);
		$this->db->or_like('contact_person',$office_name);
		$this->db->or_like('landmark',$office_name);
		$sql=$this->db->get('asset_profile');
		return $sql->result();
	}

public function searchall(){
		$this->db->select('*');
		$sql=$this->db->get('asset_profile');
		return $sql->result();
	}




}
?>
