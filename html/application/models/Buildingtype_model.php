<?php 
//FileName:BuildingType_model: Model for giving data back to buildingtype view 
//Date:02/04/2018
//Author:Ramya
class Buildingtype_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }
    ///Function for getting buildingtype name to view
    public function get_buildingtype(){
        $buildingtype_data=array();
        $this->db->select('id,buildingtype_name');    
        $this->db->from('tbl_buildingtype');
        $query = $this->db->get();
        return $query->result_array();
     }

      ///Function for adding new buildingtypes to model
      public function add_newbuildingtype($data){
         $sql=$this->db->insert('tbl_buildingtype',$data);
     }

    ///Function for deleating buildingtype names from DB
    public function deletion_buildingtype($id){
          $this->db->where('id',$id);
          $query= $this->db->delete('tbl_buildingtype');
          return;
     }

     public function update_buildingtype($id,$data){
      print "update query";
        $this->db->where('id',$id);
        $sql=$this->db->update('tbl_buildingtype',$data);
        return ;
    }


}
?>