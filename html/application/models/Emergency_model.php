<?php
/*
*@fileOveview:Emergency_Model.php getting details for emergency numbers
*@Date:20/02/2018
*/

class  Emergency_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->database();
	}

	public function getDetails(){
		$this->db->select('*');
		$this->db->from('emergencycontacts');
		$sql=$this->db->get();
		$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'contactname'=>$row->contactname,
				'contactnumber'=>$row->contactnumber
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}

	}



}	
