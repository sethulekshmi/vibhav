<?php
/*
*@fileOverview: Api_getter_model, gives info about different tables
*@author:Jithin Zacharia
*@license:None
*/
class Api_getter_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}

	//function for reterving all department data
	public function department(){
		$this->db->select('department_name');
		$this->db->from('tbl_department');
		$data=$this->db->get();
		if($data->num_rows()>0){
			return $data->result();
		}else{
			return false;
		}
	}

	//function for retreving all districts
	public function districts(){
		$this->db->select('district_name');
		$this->db->from('tbl_district');
		$data=$this->db->get();
		if($data->num_rows()>0){
			$row=$data->row_array();
			return $row;
		}else{
			return false;
		}
	}

	//function for retreving all the localbody
	public function localbody(){
		$this->db->select('*');
		$this->db->from('tbl_local_body_type');
		$data=$this->db->get();
		if($data->num_rows()>0){
			$row=$data->row_array();
			return $row;
		}else{
			return false;
		}
	}

	//function for getting the categories
	public function categories(){
		$this->db->select('institution_category');
		$this->db->from('tbl_institution_category');
		$data=$this->db->get();
		if($data->num_rows()>0){
			$row=$data->row_array();
			return $row;
		}else{
			return false;
		}
	}
}