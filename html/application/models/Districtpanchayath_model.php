<?php
/*
* @fileOverview: File for adding viewing and updating district panchyath
* @date:23/02/2018
* @atuhor:Jithin Zacharia
*/

class Districtpanchayath_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    //function for viewing all the district panchayath
    public function getall(){
    	$district_data=array();
        $this->db->select('id,localbodyname,district');    
        $this->db->from('districtpancayath');
        $query = $this->db->get();
        return $query->result_array();
    }

    //function for adding a new district panchayath
    public function add_newdistrictpanchayath($data){
    $sql=$this->db->insert('districtpancayath',$data); 
    return;   
    }

    //function for updating update_districtpanchayath 
    public function update_districtpanchayath($id,$data){
        $this->db->where('id',$id);
        $this->db->update('districtpancayath',$data);
        $this->db->select('*');
        $this->db->from('districtpancayath');
        $sql=$this->db->get();
        $results=array();
        foreach ($sql->result() as $row) {
            $results[]=array(
                'id'=>$row->id,
                'localbodyname'=>$row->localbodyname,
                'district'=>$row->district
            );
        }
        if(!empty($results)){
            return $results;
        }else{
            return false;
        }
    }

    //function for deleting districtpanchayath
    public function delete_districtpanchayath($id){
        $this->db->where('id',$id);
        $this->db->delete('districtpancayath');
        $this->db->select('*');
        $this->db->from('districtpancayath');
        $sql=$this->db->get();
        $results=array();
        foreach ($sql->result() as $row) {
            $results[]=array(
                'id'=>$row->id,
                'localbodyname'=>$row->localbodyname,
                'district'=>$row->district
            );
        }
        if(!empty($results)){
            return $results;
        }else{
            return false;
        }
    }
}
