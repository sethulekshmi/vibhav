<?php 
//FileName:User_model: Model for giving data back to corporation view 
//Date:23/09/2017
//Author:Jithin Zacharia

class Corporation_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    //viewing all corporation 
    public function getall(){
    	$district_data=array();
        $this->db->select('id,localbodyname,district');    
        $this->db->from('corporation');
        $query = $this->db->get();
        return $query->result_array();
    }

    //adding new corporation
    public function addcorporation($data){
        $this->db->insert('corporation',$data);
        $this->db->select('*');
        $this->db->from('corporation');
        $query=$this->db->get();
        return $query->result_array();
    }

    //updating coporation
    public function updatecorporation($id,$data){
        $this->db->where('id',$id);
        $this->db->update('corporation',$data);
        $this->db->select('*');
        $this->db->from('corporation');
        $sql=$this->db->get();
        $results=array();
        foreach ($sql->result() as $row) {
            $results[]=array(
                'id'=>$row->id,
                'localbodyname'=>$row->localbodyname,
                'district'=>$row->district
            );
        }
        if(!empty($results)){
            return $results;
        }else{
            return false;
        }
    }

    //deleting corporation
    public function deletecorporation($id){
        $this->db->where('id',$id);
        $this->db->delete('corporation');
        $this->db->select('*');
        $this->db->from('corporation');
        $sql=$this->db->get();
        $results=array();
        foreach ($sql->result() as $row){
            $results[]=array(
                'id'=>$row->id,
                'localbodyname'=>$row->localbodyname,
                'district'=>$row->district
            );
        }
        if(!empty($results)){
            return $results;
        }else{
            return false;
        }
    }
}
