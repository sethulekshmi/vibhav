<?php 
//FileName:User_model: Model for giving data to user module
//Date:23/09/2017
//Author:Jithin Zacharia

class Gramapanchayath_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    public function get_gramapanchayath(){
    	$gramapanchayath_data=array();
    	$this->db->select('id,localbodyname,block,district');
    	$this->db->from('gramapanchayath');
    	$query=$this->db->get();
    	return $query->result_array();
    }
}
?>