<?php 
//FileName:User_model: Model for giving data to user module
//Date:23/09/2017
//Author:Jithin Zacharia

class Model_users extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    public function email_exists(){
    $email = $this->input->post('email');
    $query = $this->db->query("SELECT email, password FROM user_name WHERE email='$email'");    
    	if($row = $query->row()){
        	return TRUE;
    	}else{
        	return FALSE;
    	}
	}
    public function resetpwd($email,$password){
        $this->db->where('email',$email);
        $sql=$this->db->query("UPDATE user_name SET password = '$password' WHERE email='$email'");
        return ;
      }

    public function oldpasscheck($old){
        $this->db->select('*');
        $this->db->from('user_name');
        $this->db->where('password',$old);
        $this->db->limit(1);
        $sql=$this->db->get();
        if($sql->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }  

    public function changepwd($email,$newpwd,$old){
        $result=$this->oldpasscheck($old);
        if($result==false){
            return false;
        }else{
        $this->db->where('email',$email);
        $sql=$this->db->query("UPDATE user_name SET password = '$newpwd' WHERE email='$email'");
        return ;
    }
    }

    //REST API model for checking email exist in db
    public function emailcheck($email){
        $query=$this->db->query("SELECT email, password from surveydata WHERE email='$email'");
        if($result=$query->row()){
            return true;
        }else{
            return false;
        }
    }

    //REST API model for resetting password for API call
    public function pwdchange($email,$new){
        $this->db->where('email',$email);
        $sql=$this->db->query("UPDATE surveydata SET password= '$new' WHERE email='$email'");
        return ;
    }

    //REST API method to see if the supplied password is oldpassword
    public function oldpwdcheck($oldpassword){
        $query=$this->db->query("SELECT password FROM surveydata where password= '$oldpassword'");
        $this->db->limit(1);
        $sql=$this->db->get();
        if($sql->num_rows()==1){
            return true;
        }else{
            return false;
        }
    }

    //REST API method to reset password
    public function resetpwdmobile($password,$oldpassword){
        $query=$this->db->query("UPDATE surveydata SET password= '$password' WHERE password= '$oldpwdcheck' ");
        return ;
    }
}
?>