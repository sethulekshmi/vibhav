<?php 
//FileName:User_model: Model for giving data to user module
//Date:23/09/2017
//Author:Jithin Zacharia

class User_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }
    public function get_userdata(){
        $main_data=array();
        $query=$this->db->query('SELECT employee_id,fullname,employee_department,status FROM surveydata');
        return $query->result_array();
    }
     public function approve_userdata($id){
        $approve_data=array();
        $active='active';
      
        $this->db->where('employee_id',$id); 
        $this->db->set('status',$active);
            $this->db->update('surveydata'); 
          
           return true;
    }
    public function block_user($id){
        $approve_data=array();
        $pending='pending';
        $this->db->where('employee_id',$id); 
        $this->db->set('status',$pending);
        $this->db->update('surveydata'); 
          
           return true;
    }
    public function delete_user($data){
        $main_data=array();
        $this->db->where('employee_id',$data);
        $this->db->delete('surveydata');
        return true;

    }  
 public function select_allasset($id){
        // $asset_data=array();
        $this->db->where('employee_id',$id);
        $this->db->select('*');
        $this->db->from('asset_profile');
        $query=$this->db->get();
        return $query->result();
    } 
public function get_profile($id){
        // $profile_data=array();
        $this->db->where('employee_id',$id); 
        $this->db->select('*'); 
        $this->db->from('surveydata'); 
        $query = $this->db->get();
        return $query->row_array(); 
   }
    


}
?>