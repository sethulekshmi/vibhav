<?php 
//FileName:User_model: Model for giving data back to districts view 
//Date:23/09/2017
//Author:Jithin Zacharia

class District_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    ///Function for getting district name to view
    public function get_district(){
        $district_data=array();
        $this->db->select('id,district_name');    
        $this->db->from('tbl_district');
        $query = $this->db->get();
        return $query->result_array();
     }

      ///Function for adding new districts to model
      public function add_newdistrict($data){
         $sql=$this->db->insert('tbl_district',$data);
     }

    ///Function for deleating districts names from DB
    public function deletion_district($id){
          $this->db->where('id',$id);
          $query= $this->db->delete('tbl_district');
          return;
     }

     ///function for 
     public function update_district($id,$data){
        $this->db->where('id',$id);
        $sql=$this->db->update('tbl_district',$data);
        return ;
    }


}
?>

