<?php 
//FileName:User_model: Model for giving data back to corporation view 
//Date:23/09/2017
//Author:Jithin Zacharia

class Blockpanchayath_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }

    //viewing all blockpanchayath 
    public function getall(){
    	$district_data=array();
        $this->db->select('id,localbodyname,district');    
        $this->db->from('blockpancayath');
        $query = $this->db->get();
        return $query->result_array();
    }
}