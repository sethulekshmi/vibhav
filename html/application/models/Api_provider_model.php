<?php
/*
*@fileOverview:Api_provider_model.php, provides the data for different providers
*@author:Jithin Zacharia
*license:None
*/
class Api_provider_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}

	//function for retreiving all the panchayath
	public function panchayath($district){
		$this->db->select('localbodyname');
		$this->db->distinct();
		$this->db->from('gramapanchayath');
		$this->db->where('district',$district);
		$sql=$this->db->get();
		if($sql->num_rows()>1){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for checking username is aleady taken
	public function emailfactory($username){
		$this->db->select('*');
		$this->db->from('surveydata');
		$this->db->where('username',$username);
		$sql=$this->db->get();
		if($sql->num_rows()>=1){
			return true;
		}else{
			return false;
		}
	}

	//function for giving out the userinfo
	public function userinfo($username){
		$this->db->select('fullname');
		$this->db->select('employee_designation');
		$this->db->select('employee_department');
		$this->db->from('surveydata');
		$this->db->where('username',$username);
		$sql=$this->db->get();
		if($sql->num_rows()==1){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for giving emergency contacts
	public function emergency(){
		$this->db->select('contactname');
		$this->db->select('contactnumber');
		$this->db->from('emergencycontacts');
		$sql=$this->db->get();
		if($sql->num_rows()>1){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for checking EmployeeID is already taken
	public function employeeidfactory($employee_id){
		$this->db->select('*');
		$this->db->from('surveydata');
		$this->db->where('employee_id',$employee_id);
		$sql=$this->db->get();
		if($sql->num_rows()>=1){
			return true;
		}else{
			return false;
		}
	}

	//Function for checking if supplied password is the old password
	public function oldpwdFactory($oldpwd){
		$this->db->select('*');
		$this->db->from('surveydata');
		$this->db->where('password',$oldpwd);
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	//function for resting password
	public function resetpwdFactory($username,$newpwd){
		$this->db->from('surveydata');
		$sql=$this->db->query("UPDATE surveydata SET password= '$newpwd' WHERE username= '$username'");
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	//function for checking if mobile number exists 
	public function mobilenumberfactory($mobilenumber){
		$this->db->select('*');
		$this->db->from('surveydata');
		$this->db->where('mobile',$mobilenumber);
		$sql=$this->db->get();
		if($sql->num_rows()>=1){
			return true;
		}else{
			return false;
		}
	}

	//function for keeping help documents files
	public function filekeeper($fileData){
		$this->db->insert('helpdocuments',$fileData);
		if ($this->db->affected_rows() == 1){
			return true;
		}else {
			return false;
		}			
	}

	//function for keeping tender pdf documents files
	public function tenderkeeper($fileData){
		$this->db->insert('tenderdocuments',$fileData);
		if($this->db->affected_rows()==1){
			return true;
		}else{
			return false;
		}
	}

	//function for prividing the file names to model
	public function filegetter($searchText){
		$this->db->select('id');
		$this->db->select('filename');
		$this->db->like('filename',$searchText);
		$sql=$this->db->get('helpdocuments');
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//giving gov tender files to mobile
	public function tendergetter($searchText){
		$this->db->select('id');
		$this->db->select('filename');
		$this->db->like('filename',$searchText);
		$sql=$this->db->get('tenderdocuments');
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for API functions
	function apidesignation($department){
		$this->db->select('designation');
		$this->db->where('department',$department);
		$this->db->from('designation');
		$sql=$this->db->get();
		if($sql->num_rows()>1){
			return $sql->result();
		}else{
			return false;
		}
	}
	
	//function for retreving all the districts
	public function districts(){
		$this->db->select('district');
		$this->db->distinct();
		$this->db->from('districtpancayath');
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for returing all the districtpachayath
	public function districtpachayath(){
		$this->db->select('localbodyname');
		$this->db->distinct();
		$this->db->from('districtpancayath');
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for giving out corporation values
	public function corporation(){
		$this->db->select('localbodyname');
		$this->db->distinct();
		$this->db->from('corporation');
		$this->db->distinct();
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//functiong for giving out department names
	public function departmentgetter(){
		$this->db->select('department_name');
		$this->db->from('tbl_department');
		$this->db->distinct();
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	//function for giving out munipality names
	public function muncipality($district){
		$this->db->select('localbodyname');
		$this->db->distinct();
		$this->db->from('muncipality');
		$this->db->where('district',$district);
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}

	public function block($district){
		$this->db->select('block');
		$this->db->distinct();
		$this->db->from('gramapanchayath');
		$this->db->where('district',$district);
		$sql=$this->db->get();
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}
}