<?php
class Api_login_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}
public function login($username,$password){
		$this->db->select('*');
		$pending='active';
		$this->db->from('surveydata');
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$this->db->where('status',$pending);
		$this->db->limit(1);
		$sql=$this->db->get();
		if ($sql->num_rows() >0){
			return true;
		}else{
			return false;
		}
	}

	public function user($username){
		$this->db->select('employee_id');
		$this->db->from('surveydata');
		$this->db->where('username',$username);
		$this->db->limit(1);
		$sql=$this->db->get();
		if($sql->num_rows()==1){
			return $sql->result();
		}else{
			return false;
		}
	}
}
?>
