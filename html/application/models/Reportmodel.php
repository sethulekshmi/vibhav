<?php 
//FileName:User_model: Model for giving data to asset  for report
//Date:23/09/2017
//Author:Jithin Zacharia

class Reportmodel extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }
    public function get_data($dtp1,$dtp2){
        $main_data=array();
               $this->db->select('employee_id,office_name,administrative_department,
                office_type,address,pincode,building_type,
                landline,district,taluk,village,block,localbody_type,
                localbody_name,email,landmark,contact_person,contact,working_start,working_end,status');
               $this->db->from('asset_profile');
               
               $this->db->where('timestamp >=',$dtp1);
              $this->db->where('timestamp <=',$dtp2);
		

               $query = $this->db->get();
               return $query->result_array();
              


    }
}
?>
<!---->
