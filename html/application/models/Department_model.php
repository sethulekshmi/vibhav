<?php 
//FileName:User_model: Model for giving data to asset  for report
//Date:23/09/2017
//Author:Jithin Zacharia

class Department_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }


    public function get_department()
     {
        $main_data=array();
               $this->db->select('id,department_name');
            
               $this->db->from('tbl_department');
               $query = $this->db->get();
               return $query->result_array();

     }


      public function add_newdepartment($data)

     {
         $sql=$this->db->insert('tbl_department',$data);
        
          return true;
          
     }


    public function deletion_department($id)

     {

     
           $this->db->where('id',$id);
          $query= $this->db->delete('tbl_department');
          return;

     
     }



     public function update_departmenttype($id,$data)
     {
        $this->db->where('id',$id);
        $sql=$this->db->update('tbl_department',$data);
        
        if($this->db->affected_rows()>0){
                                          return true;
                                          }
                         else{
                              return false;
      
                              }
      }
     
}
?>
<!---->