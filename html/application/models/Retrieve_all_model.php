<?php


class Retrieve_all_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }


    public function get_all_localbody()
     {
                $main_data=array();
                $this->db->select('local_body_type');
                $this->db->from('tbl_local_body_type');
               $query = $this->db->get();
               return $query->result_array();

     }


    public function getalldepartment()
     {
               //correct function
               $main_data=array();
               $this->db->select('department_name');
               $this->db->from('tbl_department');
               $query = $this->db->get();
               return $query->result_array();

     }

     public function get_all_district()
     {
               $main_data=array();
               $this->db->select('district_name');
                $this->db->from('tbl_district');
               $query = $this->db->get();
               return $query->result_array();

     }



     }
     ?>