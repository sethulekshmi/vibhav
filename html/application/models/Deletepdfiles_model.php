<?php
/*
* @fileOverview:Model for deleting the files from the user 
* @author:Jithin Zacharia
* @date:22/02/2017
*/

class  Deletepdfiles_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('form','url');
		$this->load->database();
	}

	public function deletepdf($pdfilename,$pdfullpath){
		$this->db->where('filename',$pdfilename);
		$this->db->delete('helpdocuments');
		delete_files($pdfullpath); 
		return true;
	}
}