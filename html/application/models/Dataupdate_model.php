<?php
//FileName:DataUpdate_model- Model for admin to update items
//Date:23/09/2017
//Author:Jithin Zacharia
class Dataupdate_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();
    }
}
?>