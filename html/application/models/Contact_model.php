<?php

/*
*@fileOverview:Model for adding, updating and deleting emergency contact numbers
*@data:23/02/2018
*@author:Jithin Zacharia
*/

class Contact_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();
    }

   public function addnewcontact($data){
   	$sql=$this->db->insert('emergencycontacts',$data);
   	if($sql->db->affected_rows()>0){
   		return true;
   	}else{
   		return false;
   	}
   }

  public function updatecontact($id,$contactname,$contactnumber,$data){
 
  	$this->db->where('id',$id);
  	$query=$this->db->update('emergencycontacts',$data);
  	$this->db->get();
  	if($query->affected_rows()==0){
  		return true;
  	}else{
  		return false;
  	}
  }

  public function deleteemergenecy($contact_person,$contact_number){
  	$this->db->where('contact_name');
  	$this->db->where('contact_number');
  	$sql=$this->db->delete();
  	$sql->db->get();
  	if($sql->affected_rows()>0){
  		return true;
  	}else{
  		return false;
  	}
  }

  //model for deleting the contact list
  public function deletecontact($id){
  	$this->db->where('id',$id);
  	$this->db->from('emergencycontacts');
  	$sql=$this->db->delete();
  	$sql=$this->db->get();
  	if($sql->affected_rows()>0){
  		return true;
  	}else{
  		return false;
  	}
  }

  //model for making the public info in screen
  public function publicinfo(){
  	$this->db->select('*');
  	$this->db->from('contact_form');
  	$sql=$this->db->get();
  	$results=array();
  	foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'name'=>$row->name,
				'email'=>$row->email,
				'mobile_number'=>$row->mobile_number,
				'subject'=>$row->subject,
				'message'=>$row->message
			);
  	if(!empty($results)){
  		return $results;
  	}else{
  		return false;
  	}
  }

}
}

