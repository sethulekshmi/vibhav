<?php
class Designation_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}

	//function for selecting all the things from db
	function selectalldesignation(){
		$this->db->select('*');
		$this->db->from('designation');
		$sql=$this->db->get();
		$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'designation'=>$row->designation,
				'department'=>$row->department
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	//function for adding new designation from UI
	function addesignation($data){
		$this->db->insert('designation',$data);
		$this->db->select('*');
		$this->db->from('designation');
		$sql=$this->db->get();
		$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'designation'=>$row->designation,
				'department'=>$row->department
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	//function for updating the designation data from UI
	function updatedesignation($id,$data){
		$this->db->where('id',$id);
		$this->db->update('designation',$data);
		$this->db->select('*');
		$this->db->from('designation');
		$sql=$this->db->get();
		$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'designation'=>$row->designation,
				'department'=>$row->department
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	//function for deleting the selected designation from UI
	function deletedesignation($id){
		$this->db->where('id',$id);
		$this->db->delete('designation');
		$this->db->select('*');
		$this->db->from('designation');
		$sql=$this->db->get();
		$results=array();
		foreach ($sql->result() as $row) {
			$results[]=array(
				'id'=>$row->id,
				'designation'=>$row->designation,
				'department'=>$row->department
			);
		}
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}
}