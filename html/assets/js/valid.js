/*
* @fileOverview:Validation for signup/register page
* @author:Jithin Zacharia
* @date: 7th Feb 2017
*/

 $(document).ready(function(){ 
    $(":input").each(function(){
    if($(this).val() == "" ){
        $('#submit').attr('disabled', 'true');      
    }else{
       $('#submit').attr('disabled', 'false');
      }
});


	$("#username").change(function() {
        var user= $(username).val();
        var length= user.length;
        if(length<8){
          document.getElementById("spnFirstName").innerHTML = "<small>* Username must contain atleast 8 characters</small>";
          document.getElementById("submit").disabled = true;
        }else{
          document.getElementById("spnFirstName").innerHTML = "";
        }
      });


      $("#re_pwd").change(function(){  
            var password = $(pwd).val();
            var repassword = $(re_pwd).val();
            if(password === repassword){
              document.getElementById("confirmpassword").innerHTML = "";
              document.getElementById("submit").disabled = false;
            }else{
              document.getElementById("confirmpassword").innerHTML = "<small> * Password doesnt match</small>";
              document.getElementById("submit").disabled = true;
            }
               });


 $("#pwd").change(function(){
    var password = $(pwd).val();
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    if(re.test(password)==false){     
      //document.getElementById("errorpassword").innerHTML = "<small>* Password must contain atleast eight characters with one uppercase and lowercase and number</small>";
      //document.getElementById("submit").disabled = true;
      }else{
        document.getElementById("errorpassword").innerHTML = "";
      }                       
  });


$("#employee").change(function(){
      var e = document.getElementById("employee");
      var strUser = e.options[e.selectedIndex].value;
      //if you need text to be compared then use
      var strUser1 = e.options[e.selectedIndex].text;
      if(strUser1=='Select your option'){
        document.getElementById("selection").innerHTML = " <small>* please select a role</small>";
        document.getElementById("submit").disabled = true;
}else{
    document.getElementById("selection").innerHTML = "";
  }
});
 
   
});
