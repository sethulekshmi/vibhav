<?php
//FileName:Messages_model:model for getting messages from DB
//Date:23/09/2017
//Author:Jithin Zacharia
class Messages_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();
    }
    public function getmessages(){
        $main_data=array();
        $query=$this->db->query('SELECT employee_id,employee_messages,message_time FROM user_message');
        return $query->result_array();
    }
   public function delete_message($id){
        $this->db->where('employee_id',$id);
        $this->db->delete('user_message');
        return true;
   }

    public function contact_admin($data){

        $query=$this->db->insert('contact_form',$data);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else{
        return false;
            }

}
}
?>