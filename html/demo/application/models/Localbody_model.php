<?php 
//FileName:User_model: Model for giving data to asset  for localbody
//Date:23/09/2017
//Author:Jithin Zacharia

class Localbody_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }


    public function get_localbody()
     {
        $main_data=array();
               $this->db->select('id,local_body_type');
            
               $this->db->from(' tbl_local_body_type');
               $query = $this->db->get();
               return $query->result_array();

     }


      public function add_newlocalbody($data)

     {
         $sql=$this->db->insert(' tbl_local_body_type',$data);
        
          
     }


    public function deletion_localbody($id)

     {

     
           $this->db->where('id',$id);
          $query= $this->db->delete(' tbl_local_body_type');
          return;

     
     }



     public function update_localbodytype($id,$data)
     {
        $this->db->where('id',$id);
        $sql=$this->db->update('tbl_local_body_type',$data);
        return ;
      }
}
?>
<!---->