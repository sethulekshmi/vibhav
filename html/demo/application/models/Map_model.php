<?php
class Map_model extends CI_Model{
function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();

}
    // get closest providers
    // around 30 kilo meters from your location
    // by using latitude , longtuide and service id //
    function get_locations(){
       $map_data=array();
       $query=$this->db->query('SELECT surveydata.fullname,asset_profile.employee_id, 
        asset_profile.latitude,asset_profile.longitude, asset_profile.office_name, asset_profile.contact,  asset_profile.email, 
        asset_profile.working_start,asset_profile.working_end from 
        surveydata inner join asset_profile on surveydata.employee_id=asset_profile.employee_id ');
       return $query->result_array();
    }
}
?> 