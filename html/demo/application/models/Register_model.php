<?php
//FileName: Register_model- Model for inserting data to DB
//Date :18/09/2017
//Author:Jithin Zacharia
	Class Register_model extends CI_Model{
		Public function __construct(){
			parent::__construct();
			$this->load->helper('form','url');
		}
		public function insert($data){
			$this->db->insert('user_name', $data);
			if ($this->db->affected_rows() > 1) {
				return true;
			} else {
			return false;
			}
		}
public function compare_db($id){
		$this->db->select('*');
		$this->db->from('user_name');
		$this->db->where('email',$id);
		$this->db->limit(1);

		$sql=$this->db->get();
		if ($sql->num_rows() >0){
			return true;
		}else{
			return false;
		}
	}
	public function compare_username($id){
		$this->db->select('*');
		$this->db->from('user_name');
		$this->db->where('username',$id);
		$this->db->limit(1);

		$sql=$this->db->get();
		if ($sql->num_rows() == 1){
			return $sql->result();
		}else{
			return false;
		}
	}
	}
?>