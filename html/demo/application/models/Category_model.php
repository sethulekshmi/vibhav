<?php 
//FileName:User_model: Model for giving data from catrgory addition
//Date:23/09/2017
//Author:Jithin Zacharia

class Category_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }


    public function get_category()
     {
        $category_data=array();
               $this->db->select('id,institution_category');
            
               $this->db->from('tbl_institution_category');
               $query = $this->db->get();
               return $query->result_array();
     }
      public function add_newcategory($data)
     {
         $sql=$this->db->insert('tbl_institution_category',$data);
          return true;
     }
    public function deletion_category($id)
     {

           $this->db->where('id',$id);
          $query= $this->db->delete('tbl_institution_category');
          return true;
     }
     public function update_categorytype($id,$data)
     {
        $this->db->where('id',$id);
        $sql=$this->db->update('tbl_institution_category',$data);
        if($this->db->affected_rows()>0){
        return true;
      }else{
        return false;
      }
      }

}
?>
<!---->