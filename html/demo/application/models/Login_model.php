<?php
//Filename:LoginModel-Controls user flow into application
//Author :Jithin Zacharia
//Date:18/09/2017 
class Login_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->database();
	}
	public function login($username,$password){
		$this->db->select('*');
		$this->db->from('user_name');
		$this->db->where('username',$username);
		$this->db->where('password',$password);

		$this->db->limit(1);

		$sql=$this->db->get();
		if ($sql->num_rows() == 1){
			return $sql->result();
		}else{
			return false;
		}
	}


	public function compare_login($username,$password){
		$this->db->select('*');
		$this->db->from('user_name');
		$this->db->where('username',$username);
		$this->db->where('password',$password);

		$this->db->limit(1);

		$sql=$this->db->get();
		if ($sql->num_rows() == 1){
			return $sql->result();
		}else{
			return false;
		}
	}
}
?>