<?php
class Home_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form','url');
        $this->load->database();
    }
    public function getdata(){
       $main_data=array();
       $query=$this->db->query('SELECT surveydata.fullname,asset_profile.employee_id, 
        asset_profile.office_name,asset_profile.office_type, asset_profile.services_offered from 
        surveydata inner join asset_profile on surveydata.employee_id=asset_profile.employee_id ');
       return $query->result_array();

       
    } 
    public function get_coordinates(){
        $return = array();
        $this->db->select("latitude,longitude");
        $this->db->from("asset_profile");
        $query = $this->db->get();
        if ($query->num_rows()>0) {
        foreach ($query->result() as $row) {
        array_push($return, $row);
            }
        }
    return $return;  
    }
    public function get_asset_type(){
        $main_datas=array();
        $query=$this->db->query("SELECT DISTINCT office_type FROM asset_profile");
        return $query->result_array();
    }
    public function search_dropdown($id){
        
        $map_data2=array();
          $this->db->where('office_type',$id);
        $this->db->select('employee_id,latitude,longitude'); 
        $this->db->from('asset_profile');
         $query = $this->db->get();        
        return $query->result_array();
                  
    }
    public function search_db($id){
        
        $map_data2=array();
          $this->db->where('office_name',$id);
        $this->db->select('employee_id,latitude,longitude'); 
        $this->db->from('asset_profile');
         $query = $this->db->get();        
        return $query->result_array();
                  
    }
}
?>