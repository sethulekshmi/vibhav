<?php 
//FileName:User_model: Model for giving data to asset  for report
//Date:23/09/2017
//Author:Jithin Zacharia

class Assetupdate_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->database();
    }
    public function get_data()
     {
        $main_data=array();
               $this->db->select('id,employee_id,office_name,administrative_department,
                office_type,address,pincode,building_type,
                landline,district,taluk,village,block,localbody_type,
                localbody_name,email,landmark,contact_person,contact,working_start, working_end,status');
               $this->db->from('asset_profile');
               $this->db->order_by("timestamp","asc");
               $query = $this->db->get();
               return $query->result_array();
              


     }


     public function approve_profile($id)
        {
              $approve_data=array();
              $active='active';
             $this->db->set('status',$active);
             $this->db->where('id',$id);
             $this->db->update('asset_profile');
            return true;
        }



    public function deletion_asset($id)

     {
           $this->db->where('id',$id);
           $query= $this->db->delete('asset_profile');
          return;

     }



     public function get_profile($id,$data)
     {
         $this->db->where('id',$id);
         $sql=$this->db->update('asset_profile',$data);
        return ;
      }
}
?>
<!---->
