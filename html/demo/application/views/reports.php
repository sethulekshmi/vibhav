<!DOCTYPE html>
<head>
<meta charset='utf-8'>
  <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


    <!--Dependend JS Files-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="/assets/js/script.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <title>Super Admin- Dashboard</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
        <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;"href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li> 
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">

     <!-- Page Content -->
     <div id="page-content-wrapper1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
       <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

         

    </div>
      </div>
    <!-- /#wrapper -->



<div style="padding-left:300px; ">
     <!--  datepicker Script -->
 <!--Dry Code-->
<form id="datepickform"  class="form-inline" action="<?=base_url();?>index.php/logincontroller/report_date" method="POST" onsubmit="cleared()">
  <div class="form-group">
    <label for="date">From Date:</label> 
    <input type="text" class="form-control" id="from_date" name="from_date" required >
  </div>
  <div class="form-group">
    <label for="date">To Date:</label>
    <input type="text" class="form-control" id="to_date" name="to_date" required >
  </div>
  <div class="form-group">
    <button class="btn btn-danger" type="submit" id="report_generate">Generate Report</button>
   
  </div>
  
</form> 

<script>
  function cleared(){
      $("#centred").hide(); // hide body div tag
  }
</script>
<script>
  $(function(){

$("#from_date").datepicker({ dateFormat: 'yy-mm-dd' });
$("#to_date").datepicker({ dateFormat: 'yy-mm-dd' });
  });
</script>

</div>
<?php if ($this->session->flashdata('entry')) { ?>
               <div class="alert alert-danger" id="centred"> <center><h4> <?= $this->session->flashdata('entry') ?> </h4></center>  </div>
                        <?php } ?>
                      
</div>

    <!-- /#wrapper -->
     <!-- Menu Toggle Script -->

  <!-- for disable buttons  -->    
     <script>
    
    $(document).ready(function() {
       validate();
   $('input').on('keyup', validate);
                       });

function validate() {
  var inputsWithValues = 0;
  
  // get all input fields except for type='submit'
  var myInputs = $("input:not([type='submit'])");

  myInputs.each(function(e) {
    // if it has a value, increment the counter
    if ($(this).val()) {
      inputsWithValues += 1;
                        }
  });

  if (inputsWithValues == myInputs.length) {
    $("input[type=submit]").prop("disabled", false);
  } else {
    $("input[type=submit]").prop("disabled", true);
  }
}
  </script>




    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
