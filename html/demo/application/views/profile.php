<html>
<head>
<meta charset='utf-8'>
        <!-- Dependend CSS Files -->
        <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
      <link rel="stylesheet" type="text/css" href="/assets/css/style.css"> 
         <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
               
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">

        <!--Dependend JS Files-->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap.js"></script>
        <script type="text/javascript" src="/assets/js/script.js"></script>
</head>
<body>
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
    <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>
      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav"></ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu"> 
        <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>     
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

<div class="container">
<div id="wrapper">
  <div class="row well">
    <div class="col-md-3">
          <ul class="nav nav-pills nav-stacked well">
              <li  class="active"><a href="#"><i class="fa fa-user-circle"></i> Profile</a></li>
             
              <li><a href="#" id="<?php echo $profile_data['employee_id'];?>" onclick="usr_asset(this);return false">
              <i class="glyphicon glyphicon-globe"></i> Assets</a></li>
              <li><a href="#" id="<?php echo $profile_data['employee_id'];?>" onclick="remove(this);return false">
              <i class="fa fa-trash"></i>Delete User</a></li>
            </ul>
        </div>
       



        <div class="col-md-9">

                <div class="panel">
                    <img class="pic img" id="profile" style="height: 120px;
                    width: 120px;" src="/assets/imgs/user1.png" alt="...">
                </div>
      <!--           <b><?php 
                  //echo json_encode($profile_data);

                  // echo str_replace("\"","",json_encode($profile_data['employee_name']));

?></b> -->
                    <div style="margin-left:20px" class="name"><b><?php 
                  //echo json_encode($profile_data);

                  echo str_replace("\"","",json_encode($profile_data['fullname']));

?></b></div>

    <br><br><br>
   
    


     </div>
  </div>  
<?php if(empty($asset_data)) {

  echo "No assets found";
}
else{

foreach($asset_data as $data){ 
  echo $data['office_name']; ?>
  
<?php }} ?>

  </div>  
</div>
<script type="text/javascript">

    function remove(el)
 {

          var idval=el.id;
         
      console.log(idval);
  
      post("<?=base_url()?>index.php/logincontroller/deleteuser",{id:idval});

     

}
 function usr_asset(el)
 {

          var idval=el.id;
         
      console.log(idval);
  debugger;
      post("<?=base_url()?>index.php/logincontroller/user_asset",{id:idval});

     

}
function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
   </script>
</body>              
</html>

