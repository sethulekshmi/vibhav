<!DOCTYPE html>
<head>
<meta charset='utf-8'>
        <!-- Dependend CSS Files -->
        <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <!--Dependend JS Files-->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/script.js"></script>
        <title>Super Admin- Messages</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
        <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li> 
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
         <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                  <i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
        </div>
    </div>
</div>

     
<!--user serach-->
<!--user search-->
<div id="distance"> </div>
<!--user search results list-->


          
<?php if(empty($main_data)) {?>

<center> <h4> <?php echo "No users found";?> </h4> <center>
<?php }
else{
foreach($main_data as $data){ ?>
  <div id="searchresult" class="container">
	<div class="row" >
    		<ul class="thumbnails" style="list-style-type:none;">
                <li class="span5 clearfix">
                  <div class="thumbnail clearfix">
                    <img src="/assets/imgs/user1.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:90px;'>
                    <div class="caption" class="pull-left">
                    <?php if($data['status']=="pending"){ ?>
                     <a href="#" class="btn btn-success icon pull-right" id="<?php echo $data['employee_id'];?>"
                     onclick="approve(this);return false">approve</a>
                      <?php } ?>
                        <?php if($data['status']=="active"){ ?>
                      <a href="#" class="btn btn-warning icon  pull-right"id="<?php echo $data['employee_id'];?>" onclick="block(this);return false"><i class="fa fa-times fa-lg"></i>Block</a>
                      &nbsp;
                      <?php } ?>
                      <a href="usermodule"  class="btn btn-danger icon  pull-right" id="<?php echo $data['employee_id'];?>" onclick="remove(this)">
                      <i class="fa fa-trash-o fa-lg"></i>Delete</a>
                      
                      <h4>      
                      <a style="cursor:pointer;" id="<?php echo $data['employee_id'];?>" onclick="passid(this);return false" >
                      <?php echo $data['fullname'];?> </a>
                      </h4>
                      <small><b>Employee ID:&nbsp; </b><b id="employee_id"><?php echo $data['employee_id'];?></b> </small> <br/>
                      <small ><b>
                       Department:&nbsp;</b><b><?php echo $data['employee_department'];?></b></small><br/>
                    </div>
                  </div>
                </li>
            </ul>
  
    </div>
    </div>
<?php }} ?>
</div>
<!--user search results list ends-->
    <!-- /#wrapper -->
     <!-- Menu Toggle Script -->
    <script>

    function remove(el)
    {

    	    var id=el.id;

			console.log(id);
	


			$.ajax( 
			{
			type:'POST',
			url:"<?=base_url()?>index.php/logincontroller/deleteuser",
			data:{"id":id},
			dataType:"json",
    		success: function () {   // success callback function
       	console.log("success");
    		$(el).parent().remove();
    		},
    		error: function () { // error callback 
       		console.log("fail");
    		}
	});
}
 // function for passing prof id
    function passid(el)
 {

          var idval=el.id;
         
      console.log(idval);
  
      post("<?=base_url()?>index.php/logincontroller/pass_id",{id:idval});

     

}

function approve(el)
 {

          var idval=el.id;
         
      console.log(idval);
  
      post("<?=base_url()?>index.php/logincontroller/approve",{id:idval});

     

}


function block(el)
 {

          var idval=el.id;
         
      console.log(idval);
  
      post("<?=base_url()?>index.php/logincontroller/block",{id:idval});
}
    
function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
   	});
   
    </script>
</body>
</html>
