<!DOCTYPE html>
<head>
<meta charset='utf-8'>
        <!-- Dependend CSS Files -->
        <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
       
        <!--Dependend JS Files-->
         
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/script.js"></script>
        
        <title>Super Admin-edit</title>

         
    
</head>
<body>

<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand"style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
      <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
         <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                  <i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
        </div>
    </div>
</div>

<!-- Page Content inside this  -->







<!--beginning of district modal-->
<table class="table table-hover table-bordered" id="districtTable" >


     <tr><td><strong>Districts</strong></td>  <td><button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#adddistrict">Add</button></td></tr> 

   <tbody>
           <?php foreach($district_data as $data){?>
           <tr>
            <td style="display:none"><?php echo $data['id'];?></td>
            <td><?php echo $data['district_name'];?></td>
            <td><button type="button"  id="updatedistrict" class="btn btn-info btn-md" data-toggle="modal"  data-target="#districtupdateModal">Update</button></td>
           
           </tr>
    </tbody>
           <?php }?> 



</table>

</div>


<!--end of district table-->






    <!-- Modal for adding district-->
  <!-- Modal -->
  <div class="modal fade" id="adddistrict" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add a district</h4>
        </div>
        <div class="modal-body">
           <form class="form-horizontal">
              <div class="form-group">
                 <label class="col-sm-4 control-label">District</label>
                     <div class="col-sm-8"> 
                        <input type="text"  id="adddistrictname"></br>
                    </div>
              </div>
            </form>
               <script type="text/javascript">
 
                      var x = document.getElementById("districtTable").rows.length;




                     $('table tbody tr  td').on('click',function()
                            {
                                $("districtupdateModal").modal("show");

                                $("#id").val($(this).closest('tr').children()[0].textContent);
                                $("#district_name").val($(this).closest('tr').children()[1].textContent);

                                 
                       
                       

           
                            });

                              

                  </script>

        <div class="modal-footer">


            <a href="messages" data-dismiss="modal" class="btn btn-success" id="adddistrictbutton">add</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  </div>



  <script type="text/javascript">

$('#adddistrictbutton').click(function() 

            {
                 var district_name = $("#adddistrictname").val();
                 console.log(district_name);

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/add_district'); ?>",
                             data: { 'district_name':district_name},

                              
                             success: function(response)
                                     {
                                        console.log("success");
                                        location.reload();
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });
</script>

  <!--end of add_modal--> 






<!--begin of district update modal-->


 <!-- Modal -->
  <div class="modal fade" id="districtupdateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">update district</h4>
        </div>
        <div class="modal-body">
          
        
            <form class="form-horizontal">
              <div class="form-group"  style="display:none" >
                <label class="col-sm-4 control-label">ID</label>
                  <div class="col-sm-8"> 
                     <input type="text" disabled="true" id="id"></br>
                  </div>
             </div>
             <div class="form-group">
                <label class="col-sm-4 control-label">District</label>
                  <div class="col-sm-8"> 
                     <input type="text"  id="district_name"></br>
                  </div>
             </div>
            </form>
               

        <div class="modal-footer">
          <a  data-dismiss="modal" class="btn btn-success" id="updateButton">Update</a>
          <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger" >Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>
      </div>
      
    </div>
  </div>




<script type="text/javascript">

$('#deleteButton').click(function() 

            {
                 var id = $("#id").val();

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/delete_district'); ?>",
                             data: { 'id':id},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                        location.reload();
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });








$('#updateButton').click(function() 
      {    

                 var id = $("#id").val();
                var district_name = $("#district_name").val();
                 $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('index.php/logincontroller/update_district'); ?>",
                          data: { 
                                  'id':id,
                                  ' district_name': district_name,
                                  
                                },
                     success: function(response)
                       {
                         console.log("success");
                         location.reload()
                        },
                    error: function(err){
                                       console.log('error');
      
                                        },

                              });
               
      
      });

</script>




<!--end of update modal-->











<!-- Page Content inside this  -->

     <script>

  
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

   
    </script>

</body>
</html>
