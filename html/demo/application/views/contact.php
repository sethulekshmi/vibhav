<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<title><?php echo $title;?></title>
	</head>
	<body>

	
	<div id="contactform">
	<div class="container">
     <div class="col-md-8">
       <div class="form-area">  
        <form id="form1">
       
                    <h3 style="margin-bottom: 25px; text-align: center ;">Contact</h3>
            
    				<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" required>
						<span id='remainingD'></span>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
					</div>
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" maxlength="200" placeholder="Message (limited to 200 characters)"  rows="7"></textarea>
                    <span id='remainingC'></span>
                                           
                    </div>
            
<!--             <button type="button" id="submit" name="submit_button" class="btn btn-primary pull-right">Submit Form</button>
 -->            <button type="button" id="button_submit" class="btn btn-primary pull-right">Submit</button>
            
        </form>
        
    </div>
</div>
</div>
</div>

	</body>
	<script type="text/javascript">
	$('#button_submit').click(function() 

            {
            
                 var name = $("#name").val();
                  var email = $("#email").val();
                   var mobile_number = $("#mobile_number").val();
                    var subject = $("#subject").val();
                     var message = $("#message").val();

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/vibhav/contactadmin'); ?>",
                             data: { 'name':name,'email':email,'mobile_number':mobile_number,'subject':subject,'message':message},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                        alert("Your Message has been sent");
                                        location.reload();
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });

	</script>

</html>
