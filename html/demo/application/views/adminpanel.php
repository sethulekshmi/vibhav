<!DOCTYPE html>
<html>
<head>

<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
    


		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/bootstrap.js"></script>
		<script type="text/javascript" src="/assets/js/script.js"></script>
         <title>Admin-Home</title>
<style>
.center {
    margin: auto;
    width: 40%;
    
    padding: 10px;
  }
#dropdownli{
  cursor: pointer;
}

</style>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
    <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>
      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
         <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                  <i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
        </div>
    </div>
</div>



    <!--Search-->
<div class="container"> <div class="center"> 
<div class="form-inline">
  <div class="row">
    <div class="form-group">
    
  <div class="form-group">
  <div class="dropdown" >
  <label>Search Assets by Type</label>
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width:370px;">Asset Type
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
    <?php foreach($asset_type as $data){ ?>
    <li  id="dropdownli" ><a href="#" id="<?php echo $data['office_type'];?>" onclick="assetsearch(this);return false">
      <?php echo $data['office_type'];?> </a>
    </li>
      <?php } ?> 
    </ul>
  </div>
  </form>
      </div>
      </div>
  </div>
  </div>
</div></div>
   <!-- admin main content starts-->
   
 <div id="map" style="height: 390px; width:955px;  margin-left:30px; border:5px solid rgb(255, 255, 255);" >
       
   <!-- google map content starts-->        
           
    <script>

      function initMap() {

        var mapData=JSON.parse('<?php echo json_encode($map_data); ?>');
console.log(mapData);

        if(mapData.length>0)
        {

var uluru = {lat: parseFloat(mapData[0].latitude), lng: parseFloat(mapData[0].longitude)};
  
          var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
            center: uluru
            });
        
        for ( i=0;i< mapData.length;i++) {
  var markerLatLong = {lat: parseFloat(mapData[i].latitude), lng: parseFloat(mapData[i].longitude)};
  
   console.log(mapData[i].office_name);
          var marker = new google.maps.Marker({
          position: markerLatLong,
          map: map
            }); 
          var contentString = mapData[i].office_name

      alertMarkerMessage(marker,contentString);
    
}

        }

        
      }

   function alertMarkerMessage(marker,contentString)
   {

  google.maps.event.addListener(marker, "click", function () {
            var infowindow = new google.maps.InfoWindow();
  infowindow.setContent(contentString);
  infowindow.open(map, this);
});
       

   }   
      
    </script>
   <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA283iCG_L0z7_Q2YRrvkYkUXgSVT5EgsA&language=en&region=IN&callback=initMap">
          
          </script>
 </div>
     
   </div>
  
 <!-- google map content ends-->
    <script>

  

   function assetsearch(el)
 {

          var idval=el.id;
         
      console.log(idval);
      
      post("<?=base_url()?>index.php/logincontroller/searchdropdown",{id:idval});

     

}

   function searchdb(el)
 {
      var idval = document.getElementById('name').value;
          // var idval=el.value;
         
      console.log(idval);
      
      post("<?=base_url()?>index.php/logincontroller/searchdb",{id:idval});

     

}
function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
    
$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
   </script>

    

</body>
</html>