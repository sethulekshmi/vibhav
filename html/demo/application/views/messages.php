<!DOCTYPE html>
<head>
<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/js/script.js"></script>
        <title>Super Admin- Messages</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
  </ul>
  <ul class="nav navbar-nav navbar-right">
  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
		 <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
			      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
        </div>
    </div>
</div>


<?php if(empty($main_data)) {?>

<center> <h4> <?php echo "No Messages found";?> </h4> <center>
<?php }
else{?>
 
 <?php foreach($main_data as $data){ ?>
  <div id="searchresult" class="container">
	<div class="row" >
    		<ul class="thumbnails"style="list-style-type:none;">
                <li class="span5 clearfix">
                  <div class="thumbnail clearfix">
                    <img src="/assets/imgs/user1.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:90px;'>

                    <div class="caption" class="pull-left">
                      &nbsp;<a href="messages" onclick="remove(this)" class="btn btn-danger icon  pull-right"><i class="fa fa-trash-o fa-lg"></i>&nbsp Delete</a>
            
                      <h4>      
                      
                      </h4>
                      <small><b>Employee ID:&nbsp; </b><b id="employee_id"><?php echo $data['employee_id'];?></b></small> </small> <br/>
                     
                      <small id="employee_messages"><b>Employee Message:&nbsp;</b><b><?php echo $data['employee_messages'];?></b></small><br/>
                      <small id="message_time"><b>Message Time:&nbsp;</b><b><?php echo $data['message_time'];?></b></small><br/>
                    </div>
                  </div>
                </li>
            </ul>
        </div>
     </div>
<?php }}?>
<!--user search results list ends-->
    <!-- /#wrapper -->
    </div>
     <!-- Menu Toggle Script -->

     <script>

    function remove(el)
    {
     var id=$('#employee_id').text();
      console.log(id);
    var c_obj = $(this).parents("div");
      c_obj.remove();

      debugger;
      $.ajax( 
      {
      type:'POST',
      url:"<?=base_url()?>index.php/logincontroller/deletemessage",
      data:{"id":id},
      dataType:"json",
        success: function () {   // success callback function
        console.log("success");
        
        $(el).parent().parent().remove();
      },
        error: function () { // error callback 
          console.log("fail");
        }
  }); 
    }
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    </script>


</body>
</html>
