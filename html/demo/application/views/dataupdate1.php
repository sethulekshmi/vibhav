<!DOCTYPE html>
<head>
<meta charset='utf-8'>
    <!-- Dependend CSS Files -->
    <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
    <!--Dependend JS Files-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/script.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <title>Super Admin- DataUpdate</title>
</head>


<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand"style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>


<!--Sidebar-->

<div id="wrapper">
        <div id="sidebar-wrapper" style="top:50px;">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#" id="main_li">Dashboard</a>
                </li>
                <li>
                    <a href="admin"><i class="fa fa-home fa-lg" aria-hidden="true"></i>Home</a>
                </li>
                <li>
                    <a href="usermodule"><i class="fa fa-user fa-lg" aria-hidden="true" class="dcjq-parent active"></i> <span>User Module</span></a>
                     
                </li>
                <li>
                    <a href="reports"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"  ></i>Reports</a>
                </li>
                <li>  
                    <a href="analytics"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i>Analytics</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bullhorn fa-lg" aria-hidden="true"></i>FeedBack and Suggestions</a>
                </li>
                <li>
                    <a href="dataupdate"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>Data Updatation</a>
                </li>
                <li>
                    <a href="messages"><i class="fa fa-info fa-lg" aria-hidden="true"></i>Messages</a>
                </li>
            </ul>
        </div>
 </div>    


<!-- Page Content -->


         <!-- Page Content -->
<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                  <i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
        </div>
    </div>
</div>

<div class="container">
<table class="table table-hover table-bordered" id="myTable">
     <tr><td><strong>Id</strong></td> <td><strong> Emp Id</strong></td><td><strong>office Name</strong></td><td><strong>address</strong></td><td><strong>type</strong></td><td><strong>contact</strong></td><td><strong>Email</strong></td><td><strong>services</strong></td><td><strong>start</strong></td><td><strong>end</strong></td><td><strong>other details</strong></td></tr> 

<tbody>
<?php foreach($main_data as $data){?>



            <tr>
            <td><?php echo $data['id'];?></td>
            <td><?php echo $data['employee_id'];?></td>
            <td><?php echo $data['office_name'];?></a></td>
            <td><?php echo $data['address'];?></td>
            <td><?php echo $data['office_type'];?></td>
            <td><?php echo $data['contact'];?></td>
            <td><?php echo $data['email'];?></td>
            <td><?php echo $data['services_offered'];?></td>
            <td><?php echo $data['working_start'];?></td>
            <td><?php echo $data['working_end'];?></td>
            <td><?php echo $data['details'];?></td>

            
           <td><button type="button"  id="update" class="btn btn-info btn-md" data-toggle="modal"  data-target="#myModal">Update</button></td>
         </tr></tbody>
        <?php }?> 

           
       </table>



       <script>
       $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
   </script>
</div>
</body>
</html>
