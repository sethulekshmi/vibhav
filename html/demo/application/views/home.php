<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/parallax.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">


		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<title>Vibhav-Home</title>
	</head>
	<body>
  <!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
  </ul>
  <ul class="nav navbar-nav navbar-right">
 <li><a href="<?php echo base_url('index.php/vibhav'); ?>"><i class="fa fa-sign-in " aria-hidden="true">Log in</i></a></li>
 <li><a href="<?php echo base_url('index.php/vibhav/register'); ?>"><i class="fa fa-user-plus" aria-hidden="true">Sign Up</i></a></li>
    </ul>
  </div>
</nav>



<div id="parallax-world-of-ugg">
  
<section>
  <div class="title">
    
    <h1>KERALA STATE GOVERNMENT</h1>

      
  </div>
</section>
<section>
    <div class="parallax-one">
      <h2>VIBHAV</h2>
    </div>
</section>

<section>
  <div class="block">
    <p><span class="first-character sc">F</span>ocus of the application is to make comprehensive utilization of KSDI services/database and 
geo-portal. The implementation of the project will help the user for better planning and decision 
making using reliable and updated data. It will also make benefits of Bhuvan/Google Map/Open 
Street services for data preparation as a base map. Reliable data collection using Akshaya stake 
holders (’s wide network) is the key feature of the project. </p>
    <p class="line-break margin-top-10"></p>
    <p class="margin-top-10">The project Vibhav is aimed to create Socio-Economic Asset database for Kerala State including 
government offices, educational institutions, hospitals, shopping malls etc. KSDI Geoportal will 
act as an effective platform to collect and update information regarding different assets of the 
state. The application is planned to develop in a hybrid environment. </p>
  </div>
</section>

<section>
  <div class="parallax-two">
    <h2>ADMINISTRATION</h2>
  </div>
</section>

<section>
  <div class="block">
    <p><span class="first-character ny">T</span>he Admin console is where administrators manage surveyors, status of the survey, report 
making, general trend in overall survey, manage feedback etc. Admin can sent messages to the 
surveyors. The web console will provide a detailed reporting module. Reports can be generated 
based on Category of Entities, Surveyors, District wise, Agency, Active Agents, Reports on 
inaccurate survey etc.. If admin finds any surveyor is capturing locations repeatedly incorrect 
and need to stop him immediately, can be suspended the surveyor and will not be able to access 
the app anymore. Or else the admin can able to hang up all the surveyors all of a sudden, if 
required. 
</p>
    <p class="line-break margin-top-10"></p>
    <p class="margin-top-10">Fueled by celebrities from coast to coast wearing UGG boots and slippers on their downtime, an entirely new era of fashion was carved out. As a result, the desire and love for UGG increased as people wanted to go deeper into this relaxed UGG experience. UGG began offering numerous color and style variations on their sheepskin boots and slippers. Cold weather boots for women and men and leather casuals were added with great success. What started as a niche item, UGG sheepskin boots were now a must-have staple in everyone's wardrobe. More UGG collections followed, showcasing everything from knit boots to sneakers to wedges, all the while maintaining that luxurious feel UGG is known for all over the world. UGG products were now seen on runways and in fashion shoots from coast to coast. Before long, the love spread even further.</p>
  </div>
</section>

<section>
  <div class="parallax-three">
    <h2>SURVEY</h2>
  </div>
</section>

<section>
  <div class="block">
    <p><span class="first-character atw">W</span>hen the New York fashion community notices your brand, the world soon follows. The widespread love for UGG extended to Europe in the mid-2000's along with the stylish casual movement and demand for premium casual fashion. UGG boots and shoes were now seen walking the streets of London, Paris and Amsterdam with regularity. To meet the rising demand from new fans, UGG opened flagship stores in the UK and an additional location in Moscow. As the love spread farther East, concept stores were opened in Beijing, Shanghai and Tokyo. UGG Australia is now an international brand that is loved by all. This love is a result of a magical combination of the amazing functional benefits of sheepskin and the heightened emotional feeling you get when you slip them on your feet. In short, you just feel better all over when you wear UGG boots, slippers, and shoes.</p>
    <p class="line-break margin-top-10"></p>
    <p class="margin-top-10">In 2011, UGG will go back to its roots and focus on bringing the active men that brought the brand to life back with new styles allowing them to love the brand again as well. Partnering with Super Bowl champion and NFL MVP Tom Brady, UGG will invite even more men to feel the love the rest of the world knows so well. UGG will also step into the world of high fashion with UGG Collection. The UGG Collection fuses the timeless craft of Italian shoemaking with the reliable magic of sheepskin, bringing the luxurious feel of UGG to high end fashion. As the love for UGG continues to spread across the world, we have continued to offer new and unexpected ways to experience the brand. The UGG journey continues on and the love for UGG continues to spread.</p>
  </div>
</section>
  
</div>


</body>
</html>
