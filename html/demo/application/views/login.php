<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<title>Sign in</title>
	</head>
	<body>



	<div id="main_body">
		<div class="flex-container">
  			<div class="flex-item">
        <?php if ($this->session->flashdata('error')) { ?>
               <div class="alert alert-danger"> <center> <?= $this->session->flashdata('error') ?> </center>  </div>
                        <?php } ?>

  			<div class="main_label">
  			<center><label><h3>Login</h3></label></center>
  			</div>
			  <?php echo form_open('logincontroller/index');?>
  				<div class="form-group">
    				<label class="control-label col-sm-2" for="user_name">Username:</label>
    				<div class="col-sm-10">
      					<input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" required>
    				</div>
  				</div>
  			<div class="form-group">
    			<label class="control-label col-sm-2" for="pwd">Password:</label>
    			<div class="col-sm-10"> 
      				<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
    			</div>
  			</div>
  		<div class="form-group"> 
    		<div class="col-sm-offset-2 col-sm-10">
      			<li style="list-style-type:none;"><a href="<?=base_url()?>index.php/logincontroller/doforget" >Forgot password</a>
    		</li>
        </div>
  		</div>
  			<div class="form-group"> 
    			<div class="col-sm-offset-2 col-sm-10">
      				<button type="submit"  class="btn btn-md btn-primary">Login</button>
    			</div>
  			</div>
			<?php echo form_close();?>
			<label>&nbsp &nbsp New to Vibhav? <a href="<?=base_url()?>index.php/logincontroller/register" >&nbspRegister Now</a></label>
  			</div>
		</div>
	
</div>
	</body>
  <script type="text/javascript">
 
//   $('#submit').click(function(){
         
  
//             var username=$('#username').val();
//             var password=$('#pwd').val();
//             console.log(username);
//             debugger;
//       post("<?=base_url()?>index.php/logincontroller/login_comparison",{username:username,password:password});


         
//               });
//   function post(path, params, method) {
//     method = method || "post"; // Set method to post by default if not specified.

//     // The rest of this code assumes you are not using a library.
//     // It can be made less wordy if you use one.
//     var form = document.createElement("form");
//     form.setAttribute("method", method);
//     form.setAttribute("action", path);

//     for(var key in params) {
//         if(params.hasOwnProperty(key)) {
//             var hiddenField = document.createElement("input");
//             hiddenField.setAttribute("type", "hidden");
//             hiddenField.setAttribute("name", key);
//             hiddenField.setAttribute("value", params[key]);

//             form.appendChild(hiddenField);
//         }
//     }

//     document.body.appendChild(form);
//     form.submit();
// }
</script>

</html>
