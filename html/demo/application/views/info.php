<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/style2.css">

		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">

		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<title><?php echo $title;?></title>



<meta name="viewport" content="width=device-width, initial-scale=1.0">

	</head>
	<body>


	<div class="infowin" style="padding-top:130px;">
         <div class="main" >


                <h4>KERALA STATE INFORMATION TECHNOLOGY MISSION</h4>

                <p>Kerala  State  IT  Mission  (KSITM),  a  government  organization  under  Department  of  Information 
Technology,  Kerala,  is  the  nodal  agency  of  IT  Department  for  E-Governance,  IT  infrastructure 
development  and  IT  enabled  services.  It  is  headed  by  the  Director,  with  the  Secretary-E  &IT 
Department,  Govt.  of  Kerala  as  the  Chairman.  The  Governing  body  of  KSITM  is  chaired  by  the 
Minister for Information Technology. KSITM is also responsible for according technical advice to the
Government of Kerala in relation to IT related endeavors. </p>




   

                 <h4>KERALA STATE SPATIAL DATA INFRASTRUCTURE (KSDI)</h4>

                <p>Kerala State Spatial Data Infrastructure (KSDI) has been established under KSITM for the 
purpose of acquiring, processing, storing, distributing and improving utilization of spatial data. 
It is in line with the National Spatial Data Structure initiative.  
KSDI facilitates a framework of geographic data, metadata and tools that are interactively 
connected in order to use spatial data in an efficient and flexible way through a Web-GIS 
platform  (Kerala  Geo-portal).  It  acts  as a central gateway which  enables  users  to  access 
organized spatial data (online/offline) for better planning and decision making. </p>
     
  </div>


<div class="right">


            <h4> AKSHAYA </h4>



<p>Akshaya, an ambitious project of the State of Kerala was launched on 18th Nov 2002 by Dr. APJ 
Abdul Kalam aiming to bridge the digital divide and to bring the benefits of ICT to the entire 
population of the state. Akshaya centres have since grown manifold and emerged as of the finest 
network of effective Common Service Centres (CSC) that help the public to avail a multitude of 
G2C, G2B as well as B2C services under one roof. At present, nearly 2,650 Akshaya e-centres 
have been set up across Kerala with every panchayat having at least 2 centres. The multitude of 
services offered through Akshaya centres are Aadhaar enrolment, e-District services, utility bill 
payments, ration card applications, Motor vehicle license payments, RSBY CHIAK enrolments, 
Labour welfare boards Aadhaar seeding, Farmer’s data entry, SC/ST pre-metric scholarship 
applications,  commercial  tax  e-filing,  FBO  registrations,  Pharmacist  registrations,  KASE 
registration, Haj registration and University Fees. </p>

</div>	


 </div>

	</body>

</html>