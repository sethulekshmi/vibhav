<!DOCTYPE html>
<head>
<meta charset='utf-8'>
    <!-- Dependend CSS Files -->
    <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!--Dependend JS Files-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/script.js"></script>
        <title>Super Admin- Reset Password</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
  </ul>
  <ul class="nav navbar-nav navbar-right">
  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"  href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
         <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->


         
 <div class="col-xs-6" style="padding-left:400px;">
  <form>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control" id="email">
  </div>
  <div class="form-group">
    <label for="pwd">Old Password:</label>
    <input type="password" class="form-control" id="pwd">
  </div>
  <div class="form-group">
    <label for="pwd">New Password:</label>
    <input type="password" class="form-control" id="re_pwd">
  </div>
  <button type="submit" class="btn btn-default" onclick="readvalue();">Submit</button>
</form>
        </div>
  <script>
  function readvalue(){
    debugger;
  var email=document.getElementById('email').value;
  var old_password=document.getElementById('pwd').value;
  var new_password=document.getElementById('re_pwd').value;
  $.ajax({
    type:"POST",
    url:"<?php echo base_url('index.php/logincontroller/change'); ?>",
    data:{
          'email':email,
          'old_password':old_password,
          'new_password':new_password
  },
  success:function(data){
    console.log('Password updated')
  },
  error:function(err){
    console.log('Error');
  }
  });
}
  </script>    
    </body>

    </html>