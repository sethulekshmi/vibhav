<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
    <!-- Dependend CSS Files -->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
      <link rel="stylesheet" type="text/css" href="/assets/css/w3button.css">
    <!--Dependend JS Files-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/js/valid.js"></script>
  <title>Sign Up</title>
</head>
<body>




<div id="main_body">


<div class="flex-containersignup">
<div id="signupbox">
<div class="container-fluid">
 <div id="headsign">
<div id="signupheading"><center><h3>Sign up</h3></center></div>
<form id="frmRegister" action="vibhav/insert_data" method="post">
 
    <div class="form-group" >
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      <span style="display:none;">Email already exist</span>
<span class="spnemail" id="spnemail"></span>
    </div>
    <div class="form-group">

      <label for="username">Username:</label>
      <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" required>
<span class="spnFirstName" id="spnFirstName"></span>
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required><span class="errorpassword" id="errorpassword"></span>
    </div>
    <div class="form-group">
      <label for="pwd">Repeat Password:</label>
      <input type="password" class="form-control" id="re_pwd" placeholder="Repeat Enter password" name="re_pwd" required>
<span id="confirmpassword"></span>
    </div>

    <div class="form-group">
  <label for="employee">Select Employee Type:</label>
  <select class="form-control" id="employee" name='role'>
      <option value="" disabled selected>Select your option</option>
    <option>Super Admin</option>
    <option>KSDI Admin</option>
    <option>Akshya Admin</option>
  </select>
<span class="selection" id="selection" name="selection"></span>
</div>


<!--<div class="form-group">
    <input type='hidden' class="form-control" name='status' value='<?php //echo "$status";?>'/>
</div> -->

</div><p>Already a user?</p>
<button type="button"  class="btn btn-success" onclick="re_direct()" >
Log in</button>
    <button type="button" id="submit" class="btn btn-primary pull-right" >Sign Up</button>
 </form>
</div>
</div>
</div>
</div>
</div>
</div>
        <script type="text/javascript">
         
        $('#submit').click(function(){
            
            var username=$('#username').val();
            var password=$('#pwd').val();
            var email_data= $('#email').val();
            var employee= $('employee').val();
            console.log(email_data);
            $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('index.php/logincontroller/register_comparison'); ?>",
                          data: { 
                                  'id':email_data,
                                  
                                },
                     success: function(response)
                       {
                        
                        if(!response ){
                      console.log(email_data);
                      console.log(response);
                      //document.getElementById("frmRegister").submit();

                        var paramstr = $("#frmRegister").serialize();
                        $.post("<?php echo base_url('index.php/vibhav/insert_data');?>",paramstr)
                        alert("Successfully Registered");
                        window.location.href="<?php echo base_url('index.php/vibhav/index');?>";

                        }
                        else{
                          console.log(response);

                          alert("email  already  exists");
                          location.reload();
                        }
                       

                        
                      //window.location.href="test/index";
                        },
                             
    error: function(err){
        console.log('error');
      console.log(err);
    },
  });
        });

function re_direct()
{
  window.location.href="<?php echo base_url('index.php/vibhav/index');?>";
}
            



//     $("#email").change(function(){
//        var email_data= $(email).val();
//       console.log(email_data);
//   debugger;   console.log(email_data);
//       post("<?=base_url()?>index.php/logincontroller/register_comparison",{id:email_data}); 

//    });
//  $("#username").change(function(){
//        var username_data= $(username).val();
//       console.log(username_data);
//   debugger;   console.log(username_data);
//       post("<?=base_url()?>index.php/logincontroller/username_comparison",{id:username_data}); 

//    });


// function post(path, params, method) {
//     method = method || "post"; // Set method to post by default if not specified.

//     // The rest of this code assumes you are not using a library.
//     // It can be made less wordy if you use one.
//     var form = document.createElement("form");
//     form.setAttribute("method", method);
//     form.setAttribute("action", path);

//     for(var key in params) {
//         if(params.hasOwnProperty(key)) {
//             var hiddenField = document.createElement("input");
//             hiddenField.setAttribute("type", "hidden");
//             hiddenField.setAttribute("name", key);
//             hiddenField.setAttribute("value", params[key]);

//             form.appendChild(hiddenField);
//         }
//     }

//     document.body.appendChild(form);
//     form.submit();
// }


     
     </script>
</body>
</html>
