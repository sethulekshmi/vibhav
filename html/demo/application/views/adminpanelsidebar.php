<div id="wrapper">
        <div id="sidebar-wrapper" style="top:50px;">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#" id="main_li">Dashboard</a>
                </li>
                <li>
                    <a href="admin"><i class="fa fa-home fa-lg" aria-hidden="true"></i>Home</a>
                </li>
                
                <li>
                    <a href="usermodule"><i class="fa fa-user fa-lg" aria-hidden="true" class="dcjq-parent active">
                      
                    </i> <span>User Module</span></a>
                     
                </li>
                <li>
                    <a href="reports"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"  ></i>Reports</a>
                </li>
                <li>    
                    <a href="analytics"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i>Analytics</a>
                </li>
                
                <li>
                    <a href="dataupdate"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>Data Updation</a>
                </li>
                <li>
                    <a href="messages"><i class="fa fa-info fa-lg" aria-hidden="true"></i>Messages</a>
                </li>
                 <li>
                    <a href="category"><i class="fa fa-th-list fa-lg" aria-hidden="true"></i>Categories</a>
                </li>
                 <li>
                    <a href="districts"><i class="fa fa-flag-o fa-lg" aria-hidden="true"></i>Districts</a>
                </li>
                 <li>
                    <a href="localbody"><i class="fa fa-university fa-lg" aria-hidden="true"></i>Localbody</a>
                </li>
                 <li>
                    <a href="department"><i class="fa fa-building-o fa-lg" aria-hidden="true"></i>Department</a>
                </li>
            </ul>
        </div>
        </div>
         <!-- Page Content -->
