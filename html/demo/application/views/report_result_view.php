<!DOCTYPE html>
<head>
<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">

		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
                 <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/js/script.js"></script>

  
<!--   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 -->  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      




        <title>Super Admin- Dashboard</title>
</head>
<body>
<nav class="navbar navbar-custom" id="navbar">
  <div class="container-fluid">
       <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>

      <a class="navbar-brand" style="color:#ffffff; font-family:serif;"href="#"></a>
    </div>
    <ul class="nav navbar-nav">
  </ul>
  <ul class="nav navbar-nav navbar-right">
  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<div id="wrapper">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#" id="main_li">Dashboard</a>
                </li>
                <li>
                    <a href="admin"><i class="fa fa-home fa-lg" aria-hidden="true"></i>Home</a>
                </li>
                <li>
                    <a href="usermodule"><i class="fa fa-user fa-lg" aria-hidden="true" ></i>User Module</a>
                </li>
                <li>
                    <a href="reports"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"  ></i>Reports</a>
                </li>
                <li>  
                    <a href="analytics"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i>Analytics</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bullhorn fa-lg" aria-hidden="true"></i>FeedBack and Suggestions</a>
                </li>
                <li>
                    <a href="dataupdate"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>Data Updatation</a>
                </li>
                <li>
                    <a href="messages"><i class="fa fa-info fa-lg" aria-hidden="true"></i>
                    Messages</a>
                </li>
                 <li>
                    <a href="districts"><i class="fa fa-info fa-lg" aria-hidden="true"></i>districts</a>
                </li>
                 <li>
                    <a href="category"><i class="fa fa-info fa-lg" aria-hidden="true"></i>categories</a>
                </li>
                 <li>
                    <a href="districts"><i class="fa fa-info fa-lg" aria-hidden="true"></i>districts</a>
                </li>
                 <li>
                    <a href="localbody"><i class="fa fa-info fa-lg" aria-hidden="true"></i>localbody</a>
                </li>
                 <li>
                    <a href="department"><i class="fa fa-info fa-lg" aria-hidden="true"></i>department</a>
                </li>
            </ul>
        </div>

     <!-- Page Content -->
     <div id="page-content-wrapper1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
       <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
<div class="row">
</div>

               </div>
            </div>
        </div>

    </div>
<form class="form-inline" action="<?=base_url();?>index.php/logincontroller/report_date" method="POST">
  <div class="form-group">
    <label for="date">From Date:</label> 
    <input type="text" class="form-control" id="from_date" name="from_date">
  </div>
  <div class="form-group">
    <label for="date">To Date:</label>
    <input type="text" class="form-control" id="to_date" name="to_date">
  </div>
  <div class="form-group">
    <button class="btn btn-danger" type="submit" id="report_generate">Generate Report</button>
    <button class="btn btn-danger" type="button" id="pdf_generate" onclick="location.href='<?php echo base_url();?>index.php/logincontroller/pdf_generate'">Report as PDF</button>
  <br></div>
  
</form>

     <!--  datepicker Script -->
 <!--Dry Code-->


<table class="table table-hover table-bordered">
     <tr><td><strong> Emp Id</strong></td><td><strong>office Name</strong></td><td><strong>address</strong></td><td><strong>type</strong></td><td><strong>contact</strong></td><td><strong>Email</strong></td><td><strong>services</strong></td><td><strong>start</strong></td><td><strong>end</strong></td><td><strong>other details</strong></td></tr> 
    <?php foreach($main_data as $data){?>
     <tr><td><?php echo $data['employee_id'];?></td><td>
     <?php echo $data['office_name'];?></td>
     <td><?php echo $data['address'];?></td>
     <td><?php echo $data['office_type'];?></td>
     <td><?php echo $data['contact'];?></td>
     <td><?php echo $data['email'];?></td>
     <td><?php echo $data['services_offered'];?></td>
     <td><?php echo $data['working_start'];?></td>
     <td><?php echo $data['working_end'];?></td>
     <td><?php echo $data['details'];?></td></tr>     
      <?php } ?>
     
</form> 

<script>
  $(function(){

$("#from_date").datepicker({ dateFormat: 'yy-mm-dd' });
$("#to_date").datepicker({ dateFormat: 'yy-mm-dd' });
    
  });
  function pdf_generate()
  {
    window.location="<?=base_url();?>index.php/logincontroller/pdf_generate";
  }
</script>
</body>
</html>
