<!DOCTYPE html>
<head>
<meta charset='utf-8'>
		<!-- Dependend CSS Files -->
		<link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style2.css">
		<!--Dependend JS Files-->
		<script src="/assets/js/jquery.min.js"></script>
		<script src="/assets/js/popper.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/js/script.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <title>Super Admin- DataUpdate</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-custom" id="navbar" style="width: 2550px;">
  <div class="container-fluid">
   <div class="navbar-header"><img src="/assets/imgs/logo.png" alt="ALT NAME" class="pull-left span2 clearfix" style='margin-right:10px;width:140px;'>
      <a class="navbar-brand" style="color:#ffffff; font-family:serif;" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" style="color:#eee;" href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li>
          <li><a href="logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
		 <!-- Page Content -->

<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
			      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
            <i class="fa fa-bars" aria-hidden="true"></i></a>
   
<table class="table table-hover table-bordered" id="myTable">
     <tr>
   <td style="display:none"><strong>Id</strong></td> 
      <td><strong> Employee Id</strong></td>
      <td><strong>Office Name</strong></td>
      <td><strong>Administrative Department</strong></td>
      <td><strong>Office Type</strong></td>
      <td><strong>Address</strong></td>
      <td><strong>Pincode</strong></td>
      <td><strong>Building Type</strong></td> 
      <td><strong>Landline</strong></td>
      <td><strong>District</strong></td>
      <td><strong>Taluk</strong></td>
      <td><strong>Village</strong></td>
      <td><strong>Block</strong></td>
      <td><strong>Local Body Type</strong></td>
      <td><strong>Local Body Name</strong></td>
      <td><strong>Email</strong></td>
      <td><strong>Landmark</strong></td>
      <td><strong>Contact person</strong></td>
      <td><strong>Contact</strong></td>
      <td><strong>Working Start</strong></td>
      <td><strong>Working End</strong></td>
      <td><strong>Status</strong></td>
      <td><strong>Edit</strong></td>

    </tr> 

<tbody>
<?php if(empty($main_data)){?>
<tr>
  <td colspan="24">
 <div style="margin-left:400px; margin-top:40px;" ><p><h3>No assets</h3></p></div>
  </td>
</tr>
<?php }
else{
 foreach($main_data as $data){ ?>           <tr>
            <td style="display:none"><?php echo $data['id'];?></td>
            <td><?php echo $data['employee_id'];?></td>
            <td><?php echo $data['office_name'];?></td>
            <td><?php echo $data['administrative_department'];?></td>  
            <td><?php echo $data['office_type'];?></td>
            <td><?php echo $data['address'];?></td>
            <td><?php echo $data['pincode'];?></td>
            <td><?php echo $data['building_type'];?></td>
            <td><?php echo $data['landline'];?></td>
            <td><?php echo $data['district'];?></td>
            <td><?php echo $data['taluk'];?></td>
            <td><?php echo $data['village'];?></td>
            <td><?php echo $data['block'];?></td>
            <td><?php echo $data['localbody_type'];?></td>
            <td><?php echo $data['localbody_name'];?></td>
            <td><?php echo $data['email'];?></td>
            <td><?php echo $data['landmark'];?></td> 
            <td><?php echo $data['contact_person'];?></td>
            <td><?php echo $data['contact'];?></td>
            <td><?php echo $data['working_start'];?></td>
            <td><?php echo $data['working_end'];?></td>
            <td><?php echo $data['status'];?></td>     
              
           <td><button type="button"  id="update" class="btn btn-info btn-md" data-toggle="modal"  data-target="#myModal">Edit</button></td>
         </tr>


        <?php }}?> 
 
 </tbody>
           
       </table>
</div>

        <div class="modal fade" id="myModal" style="overflow:scroll" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          
          <h4 class="modal-title">Update</h4>
        </div>
        
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">
                <div class="row">
                  <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
                <input type="text" disabled="true" id="id"></br>
            </div>
                </div>
            <div class="row">
         <label class="col-sm-4 control-label">Employee ID</label>
            <div class="col-sm-8"> 
                <input type="text" disabled="true" id="employee_id"></br>
            </div>
            </div>
            <div class="row">
          <label class="col-sm-4 control-label">Office Name</label>
           <div class="col-sm-8"> 
             <input type="text" id="office_name"></br>
           </div> 
           </div>
           <div class="row">
                  <label class="col-sm-4 control-label">Administrative Department</label>
             <div class="col-sm-8"> 
                <input type="text" id="administrative_department"></br>
             </div>
            </div>
              <div class="row">
          <label class="col-sm-4 control-label">Office type </label> 
            <div class="col-sm-8"> 
              <input type="text" id="office_type"></br>
             </div>  
            </div>
           <div class="row">
          <label class="col-sm-4 control-label">Address</label> 
           <div class="col-sm-8"> 
            <input type="text" id="address"></br>
           </div> 
           </div>
             <div class="row">
          <label class="col-sm-4 control-label">Pincode</label> 
            <div class="col-sm-8"> 
              <input type="text" id="pincode"></br>
            </div>  
            </div>
              <div class="row">
          <label class="col-sm-4 control-label">Building type </label> 
            <div class="col-sm-8"> 
              <input type="text" id="building_type"></br>
            </div>  
            </div>
            <div class="row">
          <label class="col-sm-4 control-label">Landline </label> 
            <div class="col-sm-8"> 
              <input type="text" id="landline"></br>
            </div>  
            </div>
              <div class="row">
          <label class="col-sm-4 control-label">District</label> 
            <div class="col-sm-8"> 
              <input type="text" id="district"></br>
            </div>  
            </div>

             <div class="row">
          <label class="col-sm-4 control-label">Taluk</label> 
            <div class="col-sm-8"> 
              <input type="text" id="taluk"></br>
            </div>  
            </div>
            <div class="row">
          <label class="col-sm-4 control-label">Village</label> 
           <div class="col-sm-8"> 
              <input type="text" id="village"></br>
           </div>
           </div>

            <div class="row">
          <label class="col-sm-4 control-label">Block</label> 
           <div class="col-sm-8"> 
              <input type="text" id="block"></br>
           </div>
           </div>
            <div class="row">
          <label class="col-sm-4 control-label">Local body Type</label> 
           <div class="col-sm-8"> 
              <input type="text" id="localbody_type"></br>
           </div>
           </div>
            <div class="row">
          <label class="col-sm-4 control-label">Local body Name</label> 
           <div class="col-sm-8"> 
              <input type="text" id="localbody_name"></br>
           </div>
           </div>
           <div class="row">
          <label class="col-sm-4 control-label">Email</label>
           <div class="col-sm-8"> 
             <input type="text" id="email"></br>
           </div>
           </div>
            <div class="row">
          <label class="col-sm-4 control-label">Landmark</label> 
           <div class="col-sm-8"> 
              <input type="text" id="landmark"></br>
           </div>
           </div>
            <div class="row">
          <label class="col-sm-4 control-label">Contact Person </label> 
           <div class="col-sm-8"> 
              <input type="text" id="contact_person"></br>
           </div>
           </div>
            <div class="row">
          <label class="col-sm-4 control-label">Contact</label> 
           <div class="col-sm-8"> 
              <input type="text" id="contact"></br>
           </div>
           </div>

           <div class="row">
          <label class="col-sm-4 control-label">Working start</label> 
           <div class="col-sm-8">
             <input type="text" id="working_start"></br>
           </div>
           </div>
           <div class="row">
          <label class="col-sm-4 control-label">Working end</label> 
           <div class="col-sm-8">
            <input type="text" id="working_end"></br>
           </div>  
             </div>

               <div class="row">
          <label class="col-sm-4 control-label">Status</label> 
           <div class="col-sm-8">
            <input type="text"  disabled="true" id="status"></br>
           </div>  
             </div>
            
            


           </div>
              

        </form>
        <script type="text/javascript">

            var x = document.getElementById("myTable").rows.length;

           

            $('table tbody tr  td').on('click',function(){
    $("#myModal").modal("show");

    
    $("#id").val($(this).closest('tr').children()[0].textContent);
    $("#employee_id").val($(this).closest('tr').children()[1].textContent);
    $("#office_name").val($(this).closest('tr').children()[2].textContent);
    $("#administrative_department").val($(this).closest('tr').children()[3].textContent);
    $("#office_type").val($(this).closest('tr').children()[4].textContent);
    $("#address").val($(this).closest('tr').children()[5].textContent);
    $("#pincode").val($(this).closest('tr').children()[6].textContent);
    $("#building_type").val($(this).closest('tr').children()[7].textContent);
    $("#landline").val($(this).closest('tr').children()[8].textContent);
    $("#district").val($(this).closest('tr').children()[9].textContent);
    $("#taluk").val($(this).closest('tr').children()[10].textContent);
    $("#village").val($(this).closest('tr').children()[11].textContent);
    $("#block").val($(this).closest('tr').children()[12].textContent);
    $("#localbody_type").val($(this).closest('tr').children()[13].textContent);
    $("#localbody_name").val($(this).closest('tr').children()[14].textContent);
    $("#email").val($(this).closest('tr').children()[15].textContent);
    $("#landmark").val($(this).closest('tr').children()[16].textContent);
    $("#contact_person").val($(this).closest('tr').children()[17].textContent);
    $("#contact").val($(this).closest('tr').children()[18].textContent);
    $("#working_start").val($(this).closest('tr').children()[19].textContent);
    $("#working_end").val($(this).closest('tr').children()[20].textContent);
    $("#status").val($(this).closest('tr').children()[21].textContent);


    
});


            var employee_id="<?php echo $data['employee_id'];?>";
            document.getElementById('employee_id').value=employee_id;

            var office_name="<?php echo $data['office_name'];?>";
            document.getElementById('office_name').value=office_name;

            var administrative_department="<?php echo $data['employee_id'];?>";
            document.getElementById('administrative_department').value=administrative_department;

            var office_type="<?php echo $data['office_type'];?>";
            document.getElementById('office_type').value=office_type;

             var address="<?php echo $data['address'];?>";
            document.getElementById('address').value=address;

             var pincode="<?php echo $data['pincode'];?>";
            document.getElementById('pincode').value=pincode;

             var building_type="<?php echo $data['building_type'];?>";
            document.getElementById('building_type').value=building_type;

            var landline="<?php echo $data['landline'];?>";
            document.getElementById('landline').value=landline;

            var district="<?php echo $data['district'];?>";
            document.getElementById('district').value=district;

            var taluk="<?php echo $data['taluk'];?>";
            document.getElementById('taluk').value=taluk;

            var village="<?php echo $data['village'];?>";
            document.getElementById('village').value=village;

            var block="<?php echo $data['block'];?>";
            document.getElementById('block').value=block;

            var localbody_type="<?php echo $data['localbody_type'];?>";
            document.getElementById('localbody_type').value=localbody_type;

            var localbody_name="<?php echo $data['localbody_name'];?>";
            document.getElementById('localbody_name').value=localbody_name;

             var email="<?php echo $data['email'];?>";
            document.getElementById('email').value=email;

            var landmark="<?php echo $data['landmark'];?>";
            document.getElementById('landmark').value=landmark;

             var contact_person="<?php echo $data['contact_person'];?>";
            document.getElementById('contact_person').value=contact_person;

            var contact="<?php echo $data['contact'];?>";
            document.getElementById('contact').value=contact;

             var working_start="<?php echo $data['working_start'];?>";
            document.getElementById('working_start').value=working_start;

             var working_end="<?php echo $data['working_end'];?>";
            document.getElementById('working_end').value=working_end;

            var status="<?php echo $data['status'];?>";
            document.getElementById('status').value=status;
            

        </script>
        <div class="modal-footer">.
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
             <?php if($data['status']=="pending"){ ?>
            <button type="button" class="btn btn-warning" data-dismiss="modal" id="approveButton">Approve</button>
              <?php } ?>
            <a href="dataupdate" data-dismiss="modal" class="btn btn-success" id="updateButton">Update</a>

        <!--<button data-dismiss="modal"  id="updateButton" type="button" class="btn btn-success" >Update</button>  -->
        <button  data-dismiss="modal" type="button" id="deleteButton" class="btn btn-danger" >Delete</button>
         

<script type="text/javascript">



     $('#approveButton').click(function() 

            {
                 var id = $("#id").val();

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/approve_asset'); ?>",
                             data: { 'id':id},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                         location.reload()
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });


//

$('#deleteButton').click(function() 

            {
                 var id = $("#id").val();

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/delete_asset'); ?>",
                             data: { 'id':id},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                        location.reload()
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });

//


$('#updateButton').click(function() 
      {
                var id = $("#id").val();
                var employee_id = $("#employee_id").val();
                var office_name = $("#office_name").val();
                var administrative_department = $("#administrative_department").val();
                var office_type = $("#office_type").val();
                var address = $("#address").val();
                var pincode = $("#pincode").val();
                var building_type = $("#building_type").val();
                var landline = $("#landline").val();
                var district = $("#district").val();
                var taluk = $("#taluk").val();
                var village = $("#village").val();
                var block = $("#block").val();
                var localbody_type = $("#localbody_type").val();
                var localbody_name = $("#localbody_name").val();
                var email=$("#email").val();
                var landmark=$("#landmark").val();
                var contact_person=$("#contact_person").val();
                var contact=$("#contact").val();
                var working_start=$("#working_start").val();
                var working_end=$("#working_end").val();
                var status=$("#status").val();
                
            $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('index.php/logincontroller/modal_asset'); ?>",
                          data: { 
                                  'id':id,
                                  'employee_id':employee_id,
                                  'office_name':office_name,
                                  'administrative_department':administrative_department, 
                                  'office_type':office_type,
                                  'address':address,
                                  'pincode':pincode,
                                  'building_type':building_type,
                                  'landline':landline,
                                  'district':district,
                                  'taluk':taluk,
                                  'village':village,
                                  'block':block,
                                  'localbody_type':localbody_type,
                                  'localbody_name':localbody_name,
                                  'email':email,
                                  'landmark':landmark,
                                  'contact_person':contact_person,
                                  'contact':contact,
                                  'working_start':working_start,
                                  'working_end':working_end,
                                  'status':status,
                                   
                                },
                     success: function(response)
                       {
                         console.log("success");
                         location.reload()
                          },
    error: function(err){
        console.log('error');
      
    },
  });
});


</script>


        </div>
      </div>
    </div>
  </div>
  
</div>


          </div>
        </div>
    </div>
</div>
 </div>
 </div>
<!--user search results list ends-->
    <!-- /#wrapper -->
     <!-- Menu Toggle Script -->
    <script>


   function idpass(el)
 {

          var idval=el.id;
         
      console.log(idval);
    
  $.ajax({
            type:'post',
            url:"<?=base_url()?>index.php/logincontroller/modal_asset",
            data:{ id :"idval"},
            success: function () {   // success callback function
        console.log("success");
                },
            error: function () { // error callback 
            console.log("fail");
            }
  
      });

     

}

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
