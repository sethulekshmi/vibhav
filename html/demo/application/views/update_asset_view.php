<!DOCTYPE html>
<head>
<meta charset='utf-8'>
        <!-- Dependend CSS Files -->
        <link rel="stylesheet" type="text/css" href="/assets/css/adminstyle.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontawsome.min.css">
                  <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <!--Dependend JS Files-->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/popper.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/script.js"></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <title>Super Admin- DataUpdate</title>
</head>
<body>
<!-- nav bar -->
<nav class="navbar navbar-inverse" id="navbar">
  <div class="container-fluid">
   <div class="navbar-header">
      <a class="navbar-brand" style="color:#eee;" href="#">Vibhva-SuperAdmin</a>
    </div>
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" style="color:#eee;" href="#">Admin
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="change"><i class="fa fa-key" aria-hidden="true"></i>Change Password</a></li> 
          <li><a href="logincontroller/logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<!--Sidebar-->
<div id="wrapper">
        <div id="sidebar-wrapper" style="top:50px;">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#" id="main_li">Dashboard</a>
                </li>
                <li>
                    <a href="admin"><i class="fa fa-home fa-lg" aria-hidden="true"></i>Home</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-user fa-lg" aria-hidden="true" class="dcjq-parent active"></i> <span>User Module</span></a>
                     
                </li>
                <li>
                    <a href="reports"><i class="fa fa-file-excel-o fa-lg" aria-hidden="true"  ></i>Reports</a>
                </li>
                <li>    
                    <a href="analytics"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i>Analytics</a>
                </li>
                <li>
                    <a href="feedbackandsuggestions"><i class="fa fa-bullhorn fa-lg" aria-hidden="true"></i>FeedBack and Suggestions</a>
                </li>
                <li>
                    <a href="dataupdate"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>Data Updatation</a>
                </li>
                <li>
                    <a href="messages"><i class="fa fa-info fa-lg" aria-hidden="true"></i>Messages</a>
                </li>
            </ul>
        </div>

         <!-- Page Content -->

<div id="page-content-wrapper1">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
                  <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
         
<table class="table table-hover table-bordered" id="myTable">
     <tr><td><strong>Id</strong></td> <td><strong> Emp Id</strong></td><td><strong>office Name</strong></td><td><strong>address</strong></td><td><strong>type</strong></td><td><strong>contact</strong></td><td><strong>Email</strong></td><td><strong>services</strong></td><td><strong>start</strong></td><td><strong>end</strong></td><td><strong>other details</strong></td></tr> 

<tbody>
<?php foreach($main_data as $data){?>



            <tr>
            <td><?php echo $data['id'];?></td>
            <td><?php echo $data['employee_id'];?></td>
            <td><?php echo $data['office_name'];?></a></td>
            <td><?php echo $data['address'];?></td>
            <td><?php echo $data['office_type'];?></td>
            <td><?php echo $data['contact'];?></td>
            <td><?php echo $data['email'];?></td>
            <td><?php echo $data['services_offered'];?></td>
            <td><?php echo $data['working_start'];?></td>
            <td><?php echo $data['working_end'];?></td>
            <td><?php echo $data['details'];?></td>

            
           <td><button type="button" id="update" class="btn btn-info btn-md" data-toggle="modal"  data-target="#myModal">Update</button></td>
         </tr></tbody>
        <?php }?> 

           
       </table>


        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          
          <h4 class="modal-title">Update</h4>
        </div>
        
        <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">
            <label class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8"> 
                <input type="text" disabled="true" id="id"></br>
            </div>
         <label class="col-sm-4 control-label">Employee ID</label>
            <div class="col-sm-8"> 
                <input type="text" disabled="true" id="employee_id"></br>
            </div>
          <label class="col-sm-4 control-label">Office Name</label>
           <div class="col-sm-8"> 
             <input type="text" id="office_name"></br>
           </div> 
          <label class="col-sm-4 control-label">Address</label> 
           <div class="col-sm-8"> 
            <input type="text" id="address"></br>
           </div> 
          <label class="col-sm-4 control-label">Office type </label> 
            <div class="col-sm-8"> 
              <input type="text" id="office_type"></br>
            </div>  
          <label class="col-sm-4 control-label">Contact</label> 
           <div class="col-sm-8"> 
              <input type="text" id="contact"></br>
           </div>
          <label class="col-sm-4 control-label">Email</label>
           <div class="col-sm-8"> 
             <input type="text" id="email"></br>
           </div>
          <label class="col-sm-4 control-label">Services offered</label> 
           <div class="col-sm-8">
            <input type="text" id="services_offered"></br>
           </div>
          <label class="col-sm-4 control-label">Working start</label> 
           <div class="col-sm-8">
             <input type="text" id="working_start"></br>
           </div>
          <label class="col-sm-4 control-label">Working end</label> 
           <div class="col-sm-8">
            <input type="text" id="working_end"></br>
           </div>  
             
           <label class="col-sm-4 control-label">Details</label> 
                <div class="col-sm-8">
                <input type="text" id="details">
               </div>


           </div>
              

        </form>
        <script type="text/javascript">

            var x = document.getElementById("myTable").rows.length;

            console.log(x)

            $('table tbody tr  td').on('click',function(){
    $("#myModal").modal("show");

    
    $("#id").val($(this).closest('tr').children()[0].textContent);
    $("#employee_id").val($(this).closest('tr').children()[1].textContent);
    $("#office_name").val($(this).closest('tr').children()[2].textContent);
    $("#address").val($(this).closest('tr').children()[3].textContent);
    $("#office_type").val($(this).closest('tr').children()[4].textContent);
    $("#contact").val($(this).closest('tr').children()[5].textContent);
    $("#email").val($(this).closest('tr').children()[6].textContent);
    $("#services_offered").val($(this).closest('tr').children()[7].textContent);
    $("#working_start").val($(this).closest('tr').children()[8].textContent);
    $("#working_end").val($(this).closest('tr').children()[9].textContent);
    $("#details").val($(this).closest('tr').children()[10].textContent);
    
});


            var employee_id="<?php echo $data['employee_id'];?>";
            document.getElementById('employee_id').value=employee_id;
            var office_name="<?php echo $data['office_name'];?>";
            document.getElementById('office_name').value=office_name;
             var address="<?php echo $data['address'];?>";
            document.getElementById('address').value=address;
             var office_type="<?php echo $data['office_type'];?>";
            document.getElementById('office_type').value=office_type;
             var contact="<?php echo $data['contact'];?>";
            document.getElementById('contact').value=contact;
             var email="<?php echo $data['email'];?>";
            document.getElementById('email').value=email;
             var services_offered="<?php echo $data['services_offered'];?>";
            document.getElementById('services_offered').value=services_offered;
             var working_start="<?php echo $data['working_start'];?>";
            document.getElementById('working_start').value=working_start;
             var working_end="<?php echo $data['working_end'];?>";
            document.getElementById('working_end').value=working_end;
             var details="<?php echo $data['details'];?>";
            document.getElementById('details').value=details;
        </script>
        <div class="modal-footer">.
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button  href="#" id="updateButton" type="button" class="btn btn-success" >Update</button>
        <button type="button" id="deleteButton" class="btn btn-danger" >Delete</button>
         

<script type="text/javascript">

$('#deleteButton').click(function() 

            {
                 var id = $("#id").val();

                 $.ajax({
                             type: "POST",
                             url: "<?php echo base_url('index.php/logincontroller/delete_asset'); ?>",
                             data: { 'id':id},
                              
                             success: function(response)
                                     {
                                        console.log("success");
                                      },
                              error: function(err)
                                      {
                                         console.log('error');
                                         console.log(err);
                                       },



                        });
            });




$('#updateButton').click(function() 
      {
                var id = $("#id").val();
                var employee_id = $("#employee_id").val();
                var office_name = $("#office_name").val(); 
                var address = $("#address").val();
                var office_type = $("#office_type").val();
                 var contact=$("#contact").val();
                 var email=$("#email").val();
                 var services_offered=$("#services_offered").val();
                 var working_start=$("#working_start").val();
                 var working_end=$("#working_end").val();
                 var details=$("#details").val();
            $.ajax({
                          type: "POST",
                          url: "<?php echo base_url('index.php/logincontroller/modal_asset'); ?>",
                          data: { 
                                  'id':id,
                                  'employee_id':employee_id,
                                  'office_name':office_name, 
                                   'address':address,
                                 'office_type':office_type,
                                  'contact':contact,
                                  'email':email,
                                  'services_offered':services_offered,
                                   'working_start':working_start,
                                   'working_end':working_end,
                                   'details':details
                                },
                     success: function(response)
                       {
                         console.log("success");
                          },
    error: function(err){
        console.log('error');
      console.log(err);
    },
  });
});


</script>


        </div>
      </div>
    </div>
  </div>
  
</div>


          </div>
        </div>
    </div>
</div>
 </div>
<!--user search results list ends-->
    <!-- /#wrapper -->
     <!-- Menu Toggle Script -->
    <script>


   function idpass(el)
 {

          var idval=el.id;
         
      console.log(idval);
    
  $.ajax({
            type:'post',
            url:"<?=base_url()?>index.php/logincontroller/modal_asset",
            data:{ id :"idval"},
            success: function () {   // success callback function
        console.log("success");
                },
            error: function () { // error callback 
            console.log("fail");
            }
  
      });

     

}

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>
