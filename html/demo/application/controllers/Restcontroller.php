<?php
//Title:RestController: Single controller for getting the data from other devices.
//Author:Jithin Zacharia
//Date:4/10/2017
 header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');

class Restcontroller extends REST_Controller{
	public function user_post(){
		//API call for register the user
		$data=array('employee_id'=>$this->post('employee_id'),
		'username'=>$this->post('username'),
		'fullname'=>$this->post('fullname'),
		'email'=>$this->post('email'),
		'mobile '=>$this->post('mobile'),
		'password'=>$this->post('password'),
		'employee_department'=>$this->post('employee_department'),
		'employee_designation'=>$this->post('employee_designation'),
		'centre_id '=>$this->post('centre_id'),
		'centre_name'=>$this->post('centre_name'),
		'status'=>$this->post('status')
	);
		$this->load->model('api_register_model');

		$result=$this->api_register_model->user_register($data);
		log_message('data office name',$data['name']);
		 if($result === false){
            $this->response(array('status' => 'failed'));
        }else{
            $this->response(array('status' => 'success'));
        }
}


//user get function
	public function user_get(){
		 if(!$this->get('id')){
            $this->response("ID supplied not correct", 400);
        }
        $user = $this->api_login_model->get( $this->get('id') );
         
        if($user){
            $this->response($user, 200); // 200 being the HTTP response code
        }
        else{
            $this->response(NULL, 404);
        }
	}

//function for posting login
	public function login_post(){
	$username=$this->post('username');
	$password=$this->post('password');
	$this->load->model('api_login_model');
	$data=$this->api_login_model->login($username,$password);
			log_message('error',$data);
	if($data){
		$this->response($data,200);
	}else{
		$this->response(array('status' => 'failed'));
	}

	}
    	
//api call for giving the categories for mobile
	public function categories_get(){
		$this->load->model('api_selection_results');
		$result=$this->api_selection_results();
		if(count($result)>0){
			return $result
			$this->response("success",200);
		}else{
			$this->response("failed",404);
		}
	}

//test function for testing api
	public function userhello_get(){
		$data="jithin";
		$count=1;
		$tr=json_encode($data);
		if($count==1){
			$this->response($tr,200);
		}else{
			$this->response('Try again',400);
		}
	}

//function for sending the search results to mobile app
	public function search_post()
	{
		
		$office_name=$this->post('searchText');
		$office_type=$this->post('searchText');
		$this->load->model('api_search_results');
		$data=$this->api_search_results->search($office_name,$office_type);
		$final_data="[";
		if(count($data)>0)
		{
			foreach($data as $v)
			{
			$office_name=$v->office_name;
			$office_type=$v->office_type;
			$latitude=$v->latitude;
			$longitude=$v->longitude;
			$photo=$v->photo;
			$address=$v->address;
			$email=$v->email;
			$final_data .="{\"office_name\":\"".$office_name."\",\"office_type\":\"".$office_type."\",\"latitude\":\"".$latitude."\",\"longitude\":\"".$longitude."\",\"photo\":\"".$photo."\",\"address\":\"".$address."\",\"email\":\"".$email."\"},";
     		         
			}

			//trim 

			$final_data = rtrim($final_data, ',');
	    }
$final_data .="]";
		header('Content-Type: application/json');
		//$this->response($final_data,200);
		echo $final_data;
		//	echo $v->office_name;
	}
public function searchall_get()
	{
		
		$this->load->model('api_search_results');
		$data=$this->api_search_results->searchall();
		$final_data="[";
		if(count($data)>0)
		{
			foreach($data as $v)
			{
			$office_name=$v->office_name;
			$office_type=$v->office_type;
			$latitude=$v->latitude;
			$longitude=$v->longitude;
			$photo=$v->photo;
			$address=$v->address;
			$email=$v->email;
			$final_data .="{\"office_name\":\"".$office_name."\",\"office_type\":\"".$office_type."\",\"latitude\":\"".$latitude."\",\"longitude\":\"".$longitude."\",\"photo\":\"".$photo."\",\"address\":\"".$address."\",\"email\":\"".$email."\"},";
     		         
			}

			//trim 

			$final_data = rtrim($final_data, ',');
	    }
$final_data .="]";
		header('Content-Type: application/json');

//$this->response($final_data,200);


echo $final_data;

		//	echo $v->office_name;
		
	}
//function for posting the assets into db
	public function assets_post(){
		//API call for posting assets
		$data=array('employee_id'=>$this->input->post('employee_id'),
        'office_name' =>$this->input->post('office_name'),
        'administrative_department'=>$this->input->post('administrative_department'),
        'office_type'=>$this->input->post('office_type'),
        'address'=>$this->input->post('address'),
         'pincode'=>$this->input->post('pincode'),
         'building_type'=>$this->input->post('building_type'),
         'landline'=>$this->input->post('landline'),
         'district'=>$this->input->post('district'),
         'taluk'=>$this->input->post('taluk'),
         'village'=>$this->input->post('village'),
         'block'=>$this->input->post('block'),
         'localbody_type'=>$this->input->post('localbody_type'),
         'localbody_name'=>$this->input->post('localbody_name'),
         'email'=>$this->input->post('email'),
         'landmark'=>$this->input->post('landmark'),
         'contact_person'=>$this->input->post('contact_person'),
         'contact'=>$this->input->post('contact'),
         'working_start' =>$this->input->post('working_start'),
         'working_end' =>$this->input->post('working_end'),
         'surveyor_name'=>$this->input->post('surveyor_name')
	);
		$this->load->model('asset_model');
		$result=$this->asset_model->assets($data);
	log_message('data office name',$data['office_name']);
		 if($result=='true'){
		
            $this->response(array('status' => 'success'));
        }else{
            $this->response(array('status' => 'failed'));
        }
}


	// api for messages
	public function messages_post()
	{
		$initdata=array('employee_id'=>$this->input->post('employee_id'),
        'employee_messages'=>$this->input->post('employee_messages'));
		
		$this->load->model("api_messages_model");
		$data=$this->api_messages_model->messagespost($initdata);

		if($data){
			$this->response("success",200);
		}
			else{
			$this->response(NULL,404);
		}
	}
}

?>
