<?php 
//FileName:Login Controller-Controller for login-UserManager 
//Date:18/09/2017
//Author:Jithin Zacharia
class Logincontroller extends CI_Controller{
    function construct(){
        parent::__construct();
        $this->load->helper('form','url');
        $this->load->database();
        $this->load->model('login_model');
        $this->load->library('session');


    }
    public function index(){
        $this->load->model('login_model');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('form','url');
        $username=$this->input->post('username');
        $password=$this->input->post('pwd');
        $form_data=$this->input->post();
        
        $query = $this->db->get("user_name");
        $data['records'] = $query->result();

        $result= $this->login_model->login($username,$password);

        if($result){
            $session_array=array();
            foreach($result as $row){
                $session_array=array(
                    'id'=>$row->id,
                    'username'=>$row->username,
                    'employee_type'=>$row->employee_type,
                );
                $this->session->set_userdata('logged_in',$session_array);
            }
            if($result){
            $this->load->model('home_model');
            $data['asset_type']=$this->home_model->get_asset_type();
            $this->load->model('map_model');
            $data['map_data']=$this->map_model->get_locations();
            $this->load->view('adminpanelsidebar');
            $this->load->view('adminpanel',$data);
          }
            }
        
        else{
             $this->session->set_flashdata('error','Wrong email or password .');
               redirect(base_url().'');
            }
    }
     public function login_comparison(){
          $username = $this->input->post('username');
          $password = $this->input->post('password');
          $this->load->model('login_model');

          $result = $this->login_model->compare_login($username,$password); 
            if(empty($result)){
              echo "<script type='text/javascript'>alert('Invalid login');</script>";
              $this->load->view('sidebar');
              $this->load->view('login');
                  }
        }
        public function change(){
          $email=$this->input->post('email');
          $old_password = $this->input->post('old_password');
          $new_password=$this->input->post('new_password');
          $this->load->model('model_users');
          $result = $this->model_users->changepwd($email,$new_password);
          echo "<script type='text/javascript'>alert('Password Changed successfully');</script>";
          $this->load->view('adminpanelsidebar');
          $this->load->view('reset');
        }
    public function test(){
        $this->load->view('sidebar');
             $this->load->view('login');
    }
    public function register(){
      $this->load->view('sidebar');
        $this->load->view('register');
    }
    public function logout(){
        $this->load->model('login_model');
        $this->load->library('session');
        $this->load->helper('form','url');
        $this->session->sess_destroy();
        $this->load->view('sidebar');
        $this->load->view('login');
    }
    

public function usermodule(){
  if(isset($this->session->userdata['logged_in']))
     {
         $this->load->model('user_model');
        $data['main_data']=$this->user_model->get_userdata();
         $this->load->view('adminpanelsidebar');
        $this->load->view('usermodule',$data);;

       }
    else{

               $this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function deleteuser(){
      if(isset($this->session->userdata['logged_in']))
     {

        $id=$this->input->post('id');
        $this->load->model('user_model');
        $this->user_model->delete_user($id);
        $query = $this->db->get("surveydata");
        $data['main_data'] = $query->result();
        echo "<script type='text/javascript'>alert('User deleted successfully');</script>";
        $this->load->view('adminpanelsidebar');
        $this->load->view('usermodule',$data);
      }
     else{
        $this->load->view('sidebar');
          $this->load->view('register');
        }

    }
    public function reports()
   {


    if(isset($this->session->userdata['logged_in']))
     {
        $this->load->view('adminpanelsidebar');
        $this->load->view('reports');
      }
      else
      {
          $this->load->view('sidebar');
          $this->load->view('register');
        }

   }

public function report_date(){

  if(isset($this->session->userdata['logged_in']))
     {
       $this->load->helper('pdf_helper');
       $from_date=$this->input->post('from_date');
       $to_date=$this->input->post('to_date');
       $this->load->model('reportmodel');
     if(  $data['main_data']=$this->reportmodel->get_data($from_date,$to_date))
         {
           $this->load->helper('pdf_helper');
           $this->load->view('pdf_report',$data);
         }

         else
         {
             //$this->load->view('adminpanelsidebar');
           // $this->load->view('reports');
            $this->session->set_flashdata('entry','No entries ');
            redirect(base_url().'index.php/logincontroller/reports');
         }                     
     }
     else
      {
          $this->load->view('sidebar');
          $this->load->view('register');

        }
}

public function pdf_generate()
    {
       if(isset($this->session->userdata['logged_in']))
     {
        $this->load->helper('pdf_helper');
        $this->load->view('pdf_report',$data);
      }

       else
      {
          $this->load->view('sidebar');
          $this->load->view('register');
        }

    }
    public function analytics(){
      if(isset($this->session->userdata['logged_in']))
       {
        $this->load->model('analytics_model'); 
        $data['main_data']= $this->analytics_model->getcount(); 
        $this->load->view('adminpanelsidebar');
        $this->load->view('analytics',$data);
       }

       else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
  
    }
    public function messages(){
       if(isset($this->session->userdata['logged_in']))
       {

        $this->load->model('messages_model');
        $data['main_data']=$this->messages_model->getmessages();
        $this->load->view('adminpanelsidebar');
        $this->load->view('messages',$data);
      }
      else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        }
    }
    public function deletemessage(){
         if(isset($this->session->userdata['logged_in']))
       {
        $id = $this->input->post('id');
        $this->load->model('messages_model');
        $this->messages_model->delete_message($id);
        $query = $this->db->get("user_message");
        $data['records'] = $query->result();
        $this->load->view('adminpanelsidebar'); 
        $this->load->view('messages',$data);
      }
     else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
    }  
    public function admin(){
       if(isset($this->session->userdata['logged_in']))
       {
        $this->load->model('home_model');
        $data['asset_type']=$this->home_model->get_asset_type();
        $this->load->model('map_model');
        $data['map_data']=$this->map_model->get_locations();
         $this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$data);
      }
       else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
    }
public function getassettype(){
  if(isset($this->session->userdata['logged_in']))
       {
        $this->load->model('home_model');
        $data['asset_type']=$this->home_model->get_asset_type();
        echo json_encode($data);
        $this->load->view('adminpanelsidebar');
        $this->load->view('adminpanel',$data);
      }
      else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
}
 public function searchdb(){
  if(isset($this->session->userdata['logged_in']))
       {
        $id = $this->input->post('id');   
        $this->load->model('home_model');
        $data['map_data']=$this->home_model->search_db($id); 
            $this->load->model('home_model');
            $data['asset_type']=$this->home_model->get_asset_type();
            $this->load->view('adminpanelsidebar');
            $this->load->view('adminpanel',$data);
       }
      else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
            
       
    }
    public function searchdropdown(){
      if(isset($this->session->userdata['logged_in']))
       {
        $id = $this->input->post('id');   
        $this->load->model('home_model');
        $data['map_data']=$this->home_model->search_dropdown($id); 
            $this->load->model('home_model');
            $data['asset_type']=$this->home_model->get_asset_type();
            $this->load->view('adminpanelsidebar');
            $this->load->view('adminpanel',$data);
         }
      else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        }    
       
    }
public function pass_id(){
        if(isset($this->session->userdata['logged_in']))
       {
          // $data = array();
          $id = $this->input->post('id');
          // echo $id;
          $this->load->model('user_model');
          $result = $this->user_model->get_profile($id);
         
            $data['profile_data'] = $result;
          
          // echo json_encode($data);
            $this->load->view('adminpanelsidebar');
          $this->load->view('profile',$data);
          
          }
      else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        } 
    }
    public function approve()
         {
          if(isset($this->session->userdata['logged_in']))
        {
          $id = $this->input->post('id');
          $this->load->model('user_model');
          $data['approve_data']=$this->user_model->approve_userdata($id);
          $this->load->model('user_model');
           $data['main_data']=$this->user_model->get_userdata();
           $this->load->view('adminpanelsidebar');
           $this->load->view('usermodule',$data);
         }
      else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        } 

      }

    public function block(){

       if(isset($this->session->userdata['logged_in']))
        {
          
          $id = $this->input->post('id');
        $this->load->model('user_model');
        $data['approve_data']=$this->user_model->block_user($id);
        $this->load->model('user_model');
        $data['main_data']=$this->user_model->get_userdata();
        $this->load->view('adminpanelsidebar');
        $this->load->view('usermodule',$data);
        }
    else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        }
    }

    public function register_comparison(){

        $id = $this->input->post('id');
        $this->load->model('register_model');

        $result = $this->register_model->compare_db($id); 
      
        }

    public function username_comparison(){
       if(isset($this->session->userdata['logged_in']))
        {
        $id = $this->input->post('id');
        $this->load->model('register_model');
        $result = $this->register_model->compare_username($id); 
        if($result){
            echo "<script type='text/javascript'>alert('This username is already registered');</script>";
            $this->load->view('sidebar');
                $this->load->view('register');
                  }

          }
       else
        {
            $this->load->view('sidebar');
          $this->load->view('register');
        }
             
    }  

      public function dataupdate()
    {

       if(isset($this->session->userdata['logged_in']))
        {
       $this->load->model('assetupdate_model');
       $data['main_data']=$this->assetupdate_model->get_data();
       $this->load->view('adminpanelsidebar');
       $this->load->view('dataupdate',$data);

         }
       else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        }

    }

      public function delete_asset()

      {
          if(isset($this->session->userdata['logged_in']))
         {
           $id=$this->input->post('id');
            $this->load->model('assetupdate_model');
          $result= $this->assetupdate_model->deletion_asset($id);
          if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('dataupdate',$result);
                    }
             else{
                    return false;
                   }

           }
       else
        {
          $this->load->view('sidebar');
          $this->load->view('register');
        }

      }
         




    public function approve_asset()
      {
        if(isset($this->session->userdata['logged_in']))
         {
           $id=$this->input->post('id');

           $this->load->model('assetupdate_model');
  
           $result = $this->assetupdate_model->approve_profile($id);
             if($result)
                {
                  return true;
                  $this->load->view('adminpanelsidebar');
                  $this->load->view('dataupdate',$result);
                }
             else
                {
                    return false;
                }
           }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }

      }


   public function modal_asset()
    {  

     if(isset($this->session->userdata['logged_in']))
      {
        $id=$this->input->post('id');
        $data=array('employee_id'=>$this->input->post('employee_id'),
        'office_name' =>$this->input->post('office_name'),
        'administrative_department'=>$this->input->post('administrative_department'),
        'office_type'=>$this->input->post('office_type'),
        'address'=>$this->input->post('address'),
         'pincode'=>$this->input->post('pincode'),
         'building_type'=>$this->input->post('building_type'),
         'landline'=>$this->input->post('landline'),
         'district'=>$this->input->post('district'),
         'taluk'=>$this->input->post('taluk'),
         'village'=>$this->input->post('village'),
         'block'=>$this->input->post('block'),
         'localbody_type'=>$this->input->post('localbody_type'),
         'localbody_name'=>$this->input->post('localbody_name'),
         'email'=>$this->input->post('email'),
         'landmark'=>$this->input->post('landmark'),
         'contact_person'=>$this->input->post('contact_person'),
         'contact'=>$this->input->post('contact'),
         'working_start' =>$this->input->post('working_start'),
         'working_end' =>$this->input->post('working_end'),
         'status' =>$this->input->post('status'),
         
        );
            log_message('data office name',$id);
           $this->load->model('assetupdate_model');

          $result = $this->assetupdate_model->get_profile($id,$data);
         if($result)
             {
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('dataupdate',$result);
             }
             else
               {
            return false;
                 }

          }
         else
         {
            $this->load->view('sidebar');
          $this->load->view('register');
         }


    }

     

    public function feedbackandsuggestions(){
     if(isset($this->session->userdata['logged_in']))
      {
        $this->load->view('adminpanelsidebar');
        $this->load->view('feedbackandsuggestions');
       }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }


    }


public function department()

    {
      if(isset($this->session->userdata['logged_in']))
      {
        $this->load->model('department_model');
       $data['department_data']=$this->department_model->get_department();
       $this->load->view('adminpanelsidebar');
       $this->load->view('department',$data);
       }
        else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }


    }

public function category()
    {
       if(isset($this->session->userdata['logged_in']))
      {
       $this->load->model('category_model');
       $data['category_data']=$this->category_model->get_category();
       $this->load->view('adminpanelsidebar');
       $this->load->view('category',$data);
       }

       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
    }
//loading localbody


public function localbody()
    {

       if(isset($this->session->userdata['logged_in']))
        {
      
         $this->load->model('localbody_model');
       $data['localbody_data']=$this->localbody_model->get_localbody();
       $this->load->view('adminpanelsidebar');
       $this->load->view('localbody',$data);
        }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
    }


public function districts()
    {
        if(isset($this->session->userdata['logged_in']))
        {
      
        $this->load->model('district_model');
       $data['district_data']=$this->district_model->get_district();
       $this->load->view('adminpanelsidebar');
       $this->load->view('districts',$data);
        }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
    }

  // for add districts


    public function add_district()
     {
        if(isset($this->session->userdata['logged_in']))
        {
      
          $data=array('district_name'=>$this->input->post('district_name'));
   
         $this->load->model('district_model');

          $result = $this->district_model->add_newdistrict($data);
         
          return;
          $this->load->view('adminpanelsidebar');
          $this->load->view('districts',$result);

         }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
     }
     //delete district

     public function delete_district()

      {
         if(isset($this->session->userdata['logged_in']))
        {
           $id=$this->input->post('id');
            $this->load->model('district_model');
          $result= $this->district_model->deletion_district($id);
          if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('districts',$result);
                  }
                   else{
                   return false;
                       }

        }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }


      }


     
    public function update_district()
    {
      if(isset($this->session->userdata['logged_in']))
      {
        $id=$this->input->post('id');
        $data=array('district_name'=>$this->input->post('district_name'),);
            
           $this->load->model('district_model');

          $result = $this->district_model->update_district($id,$data);
         if($result){
            return false;
            $this->load->view('adminpanelsidebar');
            $this->load->view('districts',$result);
            // redirect($this->uri->uri_string());
                    }
            else{
                  return true;
              }
        }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
      }
 // for add category


     public function add_category()
     {
       if(isset($this->session->userdata['logged_in']))
      {
          $data=array('institution_category'=>$this->input->post('institution_category'));
   
         $this->load->model('category_model');

          $result = $this->category_model->add_newcategory($data);
          
            if($result){
                        return true;
                        $this->load->view('adminpanelsidebar');
                     $this->load->view('category');
                  }
                  else{
                    return false;
                   }
        }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }

     }


          //delete district

     public function delete_category()
      {
           if(isset($this->session->userdata['logged_in']))
        {
           $id=$this->input->post('id');
            $this->load->model('category_model');
          $result= $this->category_model->deletion_category($id);
          if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('category',$result);
            }
            else{
                 return false;
                }
          }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
      }


     
    public function update_category()
    {
       if(isset($this->session->userdata['logged_in']))
        {
         $id=$this->input->post('id');
                 $data=array('institution_category'=>$this->input->post('institution_category'),);
           $this->load->model('category_model');
         

          $result = $this->category_model->update_categorytype($id,$data);
         if($result){
                      $this->load->view('adminpanelsidebar');
                     $this->load->view('category',$result);
                    }
            else{
                  return false;
               }
          }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
      }

// for local body

      public function add_localbody()
     {

        
         if(isset($this->session->userdata['logged_in']))
        {

           $data=array('local_body_type'=>$this->input->post('local_body_type'));
   
           $this->load->model('localbody_model');

           $result = $this->localbody_model->add_newlocalbody($data);
          
            if($result){
                        return true;
                        $this->load->view('adminpanelsidebar');
                     $this->load->view('localbody');
                  }
                  else{
                    return false;
                   }

           }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
     }



     public function update_localbody()
    {

      if(isset($this->session->userdata['logged_in']))
       {
         $id=$this->input->post('id');
         $data=array('local_body_type'=>$this->input->post('local_body_type'));
           $this->load->model('localbody_model');
           //echo "Hello";
           //echo json_encode($data);
          $result = $this->localbody_model->update_localbodytype($id,$data);
          if($result){
            $this->load->view('adminpanelsidebar');
                     $this->load->view('localbody',$result);
                    }
            else{
                  return false;
               }
          }
       else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }
      }


    public function delete_local()
      {
         if(isset($this->session->userdata['logged_in']))
       {
           $id=$this->input->post('id');
            $this->load->model('localbody_model');
          $result= $this->localbody_model->deletion_localbody($id);
          if($result){
                       return true;
                       $this->load->view('adminpanelsidebar');
                       $this->load->view('localbody',$result);
                     }
               else{
                     return false;
                   }
          }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }  
      }

        public function add_department()
     {

        
      if(isset($this->session->userdata['logged_in']))
       {

           $data=array('department_name'=>$this->input->post('department_name'));
   
           $this->load->model('department_model');

           $result = $this->department_model->add_newdepartment($data);
         
           if($result){
                        return true;
                        $this->load->view('adminpanelsidebar');
                     $this->load->view('department');
                  }
                  else{
                    return false;
                   }
          }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }  

     }


      public function delete_department()
      {

         if(isset($this->session->userdata['logged_in']))
        {
           $id=$this->input->post('id');
            $this->load->model('department_model');
          $result= $this->department_model->deletion_department($id);
          if($result){
            return true;
            $this->load->view('adminpanelsidebar');
            $this->load->view('department',$result);
                }else{
            return false;
             }
         }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         }  


      }


    public function update_department()
    {
      if(isset($this->session->userdata['logged_in']))
        {
          
           $id=$this->input->post('id');
                 $data=array('department_name'=>$this->input->post('department_name'),);
           $this->load->model('department_model');
         

          $result = $this->department_model->update_departmenttype($id,$data);
         if($result){
          $this->load->view('adminpanelsidebar');
                     $this->load->view('department',$result);
                    }
            else{
                  return false;
               }
          }
         else
         {
          $this->load->view('sidebar');
          $this->load->view('register');
         } 
      
      }
public function doforget(){
  $this->load->view('sidebar');
   $this->load->view('forgotpwd');
}
    public function forgot_password(){
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|callback_validate_credentials');
    //check if email is in the database
    $this->load->model('model_users');
    if($this->model_users->email_exists()){
      function randomString($length = 8) {
      $length = 8;        
      $str = "";
      $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
      $max = count($characters) - 1;
      for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
        }
      return $str;
      }
//$pword = randomString();
    $password = randomString();
    $email=$this->input->post('email');
    $this->load->library('email');

$config['protocol']    = 'smtp';
$config['smtp_host']    = 'ssl://smtp.gmail.com';
$config['smtp_port']    = '465';
$config['smtp_timeout'] = '7';
$config['smtp_user']    = 'vibhav.kism@gmail.com';//sender email set here
$config['smtp_pass']    = 'Vib#12345';//sender email password. also set "Access for less secure apps" to ON in sender gmail account
//https://www.google.com/settings/security/lesssecureapps
//https://accounts.google.com/DisplayUnlockCaptcha
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'text'; // or html
$config['validation'] = TRUE; // bool whether to validate email or not      
$this->email->initialize($config);
$this->email->from('vibhav.kism@gmail.com', 'Vibhav Admin');
//$mailer->Body = 'Your password is: "'.$password.'';
 $this->email->to($email); 
//$mailer->AddAddress($_POST['email']);
            $this->email->subject('Reset Password');
            $this->email->message($password );  
            $this->email->send();
            echo $this->email->print_debugger();
            echo "<script>alert('Your generated passsword has been sent to your email.')</script>";
            $this->load->model('model_users');
            $result = $this->model_users->resetpwd($email,$password);
            $this->load->view('sidebar');
            $this->load->view('login');
    }
  else{
            $this->load->view('sidebar');
            $this->load->view('forgotpwd');
            echo "<script>alert('your email is not in our database')</script>";
        }
}

public function user_asset(){
    $id=$this->input->post('id');
    $this->load->model('user_model');
    $data['profile_data'] = $this->user_model->get_profile($id);   
    $data['asset_data'] = $this->user_model->select_allasset($id); 
    $this->load->view('adminpanelsidebar');
    $this->load->view('profile',$data);
  }
}
?>
