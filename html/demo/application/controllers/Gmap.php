<?php

class Gmap extends CI_Controller{

    function construct(){
        parent::__construct();
        $this->load->helper('form','url');
        $this->load->database();
        $this->load->view('phpsqlajax_dbinfo');
        $this->load->library('session');
    }
  //require("phpsqlajax_dbinfo.php");
// Start XML file, create parent node
    public function index(){
$doc = domxml_new_doc("1.0");
$node = $doc->create_element("markers");
$parnode = $doc->append_child($node);

// Opens a connection to a MySQL server
$connection=pg_connect ('localhost', $username, $password);
if (!$connection) {
  die('Not connected : ' . pg_result_error());
}

// Set the active MySQL database
//$db_selected = pg_d($database, $connection);
//if (!$db_selected) {
  //die ('Can\'t use db : ' . pg_result_error());
//}

// Select all the rows in the markers table
$query = "SELECT * FROM asset_profile WHERE 1";
$result = pg_query ($query);
if (!$result) {
  die('Invalid query: ' . pg_result_error());
}

header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @pg_fetch_assoc($result)){
  // Add to XML document node
  $node = $doc->create_element("marker");
  $newnode = $parnode->append_child($node);

  $newnode->set_attribute("employee_id", $row['employee_id']);
  $newnode->set_attribute("office_name", $row['office_name']);
  $newnode->set_attribute("office_type", $row['office_type']);
  $newnode->set_attribute("services offered", $row['serices_offered']);
  $newnode->set_attribute("working_start", $row['working_start']);
  $newnode->set_attribute("working_end", $row['working_end']);
  $newnode->set_attribute("lat", $row['latitude']);
  $newnode->set_attribute("lng", $row['logitude']);
 
}

$xmlfile = $doc->dump_mem();
echo $xmlfile;
}
?>